/**
 * FileName : {@link WebCommonBeanConfig}.java
 * Created : 2018. 8. 19. 오전 12:36:28
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.module.webcommon.config.commonbean;

import java.nio.charset.StandardCharsets;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.ViewResolver;

import com.fasterxml.jackson.annotation.JsonInclude;

@Configuration
@PropertySource("classpath:application.properties")
public class WebCommonBeanConfig {

	@Autowired
	private Environment env;

	@Bean
	public Jackson2ObjectMapperBuilder objectMapperBuilder() {

		final Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder();
		builder.serializationInclusion(JsonInclude.Include.NON_NULL);
		return builder;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {

		return new BCryptPasswordEncoder();
	}

	@Bean
	public ReloadableResourceBundleMessageSource ReloadableResourceBundleMessageSource() {

		final ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.addBasenames("url");
		messageSource.setDefaultEncoding(StandardCharsets.UTF_8.displayName());

		return messageSource;
	}

	@Bean
	public RequestContextListener requestContextListener() {

		return new RequestContextListener();
	}

	/**
	 * API 전/후 로거 도구
	 *
	 * @author jeonghyun.kum
	 * @return API 전/후 로거 도구
	 */
	@Bean
	public CommonsRequestLoggingFilter requestLoggingFilter() {

		final CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
		loggingFilter.setIncludeClientInfo(true);
		loggingFilter.setIncludeQueryString(true);
		loggingFilter.setIncludePayload(true);
		loggingFilter.setIncludeHeaders(true);
		return loggingFilter;
	}

	@Bean
	public ViewResolver viewResolver() {

		return new JspViewResolver(this.env);
	}
}
