/**
 * FileName : {@link JspViewResolver}.java
 * Created : 2019. 2. 4. 오전 12:49:56
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.module.webcommon.config.commonbean;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.boot.env.OriginTrackedMapPropertySource;
import org.springframework.core.env.AbstractEnvironment;
import org.springframework.core.env.Environment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.io.support.ResourcePropertySource;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

public class JspViewResolver extends InternalResourceViewResolver {

	private final Environment env;

	/**
	 * {@link JspViewResolver} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 4. 오전 12:50:19 jeonghyun.kum
	 */
	public JspViewResolver(Environment env) {

		this.env = env;
		this.initialize();
	}

	private void initialize() {

		final Map<String, Object> map = new ConcurrentHashMap<>();
		final MutablePropertySources mutable = ((AbstractEnvironment) this.env).getPropertySources();

		if (mutable != null) {
			for (final Object propertySource : mutable) {

				if ((propertySource instanceof OriginTrackedMapPropertySource)
					|| (propertySource instanceof ResourcePropertySource)) {
					map.putAll(((MapPropertySource) propertySource).getSource());
				}
			}
		}

		super.setViewClass(JstlView.class);
		super.setPrefix("/WEB-INF/view/");
		super.setSuffix(".jsp");

		final Map<String, Object> hashMap = new HashMap<>();
		hashMap.put("env", map);
		super.setAttributesMap(hashMap);

	}
}
