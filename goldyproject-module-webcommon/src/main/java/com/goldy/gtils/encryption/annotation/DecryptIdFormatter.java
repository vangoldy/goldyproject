/**
 * FileName : {@link DecryptIdFormatter}.java
 * Created : 2019. 2. 16. 오후 7:50:48
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.gtils.encryption.annotation;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.Formatter;

import com.goldy.gtils.encryption.exception.TableIdEncryptionException;
import com.goldy.gtils.encryption.tableid.TableLongIdCrypto;

public class DecryptIdFormatter implements Formatter<Long> {

	/**
	 * slf4j Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(DecryptIdFormatter.class);

	@SuppressWarnings("unused")
	private final DecryptId decryptId;

	private final TableLongIdCrypto tableLongIdCrypto = new TableLongIdCrypto();

	/**
	 * {@link DecryptIdFormatter} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 16. 오후 7:51:23 jeonghyun.kum
	 */
	public DecryptIdFormatter(DecryptId decryptId) {

		this.decryptId = decryptId;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Long parse(String text, Locale locale) {

		try {
			return this.tableLongIdCrypto.decrypt(this.decryptId.value(), text);
		} catch (final TableIdEncryptionException e) {
			LOGGER.trace(e.getMessage(), e);
			throw new IllegalArgumentException(e.getMessage(), e);

		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public String print(Long object, Locale locale) {

		try {
			return this.tableLongIdCrypto.encrypt(this.decryptId.value(), object);
		} catch (final TableIdEncryptionException e) {
			LOGGER.trace(e.getMessage(), e);
			return null;
		}
	}

}
