/**
 * FileName : {@link DecryptKeyFactory}.java
 * Created : 2018. 6. 7. 오후 11:28:21
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.gtils.encryption.annotation;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.springframework.context.support.EmbeddedValueResolutionSupport;
import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Formatter;
import org.springframework.format.Parser;
import org.springframework.format.Printer;

import com.goldy.gtils.does.SonarHelper;
import com.goldy.gtils.encryption.Encryption;
import com.goldy.gtils.encryption.exception.EncryptionException;

// https://stackoverflow.com/questions/46728972/custom-converter-for-requestparam-in-spring-mvc?rq=1
public class DecryptKeyFactory extends EmbeddedValueResolutionSupport
	implements AnnotationFormatterFactory<DecryptKey> {

	/**
	 * DecryptKeyFactory 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 */
	public DecryptKeyFactory() {

		super();

	}

	@SuppressWarnings("fb-contrib:LEST_LOST_EXCEPTION_STACK_TRACE")
	private static Formatter<Long> configureFormatterFrom(DecryptKey annotation) {

		return new Formatter<Long>() {

			private Long hardParse(String text) throws ParseException {

				try {
					final String decrypt = new Encryption().decrypt(text);
					return Long.parseLong(decrypt);
				} catch (NumberFormatException | EncryptionException ex) {
					SonarHelper.unuse(ex);
					throw new ParseException(MessageFormat.format("{0}복화화 하는 중 오류 발생{1}", text, ex.getMessage()), 0);
				}
			}

			@Override
			public Long parse(String text, Locale locale) throws ParseException {

				if (annotation.required()) {
					return this.hardParse(text);
				}
				return this.softParse(text);

			}

			@Override
			public String print(Long object, Locale locale) {

				return Long.toString(object);
			}

			private Long softParse(String text) throws ParseException {

				try {
					return Long.parseLong(text);
				} catch (final NumberFormatException ex1) {
					SonarHelper.unuse(ex1);
					return this.hardParse(text);
				}
			}
		};
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Set<Class<?>> getFieldTypes() {

		final Set<Class<?>> fieldTypes = new HashSet<>();
		fieldTypes.add(Integer.class);
		return Collections.unmodifiableSet(fieldTypes);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Parser<Long> getParser(DecryptKey annotation, Class<?> fieldType) {

		return DecryptKeyFactory.configureFormatterFrom(annotation);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Printer<Long> getPrinter(DecryptKey annotation, Class<?> fieldType) {

		return DecryptKeyFactory.configureFormatterFrom(annotation);
	}
}