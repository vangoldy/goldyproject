/**
 * FileName : {@link LongCryptoSerializer}.java
 * Created : 2019. 2. 16. 오후 7:34:37
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.gtils.encryption.annotation;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.goldy.gtils.does.SonarHelper;
import com.goldy.gtils.encryption.exception.TableIdEncryptionException;
import com.goldy.gtils.encryption.tableid.TableLongIdCrypto;

public class LongCryptoSerializer extends StdSerializer<Long> implements ContextualSerializer {

	private static final long serialVersionUID = 30994538957447189L;

	private String table;

	/**
	 * {@link LongCryptoSerializer} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 16. 오후 7:39:36 jeonghyun.kum
	 * @param t
	 */
	protected LongCryptoSerializer() {

		super(Long.class);
	}

	/**
	 * {@link LongCryptoSerializer} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 16. 오후 9:49:05 jeonghyun.kum
	 * @param table
	 */
	public LongCryptoSerializer(String table) {

		this();
		this.table = table;

	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public JsonSerializer<?> createContextual(SerializerProvider prov, BeanProperty property) {

		String table = null;
		final DecryptId decryptId = property.getAnnotation(DecryptId.class);
		if (decryptId != null) {
			table = decryptId.value();
		}

		return new LongCryptoSerializer(table);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void serialize(Long value, JsonGenerator gen, SerializerProvider provider) throws IOException {

		if ((value == null) || (value <= 0)) {
			gen.writeNull();
			return;
		}

		SonarHelper.soFar(this.table);
		String encrypt;
		try {
			encrypt = new TableLongIdCrypto().encrypt(this.table, value);
		} catch (final TableIdEncryptionException e) {
			gen.writeNull();
			return;
		}

		gen.writeString(encrypt);

	}

}
