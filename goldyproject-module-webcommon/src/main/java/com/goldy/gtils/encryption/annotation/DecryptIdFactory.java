/**
 * FileName : {@link DecryptKeyFactory}.java
 * Created : 2018. 6. 7. 오후 11:28:21
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.gtils.encryption.annotation;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.context.support.EmbeddedValueResolutionSupport;
import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Parser;
import org.springframework.format.Printer;

// https://stackoverflow.com/questions/46728972/custom-converter-for-requestparam-in-spring-mvc?rq=1
public class DecryptIdFactory extends EmbeddedValueResolutionSupport
	implements AnnotationFormatterFactory<DecryptId> {

	/**
	 * DecryptKeyFactory 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 */
	public DecryptIdFactory() {

		super();

	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Set<Class<?>> getFieldTypes() {

		final Set<Class<?>> fieldTypes = new HashSet<>();
		fieldTypes.add(Long.class);
		return Collections.unmodifiableSet(fieldTypes);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Parser<Long> getParser(DecryptId annotation, Class<?> fieldType) {

		return new DecryptIdFormatter(annotation);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Printer<Long> getPrinter(DecryptId annotation, Class<?> fieldType) {

		return new DecryptIdFormatter(annotation);
	}

}
