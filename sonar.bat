SET COMMEND=sonar:sonar verify
set list= ^
goldyproject-module-payment ^
goldyproject-module-ssoclient ^
goldyproject-module-webcommon ^
goldyproject-server-admin ^
goldyproject-server-eval ^
goldyproject-server-filesystem ^
goldyproject-server-home ^
goldyproject-server-konzession ^
goldyproject-server-me ^
goldyproject-server-push ^
goldyproject-server-shorturl ^
goldyproject-server-sso ^
goldyproject-server-version
(
	mvn -f=goldyproject install -Dmaven.test.skip=true
	for %%i in (%list%) do (
		mvn -f=%%i %COMMEND%
	)
)