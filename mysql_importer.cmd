SET PATH=%PATH%;C:\Program Files\MySQL\MySQL Server 8.0\bin
SET HOST=localhost
SET USER=root
SET PASS=root
SET FILE_NAME=structure.sql
SET SED=sed "s/ AUTO_INCREMENT=[0-9]*\b//"

mysql -h %HOST% -u %USER% -p%PASS% goldyproject_admin < goldyproject-server-admin\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_eval < goldyproject-server-eval\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_filesystem < goldyproject-server-filesystem\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_home < goldyproject-server-home\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_konzession < goldyproject-server-konzession\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_me < goldyproject-server-me\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_shorturl < goldyproject-server-shorturl\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_sso < goldyproject-server-sso\%FILE_NAME%
mysql -h %HOST% -u %USER% -p%PASS% goldyproject_version < goldyproject-server-version\%FILE_NAME%