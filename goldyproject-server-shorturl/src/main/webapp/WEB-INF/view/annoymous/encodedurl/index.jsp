<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>GOLDY PROJECT</title>
<base href="<c:url value='/'/>">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<style type="text/css">
html, body {
	background-color: #212529;
	color: white;
}

.search .green_window {
	display: inline-block;
	width: 100%;
	height: 41px;
	border: 5px solid #0076ff;
	background-color: #212529;
}

.search .input_text {
	font-family: Dotum, '돋움', Helvetica, "Apple SD Gothic Neo", sans-serif;
	margin: 5px 0 0 8px;
	width: 100%;
	height: 23px;
	outline: 0;
	border: 0;
	background-color: transparent;
	color: #fff;
	font-weight: 700;
	font-size: 18px;
	line-height: 23px;
}
</style>
</head>
<body type="menu-page">
	<center>
		<div class="log-outer">
			<a href="${env["gp.baseurl"]}">
				<img src='<c:url value="/resources/custom/image/logo.png"/>' />
			</a>
		</div>
	</center>
	<div class="container">
		<center>
			<form method="POST">
				<h1>Input URL Password</h1>
				<input id="query" name="password" type="password" title="패스워드 입력" maxlength="255" class="input_text"
					placeholder="Input Password"
				>
				<button id="input_btn" type="submit" class="btn btn-primary" title="GEREATE" tabindex="3">
					<span class="blind">INPUT</span>
				</button>
			</form>
		</center>
	</div>
</body>
</html>