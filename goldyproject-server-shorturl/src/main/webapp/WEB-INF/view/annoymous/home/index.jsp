<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>GOLDY PROJECT</title>
<base href="<c:url value='/'/>">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<style type="text/css">
html, body {
	background-color: #212529;
	color: white;
}

.container {
	padding-bottom: 100px;
}

.search .green_window {
	display: inline-block;
	width: 70%;
	height: 41px;
	border: 5px solid #0076ff;
	background-color: #212529;
}

.green_window2 {
	display: inline-block;
	width: 20%;
	height: 41px;
	border: 5px solid #0076ff;
	background-color: #212529;
}

.search .input_text {
	font-family: Dotum, '돋움', Helvetica, "Apple SD Gothic Neo", sans-serif;
	margin: 5px 0 0 8px;
	width: 100%;
	height: 23px;
	outline: 0;
	border: 0;
	background-color: transparent;
	color: #fff;
	font-weight: 700;
	font-size: 18px;
	line-height: 23px;
}
</style>
<script type="text/javascript">
	var baseUrl;
	baseUrl = settingBaseUrl();
	function settingBaseUrl() {
		var context = $('base').attr('href');
		return window.location.protocol + '//' + window.location.host
				+ context.substring(0, context.length - 1);
	}

	$.fn.serializeObject = function() {
		var obj = null;
		try {
			if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
				var arr = this.serializeArray();
				if (arr) {
					obj = {};
					jQuery.each(arr, function() {
						obj[this.name] = this.value;
					});
				}
			}
		} catch (e) {
			alert(e.message);
		} finally {
		}

		return obj;
	};
	Date.prototype.customFormat = function(formatString) {
		var YYYY, YY, MMMM, MMM, MM, M, DDDD, DDD, DD, D, hhhh, hhh, hh, h, mm, m, ss, s, ampm, AMPM, dMod, th;
		YY = ((YYYY = this.getFullYear()) + "").slice(-2);
		MM = (M = this.getMonth() + 1) < 10 ? ('0' + M) : M;
		MMM = (MMMM = ["January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November",
				"December"][M - 1]).substring(0, 3);
		DD = (D = this.getDate()) < 10 ? ('0' + D) : D;
		DDD = (DDDD = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday",
				"Friday", "Saturday"][this.getDay()]).substring(0, 3);
		th = (D >= 10 && D <= 20) ? 'th' : ((dMod = D % 10) == 1)
				? 'st'
				: (dMod == 2) ? 'nd' : (dMod == 3) ? 'rd' : 'th';
		formatString = formatString.replace("#YYYY#", YYYY).replace("#YY#", YY)
				.replace("#MMMM#", MMMM).replace("#MMM#", MMM).replace("#MM#",
						MM).replace("#M#", M).replace("#DDDD#", DDDD).replace(
						"#DDD#", DDD).replace("#DD#", DD).replace("#D#", D)
				.replace("#th#", th);
		h = (hhh = this.getHours());
		if (h == 0)
			h = 24;
		if (h > 12)
			h -= 12;
		hh = h < 10 ? ('0' + h) : h;
		hhhh = hhh < 10 ? ('0' + hhh) : hhh;
		AMPM = (ampm = hhh < 12 ? 'am' : 'pm').toUpperCase();
		mm = (m = this.getMinutes()) < 10 ? ('0' + m) : m;
		ss = (s = this.getSeconds()) < 10 ? ('0' + s) : s;
		return formatString.replace("#hhhh#", hhhh).replace("#hhh#", hhh)
				.replace("#hh#", hh).replace("#h#", h).replace("#mm#", mm)
				.replace("#m#", m).replace("#ss#", ss).replace("#s#", s)
				.replace("#ampm#", ampm).replace("#AMPM#", AMPM);
	};

	$(function() {
		$('#redirecturl-shorten-form').submit(function(e) {
			e.preventDefault();
			var jsonObject = $(this).serializeObject();
			var jsonString = JSON.stringify(jsonObject);

			$.ajax({
				type : 'POST',
				url : baseUrl + "/api/redirecturl/shorten",
				data : jsonString,
				contentType : 'application/json;charset=UTF-8',
				dataType : 'JSON',
				success : function(data) {
					$('#redirecturl-shorten-result').text(data.shortUrl);
					redrawRedirectUrlTable();
				},
				error : function(xhr) {
					alert(xhr.responseText);
				}
			});
		});

		$('#encodedurl-shorten-form').submit(function(e) {
			e.preventDefault();
			var jsonObject = $(this).serializeObject();
			var jsonString = JSON.stringify(jsonObject);

			$.ajax({
				type : 'POST',
				url : baseUrl + "/api/encodedurl/shorten",
				data : jsonString,
				contentType : 'application/json;charset=UTF-8',
				dataType : 'JSON',
				success : function(data) {
					$('#encodedurl-shorten-result').text(data.shortUrl);
					redrawEncodedUrlTable();
				},
				error : function(xhr) {
					alert(xhr.responseText);
				}
			});
		});

	});
	$(function() {
		redrawRedirectUrlTable();
		redrawEncodedUrlTable()
	})

	function redrawRedirectUrlTable() {
		$.ajax({
			type : 'GET',
			url : baseUrl + '/api/redirecturls',
			success : function(result) {
				console.log(result);
				drawRedirectUrlTable(result.content);
			}
		})
	}

	function redrawEncodedUrlTable() {
		$.ajax({
			type : 'GET',
			url : baseUrl + '/api/encodedurls',
			success : function(result) {
				console.log(result);
				drawEncodedUrlTable(result.content);
			}
		})
	}

	function drawRedirectUrlTable(datas) {

		var $tableBody = $('#redirecturl-table-body');
		$tableBody.empty();
		for (var i = 0; i < datas.length; i++) {

			var $tr = createRedirectTr(datas[i]);
			$tableBody.append($tr);
		}
	}

	function drawEncodedUrlTable(datas) {

		var $tableBody = $('#encodedurl-table-body');
		$tableBody.empty();
		for (var i = 0; i < datas.length; i++) {

			var $tr = createEncodedTr(datas[i]);
			$tableBody.append($tr);
		}
	}

	function createRedirectTr(data) {

		var $tr = $('<tr>');

		$tr.append(createRedirectUrlTd(data));
		$tr.append(createOriginalUrlTd(data));
		$tr.append(createCreatedTd(data));
		$tr.append(createClickTd(data));

		return $tr;
	}

	function createEncodedTr(data) {

		var $tr = $('<tr>');

		$tr.append(createEncodedUrlTd(data));
		$tr.append(createOriginalUrlTd(data));
		$tr.append(createPasswordTd(data));
		$tr.append(createCreatedTd(data));
		$tr.append(createClickTd(data));

		return $tr;
	}

	function createRedirectUrlTd(data) {
		var $td = $('<td class="short-url-td">');
		var $a = $('<a>');
		$a.text(baseUrl + '/r' + data.shortUrl);
		$a.click(function() {
			location.href = baseUrl + '/r' + data.shortUrl;
		})
		$td.append($a);
		return $td;
	}

	function createEncodedUrlTd(data) {
		var $td = $('<td class="short-url-td">');
		var $a = $('<a>');
		$a.text(baseUrl + '/e' + data.shortUrl);
		$a.click(function() {
			location.href = baseUrl + '/e' + data.shortUrl;
		})
		$td.append($a);
		return $td;
	}

	function createOriginalUrlTd(data) {
		var $td = $('<td class="original-url-td">');
		var $a = $('<a>');
		$a.text(data.redirectUrl);
		$a.click(function() {
			location.href = data.redirectUrl;
		})
		$td.append($a);

		return $td;
	}

	function createPasswordTd(data) {
		var $td = $('<td class="password-td">');
		$td.text(data.password);

		return $td;
	}

	function createCreatedTd(data) {
		var $td = $('<td class="created-td">');
		$td.text(new Date(data.createdTime)
				.customFormat('#YYYY#/#MM#/#DD# #hh#:#mm#:#ss#'));

		return $td;
	}
	function createClickTd(data) {
		var $td = $('<td class="click-td">');
		var $a = $('<a>');
		$a.text(data.clickCount);
		$td.append($a);

		return $td;
	}
</script>
</head>
<body type="menu-page">
	<center>
		<div class="log-outer">
			<a href="${env["gp.baseurl"]}">
				<img src='<c:url value="/resources/custom/image/logo.png"/>' />
			</a>
		</div>
	</center>
	<center>
		<div class="sso-login-outer">
			<sec:authorize access="isAuthenticated()">
				<div class="btn btn-primary sso-login" onClick="javascript:location.href='logout'">logout</div>
			</sec:authorize>
			<sec:authorize access="isAnonymous()">
				<div class="btn btn-primary sso-login" onClick="javascript:location.href='securedpage'">Login</div>
			</sec:authorize>
		</div>
	</center>
	<div class="container">
		<center>
			<h1>Public Short URL</h1>
			<div class="search">
				<!--자동완성 입력창-->
				<form id="redirecturl-shorten-form">
					<span class="green_window">
						<input id="query" name="originalUrl" type="text" title="검색어 입력" maxlength="255" class="input_text"
							placeholder="Input Orriginal URL"
						>
					</span>
					<button id="search_btn" type="submit" class="btn btn-primary" title="GEREATE" tabindex="3">
						<span class="blind">GEREATE</span>
					</button>
				</form>
				<div id="redirecturl-shorten-result"></div>
			</div>
		</center>
		<table class="table table-dark">
			<thead>
				<tr>
					<th scope="col">Short URL</th>
					<th scope="col">Original URL</th>
					<th scope="col">Created</th>
					<th scope="col">Clicks</th>
				</tr>
			</thead>
			<tbody id='redirecturl-table-body'></tbody>
		</table>
	</div>
	<div class="container">
		<center>
			<h1>Private Short URL</h1>
			<div class="search">
				<!--자동완성 입력창-->
				<form id="encodedurl-shorten-form">
					<span class="green_window">
						<input id="query" name="originalUrl" type="text" title="검색어 입력" maxlength="255" class="input_text"
							placeholder="Input Orriginal URL"
						>
					</span>
					<span class="green_window2">
						<input id="query" name="password" type="password" title="비밀번호 입력" maxlength="255" class="input_text"
							placeholder="Input Password"
						>
					</span>
					<button id="search_btn" type="submit" class="btn btn-primary" title="GEREATE" tabindex="3">
						<span class="blind">GEREATE</span>
					</button>
				</form>
				<div id="encodedurl-shorten-result"></div>
			</div>
		</center>
		<table class="table table-dark">
			<thead>
				<tr>
					<th scope="col">Short URL</th>
					<th scope="col">Original URL</th>
					<th scope="col">Password</th>
					<th scope="col">Created</th>
					<th scope="col">Clicks</th>
				</tr>
			</thead>
			<tbody id='encodedurl-table-body'></tbody>
		</table>
	</div>
</body>
</html>