/**
 * FileName : {@link ShortenModel}.java
 * Created : 2018. 9. 2. 오후 7:43:13
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.model;

import java.time.LocalDateTime;

import com.goldy.project.server.shorturl.db.entity.FnTableEntity;
import com.goldy.project.server.shorturl.db.unit.FnTableType;

public class ShortenModel {

	private FnTableType type;

	private String shortUrl;

	private LocalDateTime createdType;

	private FnTableEntity saved;

	/**
	 * createdType를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return createdType
	 */
	public LocalDateTime getCreatedType() {

		return this.createdType;
	}

	/**
	 * value를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return value
	 */
	public FnTableEntity getSaved() {

		return this.saved;
	}

	/**
	 * shortUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return shortUrl
	 */
	public String getShortUrl() {

		return this.shortUrl;
	}

	/**
	 * type를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return type
	 */
	public FnTableType getType() {

		return this.type;
	}

	/**
	 * createdType 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param createdType
	 *            초기화 값
	 */
	public void setCreatedType(LocalDateTime createdType) {

		this.createdType = createdType;
	}

	/**
	 * value 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param value
	 *            초기화 값
	 */
	public void setSaved(FnTableEntity value) {

		this.saved = value;
	}

	/**
	 * shortUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param shortUrl
	 *            초기화 값
	 */
	public void setShortUrl(String shortUrl) {

		this.shortUrl = shortUrl;
	}

	/**
	 * type 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param type
	 *            초기화 값
	 */
	public void setType(FnTableType type) {

		this.type = type;
	}

}
