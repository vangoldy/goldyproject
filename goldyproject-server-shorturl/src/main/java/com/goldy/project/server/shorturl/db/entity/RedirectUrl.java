/**
 * FileName : {@link RedirectUrl}.java
 * Created : 2018. 9. 2. 오후 6:05:47
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.db.entity;

import javax.persistence.Entity;

import org.hibernate.annotations.Formula;

@Entity
public class RedirectUrl extends FnTableEntity {

	private String redirectUrl;

	@Formula("(SELECT ukey.url_key FROM ukey WHERE ukey_id = ukey.id)")
	private String shortUrl;

	@Formula("(SELECT ukey.click_count FROM ukey WHERE ukey_id = ukey.id)")
	private int clickCount;

	/**
	 * clickCount를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return clickCount
	 */
	public int getClickCount() {

		return this.clickCount;
	}

	/**
	 * redirectUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return redirectUrl
	 */
	public String getRedirectUrl() {

		return this.redirectUrl;
	}

	/**
	 * shortUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return shortUrl
	 */
	public String getShortUrl() {

		return this.shortUrl;
	}

	/**
	 * clickCount 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param clickCount
	 *            초기화 값
	 */
	public void setClickCount(int clickCount) {

		this.clickCount = clickCount;
	}

	/**
	 * redirectUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param redirectUrl
	 *            초기화 값
	 */
	public void setRedirectUrl(String redirectUrl) {

		this.redirectUrl = redirectUrl;
	}

	/**
	 * shortUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param shortUrl
	 *            초기화 값
	 */
	public void setShortUrl(String shortUrl) {

		this.shortUrl = shortUrl;
	}

}
