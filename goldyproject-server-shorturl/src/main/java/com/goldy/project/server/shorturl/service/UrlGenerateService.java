/**
 * FileName : {@link UrlGenerateService}.java
 * Created : 2018. 9. 2. 오후 7:23:01
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.goldy.gtils.utils.RandomGtils;
import com.goldy.project.server.shorturl.db.dao.UkeyDao;

@Service
public class UrlGenerateService {

	@Autowired
	private UkeyDao shortUrlDao;

	private final RandomGtils randomGtils = new RandomGtils();

	public String generate() {

		return this.recursiveGenerate(1);
	}

	private String recursiveGenerate(int range) {

		final String target = this.randomGtils.randomString(3, range);
		if (this.shortUrlDao.existsByUrlKey(target)) {
			return this.recursiveGenerate(range + 1);
		}
		return target;
	}
}
