/**
 * FileName : {@link RedirectController}.java
 * Created : 2018. 9. 2. 오후 9:08:26
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.goldy.gtils.exception.InspectException;
import com.goldy.gtils.exception.LogicErrorType;
import com.goldy.gtils.exception.LogicException;
import com.goldy.project.server.shorturl.db.dao.RedirectUrlDao;
import com.goldy.project.server.shorturl.db.dao.UkeyDao;
import com.goldy.project.server.shorturl.db.entity.RedirectUrl;
import com.goldy.project.server.shorturl.db.entity.Ukey;
import com.goldy.project.server.shorturl.model.ShortenModel;
import com.goldy.project.server.shorturl.model.ShortenRedirectPostForm;
import com.goldy.project.server.shorturl.service.ClickMetadataService;
import com.goldy.project.server.shorturl.service.RedirectShortenService;

@Controller
public class RedirectController {

	@Autowired
	private UkeyDao ukeyDao;

	@Autowired
	private RedirectUrlDao redirectUrlDao;

	@Autowired
	private ClickMetadataService clickMetadataService;

	@Autowired
	private RedirectShortenService shortenService;

	private String findRedirectUrlPath(HttpServletRequest servletRequest, String urlKey) {

		final Ukey target = this.ukeyDao.findByUrlKey(urlKey)
			.orElseThrow(() -> new InspectException("NOT_FOUND_SHORTURL"));

		final Long ukeyId = target.getId();

		final RedirectUrl redirectUrl = this.redirectUrlDao.findById(ukeyId)
			.orElseThrow(() -> new LogicException(LogicErrorType.INVALID_DESIGN));

		this.ukeyDao.incrementClickCount(ukeyId);
		this.clickMetadataService.write(servletRequest, ukeyId);

		return redirectUrl.getRedirectUrl();
	}

	@GetMapping("/p{urlKey}")
	@ResponseBody
	public String getRedirectPath(HttpServletRequest servletRequest, @PathVariable String urlKey) {

		return this.findRedirectUrlPath(servletRequest, urlKey);
	}

	@GetMapping({ "/r{urlKey}" })
	public String redirect(HttpServletRequest servletRequest, @PathVariable String urlKey) {

		final String redirectUrlPath = this.findRedirectUrlPath(servletRequest, urlKey);
		return "redirect:" + redirectUrlPath;
	}

	@GetMapping(value = "/api/redirecturls")
	@ResponseBody
	public Page<RedirectUrl> redirectUrls(
		@PageableDefault(size = 10, sort = "createdTime", direction = Direction.DESC) Pageable pageable) {

		return this.redirectUrlDao.findAll(pageable);
	}

	@PostMapping("/api/redirecturl/shorten")
	@ResponseBody
	public ShortenModel shorten(HttpServletRequest servletRequest,
		@Valid @RequestBody ShortenRedirectPostForm model) {

		return this.shortenService.shorten(servletRequest, model);
	}

}
