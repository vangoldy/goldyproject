/**
 * FileName : {@link ClickMetadataService}.java
 * Created : 2018. 9. 2. 오후 9:30:17
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.goldy.gtils.does.SonarHelper;
import com.goldy.gtils.utils.HttpServletGtils;
import com.goldy.gtils.utils.NullGtils;
import com.goldy.gtils.utils.RequestDetail;
import com.goldy.project.server.shorturl.db.dao.ClickMetadataDao;
import com.goldy.project.server.shorturl.db.entity.ClickMetadata;
import com.goldy.project.server.shorturl.validator.ClickRequesterInspection;

@Service
public class ClickMetadataService {

	@Autowired
	private ClickMetadataDao clickMetadataDao;

	public void write(HttpServletRequest servletRequest, long ukeyId) {

		try {
			final RequestDetail extractRequest = HttpServletGtils.extractRequest(servletRequest);
			final String fromUrl = NullGtils.getOr(
				extractRequest::getxForwardedServer,
				extractRequest::getxForwardedHost,
				extractRequest::getxForwardedFor,
				extractRequest::getCfConnectingIp,
				extractRequest::getReferer);
			new ClickRequesterInspection().check(fromUrl);

			final ClickMetadata clientMetadata = new ClickMetadata();
			clientMetadata.setUkeyId(ukeyId);
			clientMetadata.setIp(extractRequest.getRemoteHost());
			clientMetadata.setPort(extractRequest.getRemotePort());
			clientMetadata.setUser(extractRequest.getRemoteUser());
			clientMetadata.setSessionId(extractRequest.getSessionId());
			clientMetadata.setAgent(extractRequest.getUserAgent());
			clientMetadata.setFromUrl(fromUrl);
			clientMetadata.setAuthType(extractRequest.getAuthType());
			clientMetadata.setCookie(extractRequest.getCookieString());

			this.clickMetadataDao.save(clientMetadata);

		} catch (final Throwable e) {
			SonarHelper.unuse(e);
		}
	}

}
