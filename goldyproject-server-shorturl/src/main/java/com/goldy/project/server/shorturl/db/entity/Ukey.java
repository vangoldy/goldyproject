/**
 * FileName : {@link Ukey}.java
 * Created : 2018. 9. 2. 오후 6:21:16
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.db.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.Formula;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;
import com.goldy.project.module.webcommon.db.entity.TimeEntity;
import com.goldy.project.server.shorturl.db.converter.FnTableTypeConverter;
import com.goldy.project.server.shorturl.db.unit.FnTableType;

@Entity
public class Ukey extends TimeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId("account")
	private Long id;

	private String urlKey;

	private Long fnTableId;

	private int clickCount;

	@Formula("(SELECT fn_table.table FROM fn_table WHERE fn_table_id = fn_table.id)")
	@Convert(converter = FnTableTypeConverter.class)
	private FnTableType table;

	/**
	 * clickCount를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return clickCount
	 */
	public int getClickCount() {

		return this.clickCount;
	}

	/**
	 * fnTableId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return fnTableId
	 */
	public Long getFnTableId() {

		return this.fnTableId;
	}

	/**
	 * fnTable를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return fnTable
	 */
	public FnTableType getTable() {

		return this.table;
	}

	/**
	 * shortUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return shortUrl
	 */
	public String getUrlKey() {

		return this.urlKey;
	}

	/**
	 * clickCount 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param clickCount
	 *            초기화 값
	 */
	public void setClickCount(int clickCount) {

		this.clickCount = clickCount;
	}

	/**
	 * fnTableId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param fnTableId
	 *            초기화 값
	 */
	public void setFnTableId(Long fnTableId) {

		this.fnTableId = fnTableId;
	}

	/**
	 * fnTable 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param fnTable
	 *            초기화 값
	 */
	public void setTable(FnTableType fnTable) {

		this.table = fnTable;
	}

	/**
	 * shortUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param shortUrl
	 *            초기화 값
	 */
	public void setUrlKey(String shortUrl) {

		this.urlKey = shortUrl;
	}

	/**
	 * id를 반환합니다.
	 * @return id
	 * @author jeonghyun.kum
	 */
	public Long getId() {

		return this.id;
	}

	/**
	 * id 초기화 합니다.
	 * @param id 초기화 값
	 * @author jeonghyun.kum
	 */
	public void setId(Long id) {

		this.id = id;
	}

}
