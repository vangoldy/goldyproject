/**
 * FileName : {@link EncodedController}.java
 * Created : 2018. 9. 5. 오후 7:56:34
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.goldy.gtils.does.SonarHelper;
import com.goldy.gtils.encryption.SaltEncryptor;
import com.goldy.gtils.exception.InspectException;
import com.goldy.gtils.exception.LogicErrorType;
import com.goldy.gtils.exception.LogicException;
import com.goldy.project.server.shorturl.db.dao.EncodedUrlDao;
import com.goldy.project.server.shorturl.db.dao.UkeyDao;
import com.goldy.project.server.shorturl.db.entity.EncodedUrl;
import com.goldy.project.server.shorturl.db.entity.Ukey;
import com.goldy.project.server.shorturl.model.ShortenEncodedPostForm;
import com.goldy.project.server.shorturl.model.ShortenModel;
import com.goldy.project.server.shorturl.service.ClickMetadataService;
import com.goldy.project.server.shorturl.service.EncodedShortenService;

@Controller
public class EncodedController {

	@Autowired
	private EncodedShortenService shortenService;

	@Autowired
	private EncodedUrlDao encodedUrlDao;

	@Autowired
	private UkeyDao ukeyDao;

	@Autowired
	private ClickMetadataService clickMetadataService;

	private String findRedirectUrlPath(HttpServletRequest servletRequest, String urlKey, String password) {

		final Ukey target = this.ukeyDao.findByUrlKey(urlKey)
			.orElseThrow(() -> new InspectException("NOT_FOUND_SHORTURL"));

		final Long ukeyId = target.getId();

		final EncodedUrl encodedUrl = this.encodedUrlDao.findById(ukeyId)
			.orElseThrow(() -> new LogicException(LogicErrorType.INVALID_DESIGN));

		if (SaltEncryptor.matches(password, encodedUrl.getPassword()) == false) {
			return "/e" + urlKey + "?fail=wrongpassword";
		}

		this.ukeyDao.incrementClickCount(ukeyId);
		this.clickMetadataService.write(servletRequest, ukeyId);

		return encodedUrl.getRedirectUrl();
	}

	@GetMapping(value = "/api/encodedurls")
	@ResponseBody
	public Page<EncodedUrl> getEncodedUrls(
		@PageableDefault(size = 10, sort = "createdTime", direction = Direction.DESC) Pageable pageable) {

		return this.encodedUrlDao.findAll(pageable);
	}

	@GetMapping(value = "/e{urlKey}")
	public String getMethodName(@PathVariable String urlKey) {

		SonarHelper.soFar(urlKey);
		return "annoymous/encodedurl/index";
	}

	@PostMapping(value = "/e{urlKey}")
	public String postMethodName(HttpServletRequest servletRequest, @PathVariable String urlKey,
		String password) {

		final String redirect = this.findRedirectUrlPath(servletRequest, urlKey, password);
		return "redirect:" + redirect;
	}

	@PostMapping("/api/encodedurl/shorten")
	@ResponseBody
	public ShortenModel shorten(HttpServletRequest servletRequest,
		@Valid @RequestBody ShortenEncodedPostForm model) {

		return this.shortenService.shorten(servletRequest, model);
	}
}
