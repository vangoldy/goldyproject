/**
 * FileName : {@link ClickMetadata}.java
 * Created : 2018. 9. 2. 오후 9:30:39
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.db.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;
import com.goldy.project.module.webcommon.db.entity.TimeEntity;

@Entity
public class ClickMetadata extends TimeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId("account")
	private Long id;

	private Long ukeyId;

	private String authType;

	private String fromUrl;

	private String ip;

	private int port;

	private String user;

	private String sessionId;

	private String agent;

	private String cookie;

	/**
	 * agent를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return agent
	 */
	public String getAgent() {

		return this.agent;
	}

	/**
	 * authType를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return authType
	 */
	public String getAuthType() {

		return this.authType;
	}

	/**
	 * cookies를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return cookies
	 */
	public String getCookie() {

		return this.cookie;
	}

	/**
	 * fromUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return fromUrl
	 */
	public String getFromUrl() {

		return this.fromUrl;
	}

	/**
	 * id를 반환합니다.
	 *
	 * @return id
	 * @author jeonghyun.kum
	 */
	public Long getId() {

		return this.id;
	}

	/**
	 * ip를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return ip
	 */
	public String getIp() {

		return this.ip;
	}

	/**
	 * port를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return port
	 */
	public int getPort() {

		return this.port;
	}

	/**
	 * sessionId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return sessionId
	 */
	public String getSessionId() {

		return this.sessionId;
	}

	/**
	 * shortUrlId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return shortUrlId
	 */
	public Long getUkeyId() {

		return this.ukeyId;
	}

	/**
	 * user를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return user
	 */
	public String getUser() {

		return this.user;
	}

	/**
	 * agent 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param agent
	 *            초기화 값
	 */
	public void setAgent(String agent) {

		this.agent = agent;
	}

	/**
	 * authType 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param authType
	 *            초기화 값
	 */
	public void setAuthType(String authType) {

		this.authType = authType;
	}

	/**
	 * cookies 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param cookie
	 *            초기화 값
	 */
	public void setCookie(String cookie) {

		this.cookie = cookie;
	}

	/**
	 * fromUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param fromUrl
	 *            초기화 값
	 */
	public void setFromUrl(String fromUrl) {

		this.fromUrl = fromUrl;
	}

	/**
	 * id 초기화 합니다.
	 *
	 * @param id
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * ip 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param ip
	 *            초기화 값
	 */
	public void setIp(String ip) {

		this.ip = ip;
	}

	/**
	 * port 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param port
	 *            초기화 값
	 */
	public void setPort(int port) {

		this.port = port;
	}

	/**
	 * sessionId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param sessionId
	 *            초기화 값
	 */
	public void setSessionId(String sessionId) {

		this.sessionId = sessionId;
	}

	/**
	 * shortUrlId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param shortUrlId
	 *            초기화 값
	 */
	public void setUkeyId(Long shortUrlId) {

		this.ukeyId = shortUrlId;
	}

	/**
	 * user 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param user
	 *            초기화 값
	 */
	public void setUser(String user) {

		this.user = user;
	}

}
