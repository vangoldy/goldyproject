/**
 * FileName : {@link OriginalUrlInspection}.java
 * Created : 2018. 9. 5. 오후 9:12:38
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.validator;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.goldy.gtils.exception.InspectException;
import com.goldy.gtils.inspection.Inspection;
import com.goldy.gtils.inspection.StringInspection;

@Component
public class OriginalUrlInspection implements Inspection<String> {

	@Value("${gp.host.shorturl}")
	private String shorturlHost;

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void check(String target) {

		final List<String> apis = Arrays.asList(this.shorturlHost + "/r", this.shorturlHost + "/p",
			this.shorturlHost + "/e");

		StringInspection.checkBlank(target).inspect();

		for (final String api : apis) {
			if (target.startsWith(api)) {
				throw new InspectException("Unable to create short URL");
			}
		}

	}

}
