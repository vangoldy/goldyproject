/**
 * FileName : {@link EncodedShortenService}.java
 * Created : 2018. 9. 8. 오후 2:40:07
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.service;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.goldy.gtils.encryption.SaltEncryptor;
import com.goldy.project.server.shorturl.db.dao.EncodedUrlDao;
import com.goldy.project.server.shorturl.db.dao.FnTableDao;
import com.goldy.project.server.shorturl.db.dao.UkeyDao;
import com.goldy.project.server.shorturl.db.entity.EncodedUrl;
import com.goldy.project.server.shorturl.db.entity.FnTable;
import com.goldy.project.server.shorturl.db.entity.Ukey;
import com.goldy.project.server.shorturl.db.unit.FnTableType;
import com.goldy.project.server.shorturl.model.ShortenEncodedPostForm;
import com.goldy.project.server.shorturl.model.ShortenModel;
import com.goldy.project.server.shorturl.service.inf.ShortenService;
import com.goldy.project.server.shorturl.validator.EncodedUrlPasswordInspection;
import com.goldy.project.server.shorturl.validator.OriginalUrlInspection;

@Service
@Transactional
public class EncodedShortenService implements ShortenService<ShortenEncodedPostForm> {

	@Autowired
	private UkeyDao shortUrlDao;

	@Autowired
	private EncodedUrlDao encodedUrlDao;

	@Autowired
	private FnTableDao fnTableDao;

	@Autowired
	private OriginalUrlInspection originalUrlInspection;

	@Autowired
	private UrlGenerateService urlGenerateService;

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public ShortenModel shorten(HttpServletRequest servletRequest, ShortenEncodedPostForm model) {

		this.originalUrlInspection.check(model.getOriginalUrl());
		new EncodedUrlPasswordInspection().check(model.getPassword());

		final String url = this.urlGenerateService.generate();

		final FnTable fnTable = this.fnTableDao.findByTable(FnTableType.ENCODED_URL).orElseGet(() -> {

			final FnTable newFnTable = new FnTable();
			newFnTable.setTable(FnTableType.ENCODED_URL);
			return this.fnTableDao.save(newFnTable);
		});

		final Ukey shortUrl = new Ukey();
		shortUrl.setUrlKey(url);
		shortUrl.setFnTableId(fnTable.getId());
		shortUrl.setTable(FnTableType.ENCODED_URL);

		final Ukey savedShortUrl = this.shortUrlDao.save(shortUrl);

		final EncodedUrl redirectUrl = new EncodedUrl();
		redirectUrl.setUkeyId(savedShortUrl.getId());
		redirectUrl.setRedirectUrl(model.getOriginalUrl());
		redirectUrl.setPassword(SaltEncryptor.encode(model.getPassword()));

		final EncodedUrl savedRedirectUrl = this.encodedUrlDao.save(redirectUrl);

		final ShortenModel result = new ShortenModel();

		final String baseUrl = servletRequest.getRequestURL().toString().replace(servletRequest.getRequestURI(),
			servletRequest.getContextPath());
		result.setShortUrl(MessageFormat.format("{0}/e{1}", baseUrl, savedShortUrl.getUrlKey()));
		result.setType(savedShortUrl.getTable());
		result.setCreatedType(savedShortUrl.getCreatedTime());
		result.setSaved(savedRedirectUrl);

		return result;
	}

}
