/**
 * FileName : {@link FnTable}.java
 * Created : 2018. 9. 2. 오후 8:31:31
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.db.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.goldy.project.server.shorturl.db.converter.FnTableTypeConverter;
import com.goldy.project.server.shorturl.db.unit.FnTableType;

@Entity
public class FnTable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	private Long id;

	@Convert(converter = FnTableTypeConverter.class)
	private FnTableType table;

	/**
	 * id를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return id
	 */
	public Long getId() {

		return this.id;
	}

	/**
	 * fnTableType를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return fnTableType
	 */
	public FnTableType getTable() {

		return this.table;
	}

	/**
	 * id 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param id
	 *            초기화 값
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * fnTableType 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param fnTableType
	 *            초기화 값
	 */
	public void setTable(FnTableType fnTableType) {

		this.table = fnTableType;
	}

}
