/**
 * FileName : {@link FnTableDao}.java
 * Created : 2018. 9. 2. 오후 8:33:11
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.db.dao;

import java.util.Optional;

import javax.persistence.Convert;

import org.springframework.data.jpa.repository.JpaRepository;

import com.goldy.project.server.shorturl.db.converter.FnTableTypeConverter;
import com.goldy.project.server.shorturl.db.entity.FnTable;
import com.goldy.project.server.shorturl.db.unit.FnTableType;

public interface FnTableDao extends JpaRepository<FnTable, Long> {

	@Convert(converter = FnTableTypeConverter.class)
	Optional<FnTable> findByTable(FnTableType tableType);

}
