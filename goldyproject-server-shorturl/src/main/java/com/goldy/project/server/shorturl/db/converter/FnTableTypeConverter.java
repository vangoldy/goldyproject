/**
 * FileName : {@link FnTableTypeConverter}.java
 * Created : 2018. 9. 2. 오후 7:18:46
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.db.converter;

import java.util.Locale;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.goldy.project.server.shorturl.db.unit.FnTableType;

@Converter
public class FnTableTypeConverter implements AttributeConverter<FnTableType, String> {

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public String convertToDatabaseColumn(FnTableType attribute) {

		return attribute.name().toLowerCase(Locale.getDefault());
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public FnTableType convertToEntityAttribute(String dbData) {

		return FnTableType.valueOf(dbData.toUpperCase(Locale.getDefault()));
	}

}
