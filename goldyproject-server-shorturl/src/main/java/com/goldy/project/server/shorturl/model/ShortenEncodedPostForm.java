/**
 * FileName : {@link ShortenEncodedPostForm}.java
 * Created : 2018. 9. 2. 오후 7:43:17
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.model;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.URL;

import com.goldy.gtils.validation.constraints.RequireField;

public class ShortenEncodedPostForm {

	@RequireField
	@Size(min = 6)
	@URL
	private String originalUrl;

	@RequireField
	@Size(min = 6)
	private String password;

	/**
	 * originalUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return originalUrl
	 */
	public String getOriginalUrl() {

		return this.originalUrl;
	}

	/**
	 * password를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return password
	 */
	public String getPassword() {

		return this.password;
	}

	/**
	 * originalUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param originalUrl
	 *            초기화 값
	 */
	public void setOriginalUrl(String originalUrl) {

		this.originalUrl = originalUrl;
	}

	/**
	 * password 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param password
	 *            초기화 값
	 */
	public void setPassword(String password) {

		this.password = password;
	}

}
