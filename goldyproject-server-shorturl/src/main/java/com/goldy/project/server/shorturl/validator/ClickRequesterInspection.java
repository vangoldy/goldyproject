/**
 * FileName : {@link ClickRequesterInspection}.java
 * Created : 2018. 9. 5. 오후 9:08:40
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.validator;

import java.util.Arrays;
import java.util.List;

import com.goldy.gtils.exception.InspectException;
import com.goldy.gtils.inspection.Inspection;
import com.goldy.gtils.inspection.StringInspection;

public class ClickRequesterInspection implements Inspection<String> {

	/** 제외대상 */
	public static final String EXCLUDE = "EXCLUDE";

	private static final List<String> IGNORE_HOSTS = Arrays.asList("127.0.0.1", "localhost", "goldy");

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void check(String target) {

		if (StringInspection.isBlank(target)) {
			return;
		}

		for (final String ignoreHost : ClickRequesterInspection.IGNORE_HOSTS) {
			if (ignoreHost.contains(ignoreHost)) {
				throw new InspectException(ClickRequesterInspection.EXCLUDE);
			}
		}
	}

}
