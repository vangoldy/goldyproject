/**
 * FileName : {@link EncodedUrlPasswordInspection}.java
 * Created : 2018. 9. 5. 오후 9:04:12
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.shorturl.validator;

import com.goldy.gtils.inspection.Inspection;
import com.goldy.gtils.inspection.StringInspection;

public class EncodedUrlPasswordInspection implements Inspection<String> {

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void check(String target) {

		// 문자열이 null 또는 Empty 인지 검사
		StringInspection.checkBlank(target).inspect();

		// 한글이 포함되어있는지 검사
		StringInspection.checkExcluedHangle(target).inspect();

		// 공백이 포함되었는지 검사
		StringInspection.checkExcludedWhiteSpace(target).inspect();
	}

}
