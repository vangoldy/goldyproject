-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: goldyproject_shorturl
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `click_metadata`
--

LOCK TABLES `click_metadata` WRITE;
/*!40000 ALTER TABLE `click_metadata` DISABLE KEYS */;
INSERT INTO `click_metadata` (`id`, `ukey_id`, `ip`, `port`, `user`, `from_url`, `session_id`, `agent`, `auth_type`, `cookie`, `created_time`, `updated_time`) VALUES (559,35,'127.0.0.1',56470,NULL,NULL,'FDD27F1D980E460ACF873B1602F93F1B','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',NULL,'{\"name\":\"GOLDYSESSION\",\"value\":\"339971F870B029513006F784B1B1C776\",\"version\":0,\"maxAge\":-1,\"secure\":false,\"httpOnly\":false}\n{\"name\":\"GOLDYSESSION\",\"value\":\"7E319FC4A70DD9A938F4F6EAA3D68667\",\"version\":0,\"maxAge\":-1,\"secure\":false,\"httpOnly\":false}\n','2018-09-07 15:08:04','2018-09-07 15:08:04'),(560,52,'127.0.0.1',60325,NULL,NULL,'D7494CBBBDB59D4D69472339FCAE531F','Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',NULL,'{\"name\":\"GOLDYSESSION\",\"value\":\"D7494CBBBDB59D4D69472339FCAE531F\",\"version\":0,\"maxAge\":-1,\"secure\":false,\"httpOnly\":false}\n{\"name\":\"GOLDYSESSION\",\"value\":\"B0382B359D5C4499F99AD4A78385EDE0\",\"version\":0,\"maxAge\":-1,\"secure\":false,\"httpOnly\":false}\n','2018-09-13 11:13:35','2018-09-13 11:13:35');
/*!40000 ALTER TABLE `click_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `encoded_url`
--

LOCK TABLES `encoded_url` WRITE;
/*!40000 ALTER TABLE `encoded_url` DISABLE KEYS */;
INSERT INTO `encoded_url` (`ukey_id`, `redirect_url`, `password`, `hint`, `created_time`, `updated_time`) VALUES (46,'http://localhost:10030/shorturl/','9vohASF+xl0QpnuSeiu59A$Yf/5MrGs6Kc/6bbNvxu4Tg',NULL,'2018-09-05 13:47:17','2018-09-05 13:47:17'),(47,'http://www.naver.com','VhEsfCe2CdVXjg5PH/W+aQ$yJMXkT2o0TRctQJpNC2eqg',NULL,'2018-09-05 14:29:16','2018-09-05 14:29:16'),(48,'https://www.naver.com/','2bu3HoaFSbzXBJUDqtpnTA$Gotzx/KVEWjIpijm+mwkZA',NULL,'2018-09-07 13:59:56','2018-09-07 13:59:56'),(49,'http://www.g9.co.kr/','Ux0idh3vwLGriKHjBdbRQQ$K6QzzlAgYXvdBas6nG+q/w',NULL,'2018-09-07 14:00:46','2018-09-07 14:00:46');
/*!40000 ALTER TABLE `encoded_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fn_table`
--

LOCK TABLES `fn_table` WRITE;
/*!40000 ALTER TABLE `fn_table` DISABLE KEYS */;
INSERT INTO `fn_table` (`id`, `table`) VALUES (1,'redirect_url'),(2,'encoded_url');
/*!40000 ALTER TABLE `fn_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `redirect_url`
--

LOCK TABLES `redirect_url` WRITE;
/*!40000 ALTER TABLE `redirect_url` DISABLE KEYS */;
INSERT INTO `redirect_url` (`ukey_id`, `redirect_url`, `created_time`, `updated_time`) VALUES (27,'https://cdn2.iconfinder.com/data/icons/social-media-network-fill-flat-icon/512/Spring.me-512.png','2018-09-03 13:07:20','2018-09-03 13:07:20'),(28,'http://www.iconarchive.com/download/i63396/cornmanthe3rd/plex/System-documents.ico','2018-09-03 13:11:34','2018-09-03 13:11:34'),(29,'http://icons.iconarchive.com/icons/graphicloads/colorful-long-shadow/256/Home-icon.png','2018-09-03 13:11:47','2018-09-03 13:11:47'),(30,'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSReA7iLGG3sXmIvIaj5Bsk3mK3y23mc7F_d6LH0vy-Z-2jZBaI','2018-09-03 13:11:58','2018-09-03 13:11:58'),(31,'https://www.vextras.com/wp-content/uploads/2013/03/Link.png','2018-09-03 13:12:12','2018-09-03 13:12:12'),(32,'https://cdn4.iconfinder.com/data/icons/basic-user-interface-2/512/User_Interface-40-512.png','2018-09-03 13:12:20','2018-09-03 13:12:20'),(33,'http://cdn.onlinewebfonts.com/svg/img_335847.png','2018-09-03 13:12:29','2018-09-03 13:12:29'),(34,'https://scontent-sea1-1.cdninstagram.com/vp/9a97478ca80c31a7f5279339781e5417/5BEF13D6/t51.2885-15/e35/s480x480/38041282_2117809018480108_4726067285351989248_n.jpg','2018-09-03 13:12:39','2018-09-03 13:12:39'),(35,'http://localhost:10030/shorturl/resources/custom/image/logo.png','2018-09-03 13:14:27','2018-09-03 13:14:27'),(40,'http://localhost:10030/shorturl/','2018-09-03 14:06:15','2018-09-03 14:06:15'),(41,'http://localhost:10030/shorturl/r9HD','2018-09-05 10:49:22','2018-09-05 10:49:22'),(50,'https://image.flaticon.com/icons/svg/115/115714.svg','2018-09-08 05:01:39','2018-09-08 05:03:04'),(51,'https://cdn.onlinewebfonts.com/svg/img_242780.png','2018-09-08 05:04:54','2018-09-08 05:04:54'),(52,'https://cdn0.iconfinder.com/data/icons/octicons/1024/repo-force-push-512.png','2018-09-13 11:09:10','2018-09-13 11:09:10');
/*!40000 ALTER TABLE `redirect_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ukey`
--

LOCK TABLES `ukey` WRITE;
/*!40000 ALTER TABLE `ukey` DISABLE KEYS */;
INSERT INTO `ukey` (`id`, `url_key`, `fn_table_id`, `click_count`, `created_time`, `updated_time`) VALUES (29,'3ZM',1,106,'2018-09-03 13:11:47','2018-09-13 11:19:20'),(48,'40i',2,1,'2018-09-07 13:59:56','2018-09-07 14:00:16'),(40,'9HD',1,9,'2018-09-03 14:06:15','2018-09-05 13:55:59'),(28,'bp9',1,107,'2018-09-03 13:11:34','2018-09-13 11:19:20'),(27,'gZu',1,102,'2018-09-03 13:07:20','2018-09-13 11:19:20'),(35,'J61',1,112,'2018-09-03 13:14:27','2018-09-13 11:19:22'),(47,'KKC',2,1,'2018-09-05 14:29:16','2018-09-05 14:29:30'),(34,'Krz',1,102,'2018-09-03 13:12:39','2018-09-13 11:19:20'),(49,'n6c',2,1,'2018-09-07 14:00:45','2018-09-07 14:01:04'),(46,'pio',2,0,'2018-09-05 13:47:16','2018-09-05 13:47:16'),(51,'S0i',1,17,'2018-09-08 05:04:54','2018-09-13 11:19:20'),(52,'UbZ',1,10,'2018-09-13 11:09:10','2018-09-13 11:19:20'),(41,'Vdn',1,10,'2018-09-05 10:49:22','2018-09-05 11:29:48'),(50,'w8M',1,19,'2018-09-08 05:01:39','2018-09-13 11:19:20'),(32,'Wki',1,104,'2018-09-03 13:12:20','2018-09-13 11:19:20'),(33,'Yhy',1,102,'2018-09-03 13:12:29','2018-09-13 11:19:20'),(31,'yl0',1,105,'2018-09-03 13:12:12','2018-09-13 11:19:20'),(30,'z6H',1,105,'2018-09-03 13:11:58','2018-09-13 11:19:20');
/*!40000 ALTER TABLE `ukey` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-15 21:34:50
