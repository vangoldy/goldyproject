/**
 * FileName : {@link AccountServiceImplTest}.java
 * Created : 2019. 2. 9. 오후 9:30:40
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.account;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.times;

import java.time.LocalDate;
import java.time.Month;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.goldy.gtils.exception.LogicException;
import com.goldy.gtils.type.Gender;
import com.goldy.project.server.sso.model.entity.AccountAccessHistoryVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.service.jpa.DataSourceConfig;
import com.goldy.project.server.sso.service.jpa.dao.AccountAccessHistoryDao;
import com.goldy.project.server.sso.service.jpa.dao.AccountDao;
import com.goldy.project.server.sso.service.jpa.dao.AuthGeneralDao;
import com.goldy.project.server.sso.service.jpa.dao.PersonalInformationDao;

@RunWith(SpringRunner.class)
@ContextConfiguration(
	classes = {
			AccountServiceImpl.class,
			DataSourceConfig.class,
	})
@DataJpaTest
@SuppressWarnings("javadoc")
public class AccountServiceImplTest {

	@SpyBean
	private AccountServiceImpl target;

	@Autowired
	private AccountAccessHistoryDao accountAccessHistoryDao;

	@Autowired
	private AccountDao accountDao;

	@Autowired
	private AuthGeneralDao authGeneralDao;

	@Autowired
	private PersonalInformationDao personalInformationDao;

	@Rule
	public ExpectedException ee = ExpectedException.none();

	private AccountAccessHistoryVo createHistoryVo(long accountId) {

		final MockHttpServletRequest servletRequest = new MockHttpServletRequest();
		servletRequest.addHeader("user-agent", "TEST_AGENT");
		servletRequest.setLocalAddr("TEST_ADDR");
		servletRequest.setCookies(new Cookie("JSESSIONID", "TEST_JSESSIONID"));
		servletRequest.setLocalName("TEST_LOCALNAME");
		servletRequest.setSession(new MockHttpSession());
		return this.target.createAccessHistory(accountId, servletRequest);
	}

	/**
	 * Test method for
	 * {@link AccountServiceImpl#createAccessHistory(long, HttpServletRequest)}.
	 */
	@Test
	public void testCreateAccessHistory() {

		final MockHttpServletRequest servletRequest = new MockHttpServletRequest();
		servletRequest.addHeader("user-agent", "TEST_AGENT");
		servletRequest.setLocalAddr("TEST_ADDR");
		servletRequest.setCookies(new Cookie("JSESSIONID", "TEST_JSESSIONID"));
		servletRequest.setLocalName("TEST_LOCALNAME");
		servletRequest.setSession(new MockHttpSession());

		final AccountAccessHistoryVo actual = this.target.createAccessHistory(999L, servletRequest);

		final Optional<AccountAccessHistoryVo> savedHistory = this.accountAccessHistoryDao.findById(actual.getId());
		Assert.assertTrue("입력한 데이터가 DB에 저장되었는지 확인", savedHistory.isPresent());
	}

	/**
	 * Test method for {@link AccountServiceImpl#flaggedLoginSuccess(long)}.
	 * 이미 flag가 false이면 정상 수행
	 */
	@Test
	public void testFlaggedLoginSuccess() {

		this.createHistoryVo(999L);

		this.target.flaggedLoginSuccess(999L);

		final AccountAccessHistoryVo actual = this.accountAccessHistoryDao
			.findTop1ByAccountIdOrderByCreatedTimeDescIdDesc(999L).orElseThrow(AssertionError::new);
		Assert.assertTrue("로그인 성공 확인", actual.isLoginSuccessFlag());
	}

	/**
	 * Test method for {@link AccountServiceImpl#flaggedLoginSuccess(long)}.
	 * 이미 flag가 true일때 예외발생
	 */
	@Test
	public void testFlaggedLoginSuccessAlreadyTrue() {

		this.ee.expect(LogicException.class);
		this.ee.expectMessage("두번 true되면 로직 오류");

		final AccountAccessHistoryVo savedHistory = this.createHistoryVo(999L);
		savedHistory.setLoginSuccessFlag(true);
		this.accountAccessHistoryDao.save(savedHistory);

		this.target.flaggedLoginSuccess(999L);

		final AccountAccessHistoryVo actual = this.accountAccessHistoryDao
			.findTop1ByAccountIdOrderByCreatedTimeDescIdDesc(999L).orElseThrow(AssertionError::new);
		Assert.assertTrue("로그인 성공 확인", actual.isLoginSuccessFlag());
	}

	/**
	 * Test method for
	 * {@link AccountServiceImpl#get(Long)}.
	 */
	@Test
	public void testGet() {

		// 가상 계정 생성
		final AccountVo account = new AccountVo();
		account.setLoginEmail("TEST_EMAIL");
		final AccountVo savedAccount = this.accountDao.save(account);

		final Optional<AccountVo> optionalActual = this.target.get(savedAccount.getId());

		Assert.assertTrue("DB에 있는 계정을 잘 가져옴", optionalActual.isPresent());

		final AccountVo actual = optionalActual.get();
		Assert.assertThat("가져온 계정이 올바른 값임", actual.getLoginEmail(), is("TEST_EMAIL"));
	}

	/**
	 * Test method for
	 * {@link AccountServiceImpl#getAuthGeneral(long)}.
	 */
	@Test
	public void testGetAuthGeneral() {

		final AuthGeneralVo authGeneral = new AuthGeneralVo();
		authGeneral.setAccountId(999L);
		authGeneral.setLoginId("TEST_ID");
		authGeneral.setLoginPassword("TEST_PASSWORD");

		this.authGeneralDao.save(authGeneral);

		final Optional<AuthGeneralVo> optionalActual = this.target.getAuthGeneral(999L);
		Assert.assertTrue("DB에 있는 값을 잘 가져옴", optionalActual.isPresent());

		final AuthGeneralVo actual = optionalActual.get();
		Assert.assertThat("가져온 계정이 올바른 값임", actual.getLoginId(), is("TEST_ID"));
		Assert.assertThat("가져온 계정이 올바른 값임", actual.getLoginPassword(), is("TEST_PASSWORD"));
	}

	/**
	 * Test method for
	 * {@link AccountServiceImpl#getByEmail(java.lang.String)}.
	 */
	@Test
	public void testGetByEmail() {

		// 가상 계정 생성
		final AccountVo account = new AccountVo();
		account.setLoginEmail("a@b.com");
		final AccountVo test = this.accountDao.save(account);

		final Optional<AccountVo> optionalActual = this.target.getByEmail("a@b.com");

		Assert.assertTrue("DB에 있는 계정을 잘 가져옴", optionalActual.isPresent());

		final AccountVo actual = optionalActual.get();
		Assert.assertThat("가져온 계정이 올바른 값임", actual.getLoginEmail(), is("a@b.com"));
	}

	/**
	 * Test method for
	 * {@link AccountServiceImpl#getPersonalInformation(long)}.
	 */
	@Test
	public void testGetPersonalInformation() {

		final PersonalInformationVo pi = new PersonalInformationVo();
		pi.setAccountId(999L);
		pi.setBirthday(LocalDate.of(1990, Month.of(10), 7));
		pi.setGender(Gender.MALE);
		pi.setNickname("GOLDY");
		pi.setPhoneNumber("010-5055-6284");
		pi.setUserName("금정현");

		this.personalInformationDao.save(pi);

		final Optional<PersonalInformationVo> optionalActual = this.target.getPersonalInformation(999L);

		Assert.assertTrue("DB에 있는 계정을 잘 가져옴", optionalActual.isPresent());

		final PersonalInformationVo actual = optionalActual.get();
		Assert.assertThat("가져온 PI의 생년월일 비교", actual.getBirthday(), is(LocalDate.of(1990, Month.of(10), 7)));
		Assert.assertThat("가져온 PI의 성별 비교", actual.getGender(), is(Gender.MALE));
		Assert.assertThat("가져온 PI의 닉네임 비교", actual.getNickname(), is("GOLDY"));
		Assert.assertThat("가져온 PI의 연락처 비교", actual.getPhoneNumber(), is("010-5055-6284"));
		Assert.assertThat("가져온 PI의 이름 비교", actual.getUserName(), is("금정현"));

	}

	/**
	 * Test method for {@link AccountServiceImpl#incrementLoginCount(long)}.
	 */
	@Test
	public void testIncrementLoginCount() {

		final AccountVo virtualAccount = new AccountVo();
		virtualAccount.setLoginCount(100);
		virtualAccount.setLoginEmail("TEST_EMAIL");

		final AccountVo savedAccount = this.accountDao.save(virtualAccount);

		this.target.incrementLoginCount(savedAccount.getId());

		final AccountVo actual = this.accountDao.findById(savedAccount.getId())
			.orElseThrow(AssertionError::new);

		Assert.assertThat("로그인 카운트가 100에서 101로 증가", actual.getLoginCount(), is(101L));
	}

	/**
	 * Test method for {@link AccountServiceImpl#initializeWrongPasswordCount(long)}.
	 * wtongPasswordCount 가 0이 아닐 때 if문을 수행해야한다.
	 */
	@Test
	public void testInitializeWrongPasswordCountCount1000() {

		final AuthGeneralVo virtualAuthGeneral = new AuthGeneralVo();
		virtualAuthGeneral.setAccountId(999L);
		virtualAuthGeneral.setWrongPasswordCount(1_000);

		this.authGeneralDao.save(virtualAuthGeneral);

		this.target.initializeWrongPasswordCount(999L);

		final AuthGeneralVo actual = this.authGeneralDao.findById(999L).orElseThrow(AssertionError::new);

		Assert.assertThat("해당 함수를 수행한다면 wrongPasswordCount는 항상 0이어야한다.", actual.getWrongPasswordCount(), is(0));
	}

	/**
	 * Test method for {@link AccountServiceImpl#initializeWrongPasswordCount(long)}.
	 * wtongPasswordCount 가 0일때 if문을 수행하지 않는다.
	 */
	@Test
	public void testInitializeWrongPasswordCountCountZero() {

		final AuthGeneralVo virtualAuthGeneral = new AuthGeneralVo();
		virtualAuthGeneral.setAccountId(999L);
		virtualAuthGeneral.setWrongPasswordCount(0);

		this.authGeneralDao.save(virtualAuthGeneral);

		this.target.initializeWrongPasswordCount(999L);

		final AuthGeneralVo actual = this.authGeneralDao.findById(999L).orElseThrow(AssertionError::new);

		Assert.assertThat("해당 함수를 수행한다면 wrongPasswordCount는 항상 0이어야한다.", actual.getWrongPasswordCount(), is(0));
	}

	/**
	 * Test method for {@link AccountServiceImpl#successLogin(long)}.
	 */
	@Test
	public void testSuccessLogin() {

		final long accountId = 999L;
		Mockito.doNothing()
			.when(this.target)
			.incrementLoginCount(accountId);

		Mockito.doNothing()
			.when(this.target)
			.initializeWrongPasswordCount(accountId);

		Mockito.doNothing()
			.when(this.target)
			.flaggedLoginSuccess(accountId);

		this.target.successLogin(accountId);

		Mockito.verify(this.target, times(1)).incrementLoginCount(accountId);
		Mockito.verify(this.target, times(1)).initializeWrongPasswordCount(accountId);
		Mockito.verify(this.target, times(1)).flaggedLoginSuccess(accountId);
	}

}
