package com.goldy.project.server.sso.model.form;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.validation.Valid;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.goldy.gtils.does.SonarHelper;
import com.goldy.gtils.response.Response;
import com.goldy.project.server.sso.model.form.SignupFormTest.VirtualController;
import com.google.gson.JsonObject;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = VirtualController.class)
@SuppressWarnings("javadoc")
public class SignupFormTest {

	@RestController
	public static class VirtualController {

		@ExceptionHandler(BindException.class)
		public ResponseEntity<String> exceptionHandler(BindException throwable) {

			final BindingResult bindingResult = throwable.getBindingResult();

			return Response.badRequest(bindingResult);
		}

		@ExceptionHandler
		public ResponseEntity<String> exceptionHandler(Exception throwable) {

			System.out.println(throwable.getMessage());

			return Response.badRequest(throwable.getMessage());
		}

		@ExceptionHandler(MethodArgumentNotValidException.class)
		public ResponseEntity<String> exceptionHandler(MethodArgumentNotValidException throwable) {

			final BindingResult bindingResult = throwable.getBindingResult();

			return Response.badRequest(bindingResult);
		}

		@PostMapping(value = "/test", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
		public ResponseEntity<String> postEncodedUrlType(@Valid SignupForm model) {

			SonarHelper.unuse(model);

			return ResponseEntity.ok("success");
		}

		@PostMapping(value = "/test", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
		public ResponseEntity<String> postJsonType(@Valid @RequestBody SignupForm model) {

			SonarHelper.unuse(model);

			return ResponseEntity.ok("success");
		}

	}

	@Autowired
	private VirtualController target;

	private MockMvc mockMvc;

	@Before
	public void setUp() {

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target).build();
	}

	@Test
	public void testEmptyEncodedUrl() throws Exception {

		final ResultActions resultActions = this.mockMvc.perform(
			post("/test")
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.content(""))
			.andDo(print());

		resultActions.andExpect(status().isBadRequest());
		resultActions.andExpect(jsonPath("$.errorFields.length()").value(3));
		resultActions.andExpect(jsonPath("$.errorFields[\"email\"]").value("must not be empty"));
		resultActions.andExpect(jsonPath("$.errorFields[\"gender\"]").value("must not be null"));
		resultActions.andExpect(jsonPath("$.errorFields[\"password\"]").value("must not be empty"));
	}

	@Test
	public void testEmptyJson() throws Exception {

		final ResultActions resultActions = this.mockMvc.perform(
			post("/test")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("{}"))
			.andDo(print());

		resultActions.andExpect(status().isBadRequest());
		resultActions.andExpect(jsonPath("$.errorFields.length()").value(3));
		resultActions.andExpect(jsonPath("$.errorFields[\"email\"]").value("must not be empty"));
		resultActions.andExpect(jsonPath("$.errorFields[\"gender\"]").value("must not be null"));
		resultActions.andExpect(jsonPath("$.errorFields[\"password\"]").value("must not be empty"));
	}

	@Test
	public void testInvalidPassword() throws Exception {

		final JsonObject content = new JsonObject();
		content.addProperty("password", "invalid");

		final ResultActions resultActions = this.mockMvc.perform(
			post("/test")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content.toString()))
			.andDo(print());

		resultActions.andExpect(status().isBadRequest());
		resultActions.andExpect(jsonPath("$.errorFields.length()").value(4));
		resultActions.andExpect(jsonPath("$.errorFields[\"email\"]").value("must not be empty"));
		resultActions.andExpect(jsonPath("$.errorFields[\"gender\"]").value("must not be null"));
		resultActions.andExpect(jsonPath("$.errorFields[\"equalPasswordRepeat\"]").value("must be true (false)"));
		resultActions.andExpect(jsonPath("$.errorFields[\"password\"]", Matchers.containsString("must")));
	}

	@Test
	public void testNotEqualsPasswordAndRepeat() throws Exception {

		final JsonObject content = new JsonObject();
		content.addProperty("password", "!AcbcQef9");
		content.addProperty("passwordRepeat", "!AcbcQef99");

		final ResultActions resultActions = this.mockMvc.perform(
			post("/test")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content.toString()))
			.andDo(print());

		resultActions.andExpect(status().isBadRequest());
		resultActions.andExpect(jsonPath("$.errorFields.length()").value(3));
		resultActions.andExpect(jsonPath("$.errorFields[\"email\"]").value("must not be empty"));
		resultActions.andExpect(jsonPath("$.errorFields[\"gender\"]").value("must not be null"));
		resultActions.andExpect(jsonPath("$.errorFields[\"equalPasswordRepeat\"]").value("must be true (false)"));
	}

	@Test
	public void testValidFullValuesForJsonType() throws Exception {

		final JsonObject content = new JsonObject();
		content.addProperty("email", "hokkk01@naver.com");
		content.addProperty("password", "!AcbcQef9");
		content.addProperty("passwordRepeat", "!AcbcQef9");
		content.addProperty("nickName", "Goldy");
		content.addProperty("birthday", "1990-10-07");
		content.addProperty("phoneNumber", "010-5055-6284");
		content.addProperty("userName", "금정현");
		content.addProperty("gender", "MALE");

		final ResultActions resultActions = this.mockMvc.perform(
			post("/test")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content.toString()))
			.andDo(print());

		resultActions.andExpect(status().isOk());
		resultActions.andExpect(jsonPath("$.errorFields").doesNotHaveJsonPath());
		resultActions.andExpect(content().string("success"));
	}

	@Test
	public void testValidMinValuesForJsonType() throws Exception {

		final JsonObject content = new JsonObject();
		content.addProperty("email", "hokkk01@naver.com");
		content.addProperty("password", "!AcbcQef9");
		content.addProperty("passwordRepeat", "!AcbcQef9");
		content.addProperty("gender", "MALE");

		final ResultActions resultActions = this.mockMvc.perform(
			post("/test")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content.toString()))
			.andDo(print());

		resultActions.andExpect(status().isOk());
		resultActions.andExpect(jsonPath("$.errorFields").doesNotHaveJsonPath());
		resultActions.andExpect(content().string("success"));
	}

}
