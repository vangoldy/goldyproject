/**
 * FileName : {@link AccountAccessHistoryDaoTest}.java
 * Created : 2019. 2. 9. 오후 11:25:40
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.jpa.dao;

import static org.hamcrest.CoreMatchers.is;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.goldy.project.server.sso.model.entity.AccountAccessHistoryVo;
import com.goldy.project.server.sso.service.jpa.DataSourceConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DataSourceConfig.class)
@DataJpaTest
@SuppressWarnings("javadoc")
public class AccountAccessHistoryDaoTest {

	@Autowired
	private AccountAccessHistoryDao target;

	/**
	 * Test method for
	 * {@link AccountAccessHistoryDao#findByAccountId(long, Pageable)}.
	 */
	@Test
	public void testFindByAccountId() {

		final AccountAccessHistoryVo vo1 = new AccountAccessHistoryVo();
		vo1.setAccountId(999L);
		vo1.setAgent("TEST_AGENT1");
		vo1.setIp("TEST_IP1");
		vo1.setName("TEST_NAME1");
		vo1.setAuthType("TEST_AUTH_TYPE1");
		vo1.setJsessionId("TEST_J_SESSION_ID1");
		vo1.setSessionId("TEST_SESSION_ID1");

		final AccountAccessHistoryVo vo2 = new AccountAccessHistoryVo();
		vo2.setAccountId(999L);
		vo2.setAgent("TEST_AGENT2");
		vo2.setIp("TEST_IP2");
		vo2.setName("TEST_NAME2");
		vo2.setAuthType("TEST_AUTH_TYPE2");
		vo2.setJsessionId("TEST_J_SESSION_ID2");
		vo2.setSessionId("TEST_SESSION_ID2");

		final AccountAccessHistoryVo vo3 = new AccountAccessHistoryVo();
		vo3.setAccountId(888L);
		vo3.setAgent("TEST_AGENT3");
		vo3.setIp("TEST_IP3");
		vo3.setName("TEST_NAME3");
		vo3.setAuthType("TEST_AUTH_TYPE3");
		vo3.setJsessionId("TEST_J_SESSION_ID3");
		vo3.setSessionId("TEST_SESSION_ID3");

		this.target.save(vo1);
		this.target.save(vo2);
		this.target.save(vo3);

		final Page<AccountAccessHistoryVo> actual = this.target.findByAccountId(999L, Pageable.unpaged());
		Assert.assertThat("999에 대한 값은 2개만 해당한다", actual.getTotalElements(), is(2L));

		final Collection<String> roles = actual.stream()
			.map(AccountAccessHistoryVo::getName)
			.collect(Collectors.toList());
		Assert.assertTrue("TEST_NAME1가 포함되어있는지 확인", roles.contains("TEST_NAME1"));
		Assert.assertTrue("TEST_NAME2가 포함되어있는지 확인", roles.contains("TEST_NAME2"));
		Assert.assertFalse("TEST_NAME3가 제외 되어있는지 확인", roles.contains("TEST_NAME3"));

	}

	/**
	 * Test method for
	 * {@link AccountAccessHistoryDao#findTop1ByAccountIdOrderByCreatedTimeDescIdDesc(long)}.
	 */
	@Test
	public void testFindTop1ByAccountIdOrderByCreatedTimeDesc() {

		final AccountAccessHistoryVo vo1 = new AccountAccessHistoryVo();
		vo1.setAccountId(999L);
		vo1.setAgent("TEST_AGENT1");
		vo1.setIp("TEST_IP1");
		vo1.setName("TEST_NAME1");
		vo1.setAuthType("TEST_AUTH_TYPE1");
		vo1.setJsessionId("TEST_J_SESSION_ID1");
		vo1.setSessionId("TEST_SESSION_ID1");

		final AccountAccessHistoryVo vo2 = new AccountAccessHistoryVo();
		vo2.setAccountId(999L);
		vo2.setAgent("TEST_AGENT2");
		vo2.setIp("TEST_IP2");
		vo2.setName("TEST_NAME2");
		vo2.setAuthType("TEST_AUTH_TYPE2");
		vo2.setJsessionId("TEST_J_SESSION_ID2");
		vo2.setSessionId("TEST_SESSION_ID2");

		final AccountAccessHistoryVo vo3 = new AccountAccessHistoryVo();
		vo3.setAccountId(888L);
		vo3.setAgent("TEST_AGENT3");
		vo3.setIp("TEST_IP3");
		vo3.setName("TEST_NAME3");
		vo3.setAuthType("TEST_AUTH_TYPE3");
		vo3.setJsessionId("TEST_J_SESSION_ID3");
		vo3.setSessionId("TEST_SESSION_ID3");

		this.target.save(vo1);
		this.target.save(vo2);
		this.target.save(vo3);

		final Optional<AccountAccessHistoryVo> optionalActual = this.target
			.findTop1ByAccountIdOrderByCreatedTimeDescIdDesc(999L);

		Assert.assertTrue("999L에 해당하는 값은 2개이고 정상적으로 가져올 수 있으므로 true가 나와야한다", optionalActual.isPresent());

		final AccountAccessHistoryVo actual = optionalActual.get();

		Assert.assertThat("가장 최신 정보를 가져오기때문에 vo2를 반환함", actual.getName(), is("TEST_NAME2"));

	}

}
