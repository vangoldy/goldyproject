/**
 * FileName : {@link SecurityServiceImplTest}.java
 * Created : 2019. 2. 10. 오후 12:55:24
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.goldy.gtils.exception.NoPrincipalException;
import com.goldy.gtils.type.Gender;
import com.goldy.project.server.sso.model.entity.AccountRoleVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.service.account.AccountService;
import com.goldy.project.server.sso.service.jpa.DataSourceConfig;
import com.goldy.project.server.sso.service.jpa.dao.AccountRoleDao;
import com.goldy.project.server.sso.service.security.role.SpringSecurityRole;

@RunWith(SpringRunner.class)
@ContextConfiguration(
	classes = {
			SecurityServiceImpl.class,
			DataSourceConfig.class,
	})
@DataJpaTest
@SuppressWarnings("javadoc")
public class SecurityServiceImplTest {

	@SpyBean
	private SecurityServiceImpl target;

	@MockBean
	private AccountService accountService;

	@Autowired
	private AccountRoleDao accountRoleDao;

	private void saveRoles(long accountId, String... roles) {

		Arrays.stream(roles)
			.map((String role) -> {
				final AccountRoleVo accountRole = new AccountRoleVo();
				accountRole.setAccountId(accountId);
				accountRole.setRole(role);
				return accountRole;
			})
			.forEach(vo -> this.accountRoleDao.save(vo));
	}

	/**
	 * Test method for {@link SecurityServiceImpl#getAllRoles(long)}.
	 */
	@Test
	public void testGetAllRoles() {

		this.saveRoles(999L, "GROUP_1_MASTER", "ROLE_ADMIN", "ROLE_USER");

		final Collection<String> actual = this.target.getAllRoles(999L);

		Assert.assertTrue("USER_ROLE 포함함", actual.contains("ROLE_USER"));
		Assert.assertTrue("USER_ADMIN 포함함", actual.contains("ROLE_ADMIN"));
		Assert.assertTrue("GROUP_1_MASTER 포함함", actual.contains("GROUP_1_MASTER"));
	}

	/**
	 * Test method for {@link SecurityServiceImpl#getSpringSecurityRoles(long)}.
	 */
	@Test
	public void testGetSpringSecurityRoles() {

		this.saveRoles(999L, "GROUP_1_MASTER", "ROLE_ADMIN", "ROLE_USER");

		final Collection<SpringSecurityRole> actual = this.target.getSpringSecurityRoles(999L);

		Assert.assertThat("ROLE 개수", actual.size(), is(2));
		Assert.assertTrue("USER_ROLE 포함함", actual.contains(SpringSecurityRole.USER));
		Assert.assertTrue("USER_ADMIN 포함함", actual.contains(SpringSecurityRole.ADMIN));

	}

	/**
	 * Test method for {@link SecurityServiceImpl#getUserDetail()}.
	 */
	@Test
	public void testGetUserDetail() throws NoPrincipalException {

		final SecurityUserDetail principal = Mockito.mock(SecurityUserDetail.class);
		final Authentication authentication = new TestingAuthenticationToken(principal, "TEST_PASSWORD");
		SecurityContextHolder.getContext().setAuthentication(authentication);

		final SecurityUserDetail actual = this.target.getUserDetail();

		Assert.assertThat("사용자 정보 일치 여부 확인", actual, is(principal));
	}

	/**
	 * Test method for {@link SecurityServiceImpl#getUserDetail(Authentication)}.
	 */
	@Test
	public void testGetUserDetailAuthentication() throws NoPrincipalException {

		final SecurityUserDetail principal = Mockito.mock(SecurityUserDetail.class);
		final Authentication authentication = new TestingAuthenticationToken(principal, "TEST_PASSWORD");

		final SecurityUserDetail actual = this.target.getUserDetail(authentication);

		Assert.assertThat("사용자 정보 일치 여부 확인", actual, is(principal));
	}

	/**
	 * Test method for {@link SecurityServiceImpl#login(long)}.
	 */
	@Test
	public void testLogin() throws NoPrincipalException {

		final SecurityUserDetail securityUserDetail = Mockito.mock(SecurityUserDetail.class);

		Mockito.doReturn(securityUserDetail)
			.when(this.target)
			.toSecurityUserDetail(999L);

		this.target.login(999L);

		final Object actual = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		Assert.assertThat("로그인 정상 여부 확인", actual, is(securityUserDetail));

	}

	/**
	 * Test method for {@link SecurityServiceImpl#logout(Authentication, HttpServletRequest, HttpServletResponse)}.
	 */
	@Test
	public void testLogout() throws NoPrincipalException {

		final SecurityUserDetail principal = Mockito.mock(SecurityUserDetail.class);
		final Authentication authentication = new TestingAuthenticationToken(principal, "TEST_PASSWORD");

		Mockito.doReturn(principal)
			.when(this.target)
			.toSecurityUserDetail(999L);
		// 로그아웃 전 로그인 먼저 수행
		this.target.login(999L);

		final HttpServletRequest servletRequest = new MockHttpServletRequest();
		final HttpServletResponse servletResponse = new MockHttpServletResponse();
		this.target.logout(authentication, servletRequest, servletResponse);

		final Authentication actual = SecurityContextHolder.getContext().getAuthentication();

		Assert.assertThat("로그아웃 후 Authentication이 null이어야 함", actual, nullValue());
	}

	/**
	 * Test method for {@link SecurityServiceImpl#toSecurityUserDetail(long)}.
	 */
	@Test
	public void testToSsoUserDetail() throws NoPrincipalException {

		final long accountId = 999L;

		this.saveRoles(accountId, "GROUP_1_MASTER", "ROLE_ADMIN", "ROLE_USER");

		final AccountVo account = new AccountVo();
		account.setId(accountId);
		account.setLoginEmail("TEST_EMAIL");

		final AuthGeneralVo authGeneralVo = new AuthGeneralVo();
		authGeneralVo.setAccountId(accountId);
		authGeneralVo.setLoginId("TEST_EMAIL");
		authGeneralVo.setLoginPassword("TEST_PASSWORD");

		final PersonalInformationVo pi = new PersonalInformationVo();
		pi.setAccountId(accountId);
		pi.setGender(Gender.MALE);

		Mockito.doReturn(Optional.of(account))
			.when(this.accountService)
			.get(accountId);

		Mockito.doReturn(Optional.of(authGeneralVo))
			.when(this.accountService)
			.getAuthGeneral(accountId);

		Mockito.doReturn(Optional.of(pi))
			.when(this.accountService)
			.getPersonalInformation(accountId);

		final SecurityUserDetail actual = this.target.toSecurityUserDetail(accountId);

		Assert.assertThat("사용자 로그인 ID (Username) 값 비교", actual.getUsername(), is("TEST_EMAIL"));
		Assert.assertThat("사용자 로그인 패스워드 값 비교", actual.getPassword(), is("TEST_PASSWORD"));

		final List<String> roles = actual.getAuthorities().stream().map(grant -> grant.getAuthority())
			.collect(Collectors.toList());
		Assert.assertThat("ROLE 비교", roles, hasItems("GROUP_1_MASTER", "ROLE_ADMIN", "ROLE_USER"));

	}

}
