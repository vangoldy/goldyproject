/**
 * FileName : {@link AccountRoleDaoTest}.java
 * Created : 2019. 2. 9. 오후 9:35:39
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.jpa.dao;

import static org.hamcrest.CoreMatchers.is;

import java.util.Collection;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.goldy.project.server.sso.model.entity.AccountRoleVo;
import com.goldy.project.server.sso.service.jpa.DataSourceConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = DataSourceConfig.class)
@DataJpaTest
@SuppressWarnings("javadoc")
public class AccountRoleDaoTest {

	@Autowired
	private AccountRoleDao target;

	/**
	 * Test method for
	 * {@link AccountRoleDao#existsByAccountIdAndRole(java.lang.Long, java.lang.String)}.
	 */
	@Test
	public void testExistsByAccountIdAndRoleExist() {

		final AccountRoleVo vo1 = new AccountRoleVo();
		vo1.setAccountId(999L);
		vo1.setRole("ROLE_TEST");
		this.target.save(vo1);

		final boolean actual = this.target.existsByAccountIdAndRole(999L, "ROLE_TEST");
		Assert.assertTrue("999/ROLE_TEST에 대한 값이 있으므로 true가 반환되야한다.", actual);
	}

	/**
	 * Test method for
	 * {@link AccountRoleDao#existsByAccountIdAndRole(java.lang.Long, java.lang.String)}.
	 */
	@Test
	public void testExistsByAccountIdAndRoleNonexist() {

		final AccountRoleVo vo1 = new AccountRoleVo();
		vo1.setAccountId(888L);
		vo1.setRole("ROLE_TEST");
		this.target.save(vo1);

		final boolean actual = this.target.existsByAccountIdAndRole(999L, "ROLE_TEST");
		Assert.assertFalse("999/ROLE_TEST에 대한 값이 있으므로 false가 반환되야한다.", actual);
	}

	/**
	 * Test method for
	 * {@link AccountRoleDao#findByAccountId(long, Pageable)}.
	 */
	@Test
	public void testFindByAccountId() {

		final AccountRoleVo vo1 = new AccountRoleVo();
		vo1.setAccountId(999L);
		vo1.setRole("ROLE_TEST");

		final AccountRoleVo vo2 = new AccountRoleVo();
		vo2.setAccountId(999L);
		vo2.setRole("ROLE_TESS");

		final AccountRoleVo vo3 = new AccountRoleVo();
		vo3.setAccountId(888L);
		vo3.setRole("ROLE_INVALID");

		this.target.save(vo1);
		this.target.save(vo2);
		this.target.save(vo3);

		final Page<AccountRoleVo> actual = this.target.findByAccountId(999L, Pageable.unpaged());
		Assert.assertThat("999에 대한 값은 2개만 해당한다", actual.getTotalElements(), is(2L));

		final Collection<String> roles = actual.stream()
			.map(AccountRoleVo::getRole)
			.collect(Collectors.toList());
		Assert.assertTrue("ROLE_TEST가 포함되어있는지 확인", roles.contains("ROLE_TEST"));
		Assert.assertTrue("ROLE_TESS가 포함되어있는지 확인", roles.contains("ROLE_TESS"));
		Assert.assertFalse("ROLE_INVALID가 제외 되어있는지 확인", roles.contains("ROLE_INVALID"));

	}

}
