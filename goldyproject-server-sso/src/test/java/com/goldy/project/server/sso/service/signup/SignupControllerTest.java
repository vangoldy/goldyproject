/**
 * FileName : {@link SignupControllerTest}.java
 * Created : 2019. 2. 4. 오전 12:34:20
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.signup;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.mock.env.MockEnvironment;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ViewResolver;

import com.goldy.gtils.exception.NoPrincipalException;
import com.goldy.gtils.user.service.signup.EmailAuthHelperService;
import com.goldy.gtils.user.service.signup.controller.SignupException;
import com.goldy.project.module.webcommon.config.advice.WebCommonBadRequestAdvice;
import com.goldy.project.module.webcommon.config.advice.WebCommonServerErrorAdvice;
import com.goldy.project.module.webcommon.config.commonbean.JspViewResolver;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.form.SignupForm;
import com.goldy.project.server.sso.service.security.SecurityServiceImpl;
import com.goldy.project.server.sso.service.security.SecurityUserDetail;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { SignupController.class, MockServletContext.class })
@SuppressWarnings("javadoc")
public class SignupControllerTest {

	private MockMvc mockMvc;

	@SpyBean
	private SignupController target;

	@MockBean
	private SignupServiceImpl signupServiceImpl;

	@MockBean
	private SignupPageHandler signupPageHandler;

	@MockBean
	private EmailAuthHelperService emailAuthHelperService;

	@MockBean
	private SecurityServiceImpl securityServiceImpl;

	@MockBean
	MockEnvironment env;

	private Authentication authentication;

	@Rule
	public ExpectedException ee = ExpectedException.none();

	@SpyBean
	private MockServletContext servletContext;

	@Before
	public void setUp() {

		this.mockMvc = MockMvcBuilders.standaloneSetup(this.target)
			.setViewResolvers(this.viewResolver())
			.setControllerAdvice(new WebCommonBadRequestAdvice(), new WebCommonServerErrorAdvice())
			.build();
		this.authentication = new TestingAuthenticationToken("TEST_EMAIL", "password",
			AuthorityUtils.createAuthorityList("ROLE_USER"));
		SecurityContextHolder.getContext().setAuthentication(this.authentication);
	}

	@Test
	public void testGetSignupForm() throws Exception {

		Mockito.doReturn("EXPECT_PATH")
			.when(this.signupPageHandler)
			.getSignupPage();

		final ResultActions resultActions = this.mockMvc.perform(get("/signup"));

		resultActions.andExpect(status().isOk());
		resultActions.andExpect(view().name("EXPECT_PATH"));
		resultActions.andExpect(model().attributeExists("signupForm"));

		Mockito.verify(this.signupPageHandler).getSignupPage();
	}

	@Test
	public void testProcessEmailAuth() throws Exception {

		Mockito.doReturn(true)
			.when(this.emailAuthHelperService)
			.isValidAuthenticationCode("TEST_EMAIL", "123");

		final SecurityUserDetail userDetail = mock(SecurityUserDetail.class);
		Mockito.doReturn(userDetail)
			.when(this.securityServiceImpl)
			.getUserDetail(this.authentication);

		final AccountVo accountVo = new AccountVo();
		accountVo.setId(999L);
		Mockito.doReturn(accountVo)
			.when(userDetail)
			.getAccount();

		Mockito.doReturn(true)
			.when(this.signupServiceImpl)
			.isAlreadyRegistredUser(999L);

		this.mockMvc.perform(get("/signup/auth/email?authcode=123")
			.principal(this.authentication));

		Mockito.verify(this.signupPageHandler).getAlreadyRegistredUserPage();
		Mockito.verify(this.signupServiceImpl, never()).defineUserRole(999L);
		Mockito.verify(this.signupPageHandler, never()).getFinishEmailAuthPage();
	}

	/**
	 * Test method for
	 * {@link SignupController#processEmailAuth(Authentication, String)}.
	 *
	 * @throws Exception
	 */
	@Test
	public void testProcessEmailAuthFailAuth() throws Exception {

		Mockito.doReturn(false)
			.when(this.emailAuthHelperService)
			.isValidAuthenticationCode("TEST_EMAIL", "123");

		this.mockMvc.perform(get("/signup/auth/email?authcode=123")
			.principal(this.authentication));

		Mockito.verify(this.signupPageHandler).getFailEmailAuthPage();
		Mockito.verify(this.signupServiceImpl, never()).defineUserRole(999L);
		Mockito.verify(this.signupPageHandler, never()).getFinishEmailAuthPage();
	}

	@Test
	public void testProcessEmailAuthNoPrincipalException() throws Exception {

		Mockito.doReturn(true)
			.when(this.emailAuthHelperService)
			.isValidAuthenticationCode("TEST_EMAIL", "123");

		Mockito.doThrow(new NoPrincipalException())
			.when(this.securityServiceImpl)
			.getUserDetail(this.authentication);

		this.mockMvc.perform(get("/signup/auth/email?authcode=123")
			.principal(this.authentication));

		Mockito.verify(this.signupServiceImpl, never()).defineUserRole(999L);
		Mockito.verify(this.signupPageHandler, never()).getFinishEmailAuthPage();

	}

	@Test
	public void testProcessEmailAuthValid() throws Exception {

		Mockito.doReturn(true)
			.when(this.emailAuthHelperService)
			.isValidAuthenticationCode("TEST_EMAIL", "123");

		final SecurityUserDetail userDetail = mock(SecurityUserDetail.class);
		Mockito.doReturn(userDetail)
			.when(this.securityServiceImpl)
			.getUserDetail(this.authentication);

		final AccountVo accountVo = new AccountVo();
		accountVo.setId(999L);
		Mockito.doReturn(accountVo)
			.when(userDetail)
			.getAccount();

		Mockito.doReturn(false)
			.when(this.signupServiceImpl)
			.isAlreadyRegistredUser(999L);

		this.mockMvc.perform(get("/signup/auth/email?authcode=123")
			.principal(this.authentication));

		Mockito.verify(this.signupServiceImpl).defineUserRole(999L);
		Mockito.verify(this.signupPageHandler).getFinishEmailAuthPage();
	}

	@Test
	public void testSignup() throws SignupException {

		final SignupForm signupForm = mock(SignupForm.class);
		final HttpServletRequest servletRequest = new MockHttpServletRequest(this.servletContext);
		final BindingResult bindingResult = mock(BindingResult.class);

		final AccountVo account = new AccountVo();
		account.setId(999L);

		Mockito.doThrow(new SignupException("already regestred and authenticated email"))
			.when(this.signupServiceImpl)
			.signup(signupForm);
		Mockito.doReturn("EXPECT_PATH")
			.when(this.signupPageHandler)
			.getSignupPage();

		final String actual = this.target.signup(signupForm, servletRequest, bindingResult);

		Assert.assertThat("이미 등록된 사용자인 경우 회원가입 페이지로 이동한다", actual, is("EXPECT_PATH"));
		Mockito.verify(bindingResult, times(1)).addError(any());
	}

	@Test
	public void testSignupInvalidForm() throws Exception {

		Mockito.doReturn("EXPECT_PATH")
			.when(this.signupPageHandler)
			.getSignupPage();

		final ResultActions resultActions = this.mockMvc.perform(post("/signup")
			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
			.param("email", "hokkk01")
			.param("password", "a")
			.param("passwordRepeat", "b")
			.param("gender", ""));

		resultActions.andExpect(view().name("EXPECT_PATH"));

		Mockito.verify(this.signupPageHandler).getSignupPage();
		Mockito.verify(this.target, never()).signup(any(), any(), any());
	}

	@Test
	public void testSignupValidForm() throws Exception {

		Mockito.doReturn("EXPECT_PATH")
			.when(this.target)
			.signup(any(), any(), any());

		final ResultActions resultActions = this.mockMvc.perform(post("/signup")
			.contentType(MediaType.APPLICATION_FORM_URLENCODED)
			.param("email", "hokkk01@naver.com")
			.param("password", "!AcbcQef9")
			.param("passwordRepeat", "!AcbcQef9")
			.param("gender", "MALE"));

		resultActions.andExpect(view().name("EXPECT_PATH"));

		Mockito.verify(this.target).signup(any(), any(), any());
		Mockito.verify(this.signupPageHandler, never()).getSignupPage();
	}

	private ViewResolver viewResolver() {

		return new JspViewResolver(this.env);
	}

}
