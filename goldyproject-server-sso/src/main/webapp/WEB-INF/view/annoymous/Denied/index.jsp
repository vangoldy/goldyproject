<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
</head>
<body type="menu-page">
    <div class="center_div">
        <table>
            <tr>
                <td>
                    <div class="header">
                        <div class="content">
                            <c:import url="/WEB-INF/Component/logo.jsp"></c:import>
                        </div>
                    </div>
                    <div class="content">
                        <div class="head_message">HTTP Status 403 - Access is denied</div>
                        <h2>${message}</h2>
                        <div>
                            <c:choose>
                                <c:when test="${empty user}">
                                You do not have permission to access this page!
                            </c:when>
                                <c:otherwise>
                                    ${requester.name} : ${requester.role}
                                    <br />
                                    You do not have permission to access this page!
                            </c:otherwise>
                            </c:choose>
                        </div>
                        <a class="link_label" href="<c:url value="/logout" />">로그아웃</a>
                        <a class="link_label" href="javascript:history.back()">뒤로가기</a>
                    </div>
                </td>
                <td>
                    <img alt="" src="<c:url value='/resources/custom/Image/error_403.png'/>">
                </td>
            </tr>
        </table>
    </div>
</body>
</html>