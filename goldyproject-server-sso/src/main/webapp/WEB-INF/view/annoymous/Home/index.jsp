<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<sec:authentication property="principal" var="principal" />
<html>
<head>
</head>
<body type="menu-page">
	<p>${principal}</p>
	<div>
		<sec:authorize access="isAnonymous()">
        isAnonymous
        </sec:authorize>
		<sec:authorize access="isAuthenticated()">
		isAuthenticated
	   </sec:authorize>
		<sec:authorize access="hasRole('ROLE_USER')">
		hasRole('ROLE_USER')
	   </sec:authorize>
		<sec:authorize access="hasRole('ROLE_USER') and isAuthenticated()">
		</sec:authorize>
	</div>
	<div class="login_panel center_div">
		<div class="header">
			<div class="content">
				<img src="<c:url value='/resources/custom/images/logo.png' />">
			</div>
			<center>
				<h1>공사중</h1>
				<h3>빠른 시일내 찾아뵙겠습니다!</h3>
			</center>
		</div>
	</div>
</body>
</html>