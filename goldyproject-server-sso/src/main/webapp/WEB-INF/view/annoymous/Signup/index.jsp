<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="<c:url value='/resources/custom/view/annoymous/Signup/Signup.js'/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/custom/reset.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/custom/logo.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/custom/view/annoymous/Signup/Signup.css'/>" />
</head>
<body>
	<spring-form:form action="/signup" modelAttribute="signupForm" method="POST" enctype="utf8" role="form">
		<section id="login">
			<h1 id="logo">
				<a href="../index.html">LOGO</a>
			</h1>
			<div id="email-container" class="field-container">
				<label for="email" class="field-title">이메일</label>
				<spring-form:input type="text" id="email" path="email" cssClass="form-control full-width" required="required"
					autofocus="autofocus" placeholder="이메일아이디"
				/>
				<spring-form:errors id="error-email" path="email" cssClass="error-field warnning" />
			</div>
			<div id="password-container" class="field-container">
				<label for="password" class="field-title">비밀번호</label>
				<spring-form:password id="password" path="password" cssClass="form-control full-width" required="required"
					placeholder="비밀번호"
				/>
				<spring-form:errors id="error-password" path="password" cssClass="error-field warnning" />
			</div>
			<div id="password-repeat-container" class="field-container">
				<label for="password-repeat" class="field-title">비밀번호 재확인</label>
				<spring-form:password id="password-repeat" path="passwordRepeat" cssClass="form-control full-width"
					required="required" placeholder="비밀번호 재입력"
				/>
				<spring-form:errors id="error-password-repeat" path="passwordRepeat" cssClass="error-field warnning" />
			</div>
			<div id="name-container" class="field-container">
				<label for="user-name" class="field-title">이름</label>
				<spring-form:input type="text" id="user-name" path="userName" cssClass="form-control full-width" />
				<spring-form:errors id="error-user-name" path="userName" cssClass="info" />
			</div>
			<div id="birthday-container" class="field-container">
				<spring-form:hidden path="birthday" />
				<label for="year" class="field-title">생년월일</label>
				<div id="birthday-area">
					<div class="day-box">
						<select id="year" class="form-control">
						</select>
					</div>
					<div class="day-box-empty"></div>
					<div class="day-box">
						<select id="month" class="form-control">
						</select>
					</div>
					<div class="day-box-empty"></div>
					<div class="day-box">
						<select id="day" class="form-control">
						</select>
					</div>
				</div>
			</div>
			<div id="gender-container" class="field-container">
				<label for="gender" class="field-title">성별</label>
				<spring-form:select path="gender" id="gender" cssClass="form-control  full-width">
					<spring-form:option value="MAIL">남자</spring-form:option>
					<spring-form:option value="FEMAIL">여자</spring-form:option>
				</spring-form:select>
			</div>
			<button id="submit" type="submit">Register</button>
		</section>
	</spring-form:form>
</body>
</html>