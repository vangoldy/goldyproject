<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<header></header>
<body>
	<section>이메일 인증에 성공하였습니다.</section>
	<div>
		<span id="count">5</span>
		초 뒤 메인페이지로 이동합니다.
	</div>
	<c:import url="/resources/custom/to_main.jsp"></c:import>
</body>
</html>
