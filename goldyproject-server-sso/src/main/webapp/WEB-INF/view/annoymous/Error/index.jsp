<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
</head>
<body type="menu-page">
	<div class="center_div">
		<div class="header">
			<div class="content">
				<c:import url="/WEB-INF/component/logo.jsp"></c:import>
			</div>
		</div>
		<div>
			<div class="head_message">
				지원하지 않는 URL입니다.
				<a class="padding-left" href="javascript:history.back()">뒤로가기</a>
			</div>
			<div>timestamp:${timestamp}</div>
			<div>status:${status}</div>
			<div>error:${error}</div>
			<div>message:${message}</div>
			<div>path:${path}</div>
			<div>tarce:${trace}</div>
		</div>
	</div>
</body>
</html>