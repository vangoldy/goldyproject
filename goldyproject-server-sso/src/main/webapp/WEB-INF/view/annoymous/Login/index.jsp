<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring-form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<script type="text/javascript" src="<c:url value='/resources/custom/view/annoymous/Login/login.js'/>"></script>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/custom/reset.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/custom/logo.css'/>" />
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/custom/view/annoymous/Login/Login.css'/>" />
</head>
<body type="menu-page">
	<div class="login_panel center_div">
		<div class="header">
			<div class="content">
				<img src="${env["gp.host.shorturl.redirect.prefix"]}J61">
			</div>
		</div>
		<section id="login">
			<h1 id="logo">
				<a href="index.html">LOGO</a>
			</h1>
			<form action="/session/signin/processing" method="POST" role="form" id="loginForm">
				<spring-form:input type="hidden" path="${_csrf.parameterName}" name="${_csrf.parameterName}" value="${_csrf.token}" />
				<spring-form:input placeholder="* 아이디" id="user-email" path="username" name="username" type="text" class="form-control"
					required="true" autofocus="true"
				/>
				<label for="user-email"></label>
				<spring-form:input placeholder="* 패스워드" id="user-password" path="password" name="password" type="password" class="form-control"
					required="true"
				/>
				<label for="user-password"></label>
				<h2 id="jing">
					<a href="#" id="error_message"></a>
				</h2>
				<h3>
					<a href="#">앗! 로그인이 안 되나요?</a>
				</h3>
				<div class="login">
					<!-- <a href="#">로그인</a> -->
					<input type="submit" id="submit" value="로그인" />
				</div>
				<div id="checkbox_img">
					<input type="checkbox" id="uji" class="uji" name="uji">
					<i></i> <label for="uji">로그인 상태 유지</label>
				</div>
				<div class="find">
					<ul>
						<li><a href="#">아이디 찾기</a></li>
						<li><a href="#">비밀번호 찾기</a></li>
						<li><a href="<c:url value='/signup'/>">회원가입</a></li>
					</ul>
				</div>
			</form>
		</section>
		<div class="base">
			<div class="login-box">
				<div class="content">
					<div class="login-form">
						<form action="<c:url value='/session/signin/processing'/>" method="POST" role="form" id="loginForm">
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<c:if test="${loginFailMessage!=null}">
								<div class="alert alert-danger alert-dismissible">
									<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
									<p>${loginFailMessage}</p>
								</div>
							</c:if>
							<div class="row_area">
								<input placeholder="* 아이디" id="user-email" name="username" type="text" class="form-control" required autofocus />
							</div>
							<div class="row_area">
								<span class="label default_font"> </span>
								<input placeholder="* 패스워드" id="user-password" name="password" type="password" class="form-control" required />
							</div>
							<div class="row_area">
								<button type="submit" id="submit">로그인</button>
							</div>
						</form>
					</div>
					<div class="row_area">
						<div class="create_account default_font">
							<span> 계정이 없으신가요?</span>
							<span class="account_label">
								<a href="<c:url value='/signup'/>">회원가입</a>
							</span>
						</div>
					</div>
					<center>
						<div id="social_login_box">
							<a class="btn azm-social azm-btn azm-border-bottom azm-google-plus social-button"
								href="<c:url value='/oauth2/authorization/google'/>"
							>
								<i class="fa fa-google"></i> 구글 로그인
							</a>
							<%--
                            https만 허용하여 차단함
                            <a class="btn azm-social azm-btn azm-border-bottom azm-facebook social-button"
								href="<c:url value='/oauth2/authorization/facebook'/>"
							>
								<i class="fa fa-facebook"></i> 페이스북 로그인
							</a> --%>
						</div>
					</center>
				</div>
			</div>
		</div>
	</div>
</body>
</html>