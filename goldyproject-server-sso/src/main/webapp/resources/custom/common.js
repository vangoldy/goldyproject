var baseUrl;
var $fw = {
	loading : {},
	ajax : {},
	message : {},
};

// -- Start Setting -- //

baseUrl = settingBaseUrl();
setupLoading();
setupAjax();
setupAuthenticationFunction();

$(function() {
	setupMessage();
	setupScrollbar();
	setupSelect2();
});

function settingBaseUrl() {
	var context = $('base').attr('href');
	return window.location.protocol + '//' + window.location.host
			+ context.substring(0, context.length - 1);
}

function setupLoading() {
	$fw.loading.show = function() {
		$.LoadingOverlay("show");
	};

	$fw.loading.hide = function() {
		$.LoadingOverlay("hide");
	};
}

function setupScrollbar() {
	var $target = $('.scrollbar-macosx');
	$target.scrollbar();
	$(window).resize(function() {
		$target.scrollbar();
	});
}

function setupAjax() {
	$(document).ajaxStart(function(arg) {
		$fw.loading.show();
	});

	$(document).ajaxStop(function(arg) {
		$fw.loading.hide();
	});

	$fw.ajax.sendGet = function(address, data, args) {
		sendAjax(address, data, "GET", args);
	};

	$fw.ajax.sendPost = function(address, data, args) {
		sendAjax(address, data, "POST", args);
	};

	$fw.ajax.sendPut = function(address, data, args) {
		sendAjax(address, data, "PUT", args);
	};

	$fw.ajax.sendDelete = function(address, data, args) {
		sendAjax(address, data, "DELETE", args);
	};

	$fw.ajax.asyncGet = function(address, args) {

		var defaults = {
			showErrorMessage : true,
			successEvent : function() {
			},
			errorEvent : function() {
			}
		}
		$.extend(true, defaults, args);

		var result = "";
		$.ajax({
			url : baseUrl + address,
			async : false,
			success : function(data, a, b) {
				result = data;
			},
			error : function(xhr, status) {
				errorAction(xhr, status, defaults.showErrorMessage);
				defaults.errorEvent(xhr, status);
				throw xhr;
			}
		})
		return result;
	};

	$fw.ajax.asyncPost = function(address, json, args) {

		var defaults = {
			dataType : "json",
			showErrorMessage : true,
			successEvent : function() {
			},
			errorEvent : function() {
			}
		}
		$.extend(true, defaults, args);

		var result = "";
		$.ajax({
			url : baseUrl + address,
			type : 'POST',
			data : JSON.stringify(json),
			dataType : defaults.dataType,
			contentType : "application/json; charset=UTF-8",
			async : false,
			success : function(data) {
				result = data;
			},
			error : function(xhr, status) {
				errorAction(xhr, status, defaults.showErrorMessage);
				defaults.errorEvent(xhr, status);
				throw xhr;
			}
		})
		return result;
	};

	$fw.ajax.sendTextType = function(address, value, successEvent) {
		var url = downloadAdminBaseUrl + address;
		var data = value.toString();
		$.ajax({
			url : url,
			type : 'PUT',
			data : data,
			dataType : "text",
			contentType : "text/plain; charset=UTF-8",
			success : function(data) {
				successEvent();
			},
			error : function(xhr, status) {
				errorAction(xhr, status, true);
			}
		})
	};

	$fw.ajax.sendJsonType = function(address, value, successEvent) {

		var url = downloadAdminBaseUrl + address;
		var data = JSON.stringify(value);
		$.ajax({
			url : url,
			type : 'PUT',
			data : data,
			dataType : "text",
			contentType : "application/json; charset=UTF-8",
			success : function(data) {
				successEvent();
			},
			error : function(xhr, status) {
				errorAction(xhr, status, true);
			}
		});
	};

	function errorAction(xhr, status, showErrorMessage) {
		$fw.loading.hide();
		if (xhr.status == 401) {
			location.href = baseUrl + '/login';
		} else if (showErrorMessage === true) {
			$fw.message.error(xhr.responseText);
		}
	}

	function sendAjax(address, data, type, args) {
		var defaults = {
			dataType : "text",
			contentType : "text/plain; charset=UTF-8",
			showErrorMessage : true,
			successEvent : function() {
			},
			errorEvent : function() {
			}
		}
		$.extend(true, defaults, args);

		if (typeof data === 'string' || typeof data === 'boolean') {
			data = data.toString();
			defaults.contentType = "text/plain; charset=UTF-8";
		} else if (typeof data === 'object') {
			data = JSON.stringify(data);
			defaults.contentType = "application/json; charset=UTF-8";
		}

		$.ajax({
			url : baseUrl + address,
			data : data,
			type : type,
			dataType : defaults.dataType,
			contentType : defaults.contentType,
			success : function(data) {
				defaults.successEvent(data);
			},
			error : function(xhr, status) {
				errorAction(xhr, status, defaults.showErrorMessage);
				defaults.errorEvent(xhr, status);
			}
		});
	}
}

function setupMessage() {
	$fw.message.error = function(message) {
		notif({
			type : "error",
			msg : message,
			width : "all",
			position : "center"
		});
	}

	$fw.message.success = function(message) {
		notif({
			type : "success",
			msg : message,
			width : "all",
			position : "center"
		});
	}

	$fw.message.showToast = function(message) {
		console.log(message);
	}
}

function setupAuthenticationFunction() {
	function getSession() {
		return $fw.ajax.asyncGet("/session/myAuthentication");
	}

	$fw.isUser = function() {
		var session = getSession();
		if (session === "" || session === null || session.length === 0) {
			return false;
		}

		for (var i = 0; i < session.authorities.length; i++) {
			if (session.authorities[i].authority === 'ROLE_USER') {
				return true;
			}
		}
		return false;
	}
}

function setupSelect2() {
	$('select').select2({
		placeholder : function() {
			$(this).data('placeholder');
		}
	});
}
