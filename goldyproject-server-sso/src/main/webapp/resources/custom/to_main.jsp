<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
	var $countDiv = $('span#count');
	setInterval(updateIndex, 1000);

	function updateIndex() {
		var val = $countDiv.text();
		if (val == 0) {
			location.href = "<c:url value="/"/>";
		}
		val--;
		$countDiv.text(val);
	}
</script>