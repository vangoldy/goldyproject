Date.prototype.yMd = function(split) {
	const mm = this.getMonth() + 1;
	const dd = this.getDate();

	const arr = [this.getFullYear(), (mm > 9 ? '' : '0') + mm,
			(dd > 9 ? '' : '0') + dd];

	if (!split) {
		split = "-";
	}
	return arr.join(split);
}

$(function() {
	initializeBirthday();
})

function initializeBirthday() {
	const $birthdayField = {
		birthday : $('#birthday'),
		year : $('#year'),
		month : $('#month'),
		day : $('#day')
	}

	var definedBirthday = $birthdayField.birthday.val();
	if (!definedBirthday) {
		definedBirthday = "1990-10-07";
	}

	initialYearField($birthdayField.year);
	initialMonthField($birthdayField.month);
	defineEventBirthdayFields($birthdayField, definedBirthday);
	setBirthday($birthdayField, definedBirthday);
}

function setBirthday($birthdayField, stringDate) {
	var date = new Date(stringDate);
	$birthdayField.year.val(date.getFullYear()).change();
	$birthdayField.month.val(date.getMonth() + 1).change();
	$birthdayField.day.val(date.getDate()).change();
}

function createOption(value, text) {
	const $option = $('<option>');
	$option.val(value);
	$option.text(text);
	return $option;
}
function initialYearField($year) {

	const currentYear = new Date().getFullYear();

	for (let i = currentYear; i > 1900; i--) {
		const $yearOption = createOption(i, i + '년');
		$year.append($yearOption);
	}
}

function initialMonthField($month) {
	for (var i = 1; i <= 12; i++) {
		var $monthOption = createOption(i, i + '월');
		$month.append($monthOption);
	}
}

function defineEventBirthdayFields($birthdayField, definedBirthday) {

	let birthdayDate = new Date(definedBirthday);

	$birthdayField.year.change(function() {

		var yearValue = $birthdayField.year.val();

		birthdayDate.setFullYear(yearValue);
		resetDays();
		cal();
	})

	$birthdayField.month.change(function() {
		birthdayDate.setMonth($birthdayField.month.val() - 1);
		const selectedDay = $birthdayField.day.val();
		resetDays();

		const lastDay = $birthdayField.day.find('option:last-child').val();
		if (selectedDay > lastDay) {
			$birthdayField.day.val(1);
		} else {
			$birthdayField.day.val(selectedDay);
		}
		cal();
	})

	$birthdayField.day.change(function() {
		birthdayDate.setDate($birthdayField.day.val());
		cal();
	})

	function resetDays() {

		$birthdayField.day.empty();

		const year = birthdayDate.getFullYear();
		const month = birthdayDate.getMonth();
		const lastDayOfMonth = new Date(year, month + 1, 0).getDate();

		for (var i = 1; i <= lastDayOfMonth; i++) {
			const $dayOption = createOption(i, i + "일");
			$birthdayField.day.append($dayOption);
		}
	}

	function cal() {

		const yyyyMMdd = birthdayDate.yMd();

		$birthdayField.birthday.val(yyyyMMdd);

	}

}