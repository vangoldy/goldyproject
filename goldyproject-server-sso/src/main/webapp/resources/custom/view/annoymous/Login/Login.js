$(function() {

	var $idBox = $("#user-email");
	var $passwordBox = $("#user-password");
	var $errorMessageDiv = $("#error_message");

	$(".login").click(function() {

		$errorMessageDiv.empty();

		var idText = $idBox.val(); // val() 내용물을 가져온다
		var passwordText = $passwordBox.val();

		if (validIdText(idText) == false) {
			$idBox.focus();
			var a = showMessage("아이디가 잘못입력됬습니다");
			if (validPasswordText(passwordText) == false) {
				$idBox.focus();
				var a = showMessage("비밀번호가 잘못입력됬습니다")
			}
		}

		if (validPasswordText(passwordText) == false) {
			$passwordBox.focus();
		}
	})

	function showMessage(message) {
		$errorMessageDiv.text(message);
	}

	function validIdText(str) {

		if (!str || str == null || str.length == 0) {
			return false;
		}

		if (str.indexOf(' ') != -1) {
			return false;
		}
		return true;
	}

	function validPasswordText(str) {
		if (str == null || str.length == 0) {
			return false;
		}

		if (str.indexOf(' ') != -1) {
			return false;
		}

		return true;

	}

})
