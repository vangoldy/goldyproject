/**
 * FileName : {@link SessionRedirectSupporter}.java
 * Created : 2019. 1. 27. 오후 2:30:42
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.web.savedrequest.SavedRequest;

import com.goldy.gtils.utils.HttpServletGtils;
import com.goldy.gtils.utils.NullGtils;

public class SessionRedirectSupporter {

	private static final List<String> IGNORE_URLS = Arrays.asList("/session", "/resources");

	/**
	 * {@link SessionRedirectSupporter} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 17. 오후 5:05:50 jeonghyun.kum
	 */
	private SessionRedirectSupporter() {

		// do nothing

	}

	@SuppressWarnings("findsecbugs:UNVALIDATED_REDIRECT")
	public static void autoRedirect(HttpServletRequest servletRequest,
		HttpServletResponse servletResponse) throws IOException {

		String redirect = NullGtils.<String> getOr(
			() -> HttpServletGtils.getReferer(servletRequest),
			() -> {
				final HttpSession session = servletRequest.getSession();
				final String attribute = (String) session.getAttribute("referer");
				session.removeAttribute("referer");
				return attribute;
			},
			() -> "/");

		if (containsIgnoreUrl(redirect)) {
			redirect = "/";
		}
		servletResponse.sendRedirect(redirect);
	}

	public static boolean containsIgnoreUrl(String url) {

		if (url == null) {
			return false;
		}

		for (final String ignoreUrl : IGNORE_URLS) {
			if (url.startsWith(ignoreUrl)) {
				return true;
			}
		}
		return false;
	}

	public static void restoreRedirect(HttpServletRequest servletRequest) {

		final HttpSession session = servletRequest.getSession(false);

		final SavedRequest savedRequest = (SavedRequest) session.getAttribute("SPRING_SECURITY_SAVED_REQUEST");
		if (savedRequest == null) {
			return;
		}

		final String baseUrl = HttpServletGtils.extractBaseUrl(servletRequest);

		if (containsIgnoreUrl(savedRequest.getRedirectUrl().replace(baseUrl, ""))) {
			session.removeAttribute("SPRING_SECURITY_SAVED_REQUEST");
		}

	}
}
