/**
 * FileName : {@link SsoApplication}.java
 * Created : 2018. 8. 31. 오후 10:41:29
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import com.goldy.project.module.webcommon.config.advice.AdviceConfig;
import com.goldy.project.module.webcommon.config.async.WebCommonAsyncConfig;
import com.goldy.project.module.webcommon.config.commonbean.WebCommonBeanConfig;
import com.goldy.project.module.webcommon.config.profile.ProfilePropertiesConfig;
import com.goldy.project.module.webcommon.config.webmvc.WebCommonWebMvcConfig;

@SpringBootConfiguration
@EnableAutoConfiguration(exclude = {
		JmxAutoConfiguration.class
})

@Import({
		MethodValidationPostProcessor.class,
		ProfilePropertiesConfig.class,
		AdviceConfig.class,
		WebCommonAsyncConfig.class,
		WebCommonBeanConfig.class,
		WebCommonWebMvcConfig.class,
		Config.class,
})
public class SsoApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {

		SpringApplication.run(SsoApplication.class, args);
	}

}
