/**
 * FileName : {@link AuthGeneralDao}.java
 * Created : 2018. 9. 9. 오후 2:06:49
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.jpa.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.goldy.project.server.sso.model.entity.AuthGeneralVo;

/**
 * "auth_general" 테이블 JPA Repository
 */
public interface AuthGeneralDao extends JpaRepository<AuthGeneralVo, Long> {

	/**
	 * "login_id" 필드에 해당하는 VO를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginId
	 *            "login_id" 필드 값
	 * @return 해당하는 {@link AuthGeneralVo}
	 */
	Optional<AuthGeneralVo> findByLoginId(String loginId);

}
