/**
 * FileName : {@link SecurityUserDetail}.java
 * Created : 2018. 9. 9. 오후 1:44:30
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.Assert;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.goldy.gtils.utils.JsonGtils;
import com.goldy.project.server.sso.model.entity.AccountAccessHistoryVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.model.type.LoginProvider;

public class SecurityUserDetail extends User {

	/**
	 * Builds the user to be added. At minimum the username, password, and authorities
	 * should provided. The remaining attributes have reasonable defaults.
	 */
	public static class Builder {

		private String username;

		private PersonalInformationVo personalInformationVo;

		private String password;

		private final Set<GrantedAuthority> authorities = new HashSet<>();

		private boolean accountNonExpired;

		private boolean accountNonLocked;

		private boolean credentialsNonExpired;

		private boolean enable;

		private AccountVo accountVo;

		private AuthGeneralVo authGeneralVo;

		private LoginProvider loginProvider;

		private AccountAccessHistoryVo accessHistory;

		/**
		 * Creates a new instance
		 */
		protected Builder() {}

		public Builder accessHistory(AccountAccessHistoryVo accessHistory) {

			this.accessHistory = accessHistory;
			return this;
		}

		/**
		 * Defines if the account is expired or not. Default is false.
		 *
		 * @param accountNonExpired
		 *            true if the account is expired, false otherwise
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder accountNonExpired(boolean accountNonExpired) {

			this.accountNonExpired = accountNonExpired;
			return this;
		}

		/**
		 * Defines if the account is locked or not. Default is false.
		 *
		 * @param accountNonLocked
		 *            true if the account is locked, false otherwise
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder accountNonLocked(boolean accountNonLocked) {

			this.accountNonLocked = accountNonLocked;
			return this;
		}

		public Builder authGeneralVo(AuthGeneralVo authGeneralVo) {

			this.authGeneralVo = authGeneralVo;
			return this;
		}

		/**
		 * Populates the authorities. This attribute is required.
		 *
		 * @param authorities
		 *            the authorities for this user. Cannot be null, or contain
		 *            null values
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder authorities(Collection<? extends GrantedAuthority> authorities) {

			this.authorities.addAll(authorities);
			return this;
		}

		/**
		 * Populates the authorities. This attribute is required.
		 *
		 * @param authorities
		 *            the authorities for this user. Cannot be null, or contain
		 *            null values
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder authorities(GrantedAuthority... authorities) {

			return this.authorities(Arrays.asList(authorities));
		}

		/**
		 * Populates the authorities. This attribute is required.
		 *
		 * @param authorities
		 *            the authorities for this user (i.e. ROLE_USER, ROLE_ADMIN,
		 *            etc). Cannot be null, or contain null values
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder authorities(String... authorities) {

			return this.authorities(AuthorityUtils.createAuthorityList(authorities));
		}

		public SecurityUserDetail build() {

			final SecurityUserDetail securityUserDetail = new SecurityUserDetail(this.username, this.password,
				this.enable,
				this.accountNonExpired,
				this.credentialsNonExpired, this.accountNonLocked, this.authorities);

			securityUserDetail.accountVo = this.accountVo;
			securityUserDetail.authGeneralVo = this.authGeneralVo;
			securityUserDetail.loginProvider = this.loginProvider;
			securityUserDetail.personalInformationVo = this.personalInformationVo;
			securityUserDetail.accessHistory = this.accessHistory;
			securityUserDetail.appendVo(securityUserDetail);

			return securityUserDetail;
		}

		/**
		 * Defines if the credentials are expired or not. Default is false.
		 *
		 * @param credentialsNonExpired
		 *            true if the credentials are expired, false otherwise
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder credentialsNonExpired(boolean credentialsNonExpired) {

			this.credentialsNonExpired = credentialsNonExpired;
			return this;
		}

		/**
		 * Defines if the account is disabled or not. Default is false.
		 *
		 * @param enable
		 *            true if the account is disabled, false otherwise
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder enable(boolean enable) {

			this.enable = enable;
			return this;
		}

		public Builder loginProvider(LoginProvider loginProvider) {

			this.loginProvider = loginProvider;
			return this;
		}

		/**
		 * Populates the password. This attribute is required.
		 *
		 * @param password
		 *            the password. Cannot be null.
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder password(String password) {

			Assert.notNull(password, "password cannot be null");
			this.password = password;
			return this;
		}

		public Builder personalInformationVo(PersonalInformationVo personalInformationVo) {

			this.personalInformationVo = personalInformationVo;
			return this;
		}

		/**
		 * Populates the roles. This method is a shortcut for calling
		 * {@link #authorities(String...)}, but automatically prefixes each entry with
		 * "ROLE_". This means the following:
		 * <code>
		 *     builder.roles("USER","ADMIN");
		 * </code>
		 * is equivalent to
		 * <code>
		 *     builder.authorities("ROLE_USER","ROLE_ADMIN");
		 * </code>
		 * <p>
		 * This attribute is required, but can also be populated with
		 * {@link #authorities(String...)}.
		 * </p>
		 *
		 * @param roles
		 *            the roles for this user (i.e. USER, ADMIN, etc). Cannot be null,
		 *            contain null values or start with "ROLE_"
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder roles(String... roles) {

			final List<GrantedAuthority> authorities = new ArrayList<>(
				roles.length);
			for (final String role : roles) {
				Assert.isTrue(!role.startsWith("ROLE_"), role
					+ " cannot start with ROLE_ (it is automatically added)");
				authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
			}
			return this.authorities(authorities);
		}

		public Builder user(AccountVo user) {

			this.accountVo = user;
			return this;
		}

		/**
		 * Populates the username. This attribute is required.
		 *
		 * @param username
		 *            the username. Cannot be null.
		 * @return the {@link Builder} for method chaining (i.e. to populate
		 *         additional attributes for this user)
		 */
		public Builder username(String username) {

			Assert.notNull(username, "username cannot be null");
			this.username = username;
			return this;
		}
	}

	/** Serial Version UID */
	private static final long serialVersionUID = 1561976986026885255L;

	private AccountAccessHistoryVo accessHistory;

	private final Map<String, Object> user = new HashMap<>();

	private AccountVo accountVo;

	private AuthGeneralVo authGeneralVo;

	private LoginProvider loginProvider;

	private PersonalInformationVo personalInformationVo;

	/**
	 * {@link SecurityUserDetail} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 1. 19. 오후 9:15:59 jeonghyun.kum
	 * @param username
	 *            the username presented to the
	 *            <code>DaoAuthenticationProvider</code>
	 * @param password
	 *            the password that should be presented to the
	 *            <code>DaoAuthenticationProvider</code>
	 * @param enabled
	 *            set to <code>true</code> if the user is enabled
	 * @param accountNonExpired
	 *            set to <code>true</code> if the account has not expired
	 * @param credentialsNonExpired
	 *            set to <code>true</code> if the credentials have not
	 *            expired
	 * @param accountNonLocked
	 *            set to <code>true</code> if the account is not locked
	 * @param authorities
	 *            the authorities that should be granted to the caller if they
	 *            presented the correct username and password and the user is enabled. Not null.
	 * @throws IllegalArgumentException
	 *             if a <code>null</code> value was passed either as
	 *             a parameter or as an element in the <code>GrantedAuthority</code> collection
	 */
	protected SecurityUserDetail(String username, String password, boolean enabled, boolean accountNonExpired,
		boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {

		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
	}

	public static Builder getBuilder() {

		return new Builder();
	}

	public void appendVo(Object value) {

		final Map<String, Object> map = JsonGtils.toMap(value);

		this.user.putAll(map);
	}

	/**
	 * accessHistory를 반환합니다.
	 *
	 * @return accessHistory
	 * @author jeonghyun.kum
	 */
	public AccountAccessHistoryVo getAccessHistory() {

		return this.accessHistory;
	}

	/**
	 * user를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return user
	 */
	public AccountVo getAccount() {

		return this.accountVo;
	}

	/**
	 * generalUserAuth를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return authGeneralVo
	 */
	public AuthGeneralVo getAuthGeneral() {

		return this.authGeneralVo;
	}

	/**
	 * loginProvider를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginProvider
	 */
	public LoginProvider getLoginProvider() {

		return this.loginProvider;
	}

	/**
	 * userDetail를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return personalInformationVo
	 */
	public PersonalInformationVo getPi() {

		return this.personalInformationVo;
	}

	/**
	 * user를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return user
	 */
	public Map<String, Object> getUser() {

		return this.user;
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public String toString() {

		final ObjectMapper build = Jackson2ObjectMapperBuilder.json().build();
		try {
			return build.writeValueAsString(this);
		} catch (final JsonProcessingException e) {
			return "";
		}
	}
}
