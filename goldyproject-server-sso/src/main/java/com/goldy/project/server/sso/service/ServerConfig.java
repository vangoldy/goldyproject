/**
 * FileName : {@link ServerConfig}.java
 * Created : 2019. 1. 29. 오후 11:35:47
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.goldy.project.server.sso.service.jpa.DataSourceConfig;
import com.goldy.project.server.sso.service.mvc.WebMvcConfig;
import com.goldy.project.server.sso.service.security.SecurityConfig;

@Configuration
@Import({
		DataSourceConfig.class,
		SecurityConfig.class,
		WebMvcConfig.class,
})
public class ServerConfig {
	// do nothing
}
