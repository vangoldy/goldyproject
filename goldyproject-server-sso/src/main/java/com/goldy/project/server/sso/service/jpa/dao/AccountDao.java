/**
 * FileName : {@link AccountDao}.java
 * Created : 2018. 9. 9. 오후 2:06:11
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.jpa.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.goldy.project.server.sso.model.entity.AccountVo;

/**
 * "account" 테이블 JPA Repository
 */
public interface AccountDao extends JpaRepository<AccountVo, Long> {

	/**
	 * "login_email"에 해당하는 데이터가 존재한지 여부를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginEmail
	 *            "login_email" 필드 값
	 * @return 존재 여부
	 */
	boolean existsByLoginEmail(String loginEmail);

	/**
	 * "login_email"에 해당하는 VO를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginEmail
	 *            "login_email" 필드 값
	 * @return 해당하는 {@link AccountVo}
	 */
	Optional<AccountVo> findByLoginEmail(String loginEmail);

}
