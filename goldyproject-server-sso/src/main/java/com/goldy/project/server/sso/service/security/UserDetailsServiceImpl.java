/**
 * FileName : {@link UserDetailsServiceImpl}.java
 * Created : 2018. 9. 9. 오후 2:25:03
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import java.util.Collection;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.goldy.gtils.exception.LogicErrorType;
import com.goldy.gtils.exception.LogicException;
import com.goldy.project.server.sso.model.entity.AccountAccessHistoryVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.model.type.LoginProvider;
import com.goldy.project.server.sso.service.account.AccountService;

public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private AccountService accountService;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private HttpServletRequest servletRequest;

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public SecurityUserDetail loadUserByUsername(String username) throws UsernameNotFoundException {

		final AccountVo account = this.accountService.getByEmail(username)
			.orElseThrow(() -> new UsernameNotFoundException(username + "에 대한 사용자를 찾을 수 없습니다."));

		return this.toUserDetail(account);

	}

	private SecurityUserDetail toUserDetail(AccountVo account) {

		final long accountId = account.getId();
		final AuthGeneralVo authGeneralVo = this.accountService.getAuthGeneral(accountId)
			.orElseThrow(() -> new LogicException(LogicErrorType.INVALID_DESIGN));

		final PersonalInformationVo personalInformationVo = this.accountService.getPersonalInformation(accountId)
			.orElseThrow(() -> new LogicException(LogicErrorType.INVALID_DESIGN));

		final Collection<SimpleGrantedAuthority> collect = this.securityService.getAllRoles(accountId)
			.stream()
			.map(SimpleGrantedAuthority::new)
			.collect(Collectors.toList());

		final AccountAccessHistoryVo savedHistory = this.accountService.createAccessHistory(accountId,
			this.servletRequest);

		return SecurityUserDetail.getBuilder()
			.username(account.getLoginEmail())
			.password(authGeneralVo.getLoginPassword())
			.enable(account.isLoginEnable())
			.accountNonExpired(account.isLoginNonExpired())
			.credentialsNonExpired(account.isCredentialsNonExpired())
			.accountNonLocked(account.isLoginNonLocked())
			.authorities(collect)
			.accessHistory(savedHistory)
			.user(account)
			.personalInformationVo(personalInformationVo)
			.authGeneralVo(authGeneralVo)
			.loginProvider(LoginProvider.GENERAL)
			.build();
	}

}
