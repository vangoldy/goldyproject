/**
 * FileName : {@link SignupForm}.java
 * Created : 2019. 1. 20. 오후 6:56:13
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.form;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import com.goldy.gtils.type.Gender;
import com.goldy.gtils.user.form.EmailSignupForm;
import com.goldy.gtils.user.validator.Phone;
import com.goldy.gtils.validation.constraints.RequireField;

public class SignupForm extends EmailSignupForm {

	private String nickName;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate birthday;

	@Phone
	private String phoneNumber;

	private String userName;

	@RequireField
	private Gender gender;

	/**
	 * birthday를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return birthday
	 */
	public LocalDate getBirthday() {

		return this.birthday;
	}

	/**
	 * gender를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return gender
	 */
	public Gender getGender() {

		return this.gender;
	}

	/**
	 * nickName를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return nickName
	 */
	public String getNickName() {

		return this.nickName;
	}

	/**
	 * phoneNumber를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return phoneNumber
	 */
	public String getPhoneNumber() {

		return this.phoneNumber;
	}

	/**
	 * userName를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return userName
	 */
	public String getUserName() {

		return this.userName;
	}

	/**
	 * birthday 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param birthday
	 *            초기화 값
	 */
	public void setBirthday(LocalDate birthday) {

		this.birthday = birthday;
	}

	/**
	 * gender 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param gender
	 *            초기화 값
	 */
	public void setGender(Gender gender) {

		this.gender = gender;
	}

	/**
	 * nickName 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param nickName
	 *            초기화 값
	 */
	public void setNickName(String nickName) {

		this.nickName = nickName;
	}

	/**
	 * phoneNumber 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param phoneNumber
	 *            초기화 값
	 */
	public void setPhoneNumber(String phoneNumber) {

		this.phoneNumber = phoneNumber;
	}

	/**
	 * userName 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param userName
	 *            초기화 값
	 */
	public void setUserName(String userName) {

		this.userName = userName;
	}
}
