/**
 * FileName : {@link AuthenticationSuccessHandlerImpl}.java
 * Created : 2019. 1. 27. 오전 1:43:07
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.goldy.gtils.exception.LogicErrorType;
import com.goldy.gtils.exception.LogicException;
import com.goldy.gtils.exception.NoPrincipalException;
import com.goldy.project.server.sso.service.account.AccountService;

@Component
public class AuthenticationSuccessHandlerImpl extends SavedRequestAwareAuthenticationSuccessHandler {

	@Autowired
	private SecurityService securityServiceImpl;

	@Autowired
	private AccountService accountService;

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest servletRequest, HttpServletResponse servletResponse,
		Authentication authentication) throws IOException, ServletException {

		SessionRedirectSupporter.restoreRedirect(servletRequest);

		SecurityUserDetail userDetail;
		try {
			userDetail = this.securityServiceImpl.getUserDetail(authentication);
		} catch (final NoPrincipalException e) {
			throw new LogicException(LogicErrorType.INVALID_DESIGN, e);
		}

		this.accountService.successLogin(userDetail.getAccount().getId());

		super.onAuthenticationSuccess(servletRequest, servletResponse, authentication);
	}

}
