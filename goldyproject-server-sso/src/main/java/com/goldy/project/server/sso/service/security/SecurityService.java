/**
 * FileName : {@link SecurityService}.java
 * Created : 2019. 2. 10. 오후 12:51:50
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Min;

import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;

import com.goldy.gtils.exception.NoPrincipalException;
import com.goldy.gtils.validation.constraints.RequireField;
import com.goldy.project.server.sso.service.security.role.SpringSecurityRole;

/**
 * 계정 보안 서비스
 */
@Validated
public interface SecurityService {

	/**
	 * 계정 ID에 해당하는 모든 권한을 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 * @return 포함된 모든 권한
	 */
	Collection<String> getAllRoles(@Min(1) long accountId);

	Collection<SpringSecurityRole> getSpringSecurityRoles(@Min(1) long accountId);

	/**
	 * API를 호출한 requester을 분석하여 사용자의 정보를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @since 2016. 4. 25. 오전 1:02:52
	 * @return 사용자 정보 {@link SecurityUserDetail}
	 * @throws NoPrincipalException
	 *             사용자 세션이 없는 경우 발생합니다.
	 */

	SecurityUserDetail getUserDetail() throws NoPrincipalException;

	/**
	 * 현재 세션의 {@link SecurityUserDetail}를 반환한다.
	 *
	 * @author jeonghyun.kum
	 * @param authentication
	 *            현재 로그인한 사용자 세션
	 * @return {@link SecurityUserDetail}
	 * @throws NoPrincipalException
	 *             사용자 세션이 없는 경우 발생합니다.
	 */
	SecurityUserDetail getUserDetail(@RequireField Authentication authentication) throws NoPrincipalException;

	/**
	 * (위험) 사용자를 로그인합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 아이디
	 * @throws NoPrincipalException
	 *             사용자 세션이 없는 경우 발생합니다.
	 * @since 2016. 4. 25. 오후 3:11:30
	 */
	void login(@Min(1) long accountId) throws NoPrincipalException;

	/**
	 * 사용자를 로그아웃합니다.
	 *
	 * @author jeonghyun.kum
	 * @param authentication
	 *            Spring {@link Authentication} Instance
	 * @param servletRequest
	 *            서블릿 요청 {@link HttpServletRequest}
	 * @param servletResponse
	 *            서블릿 응답 {@link HttpServletResponse}
	 */
	void logout(@RequireField Authentication authentication, @RequireField HttpServletRequest servletRequest,
		@RequireField HttpServletResponse servletResponse);

}
