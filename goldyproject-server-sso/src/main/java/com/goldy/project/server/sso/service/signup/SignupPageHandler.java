/**
 * FileName : {@link SignupPageHandler}.java
 * Created : 2019. 1. 23. 오후 9:21:41
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.signup;

import org.springframework.stereotype.Service;

@Service
public class SignupPageHandler {

	/**
	 * @return 이미 인증된 사람인 경우 이동하는 페이지
	 */
	public String getAlreadyRegistredUserPage() {

		return "annoymous/Signup/already_registred_user";
	}

	/**
	 * @return 이메일 인증에 실패하였을 때 페이지
	 */
	public String getFailEmailAuthPage() {

		return "annoymous/Signup/fail_email_auth";
	}

	/**
	 * @return 이메일 인증이 성공적으로 완료되었을 때 페이지
	 */
	public String getFinishEmailAuthPage() {

		return "annoymous/Signup/finish_email_auth";
	}

	/**
	 * @return 이메일 전송했다고 알려주는 페이지
	 */
	public String getSendedEmailPage() {

		return "annoymous/Signup/email_sended";
	}

	/**
	 * @return 회원가입 페이지
	 */
	public String getSignupPage() {

		return "annoymous/Signup/index";
	}
}
