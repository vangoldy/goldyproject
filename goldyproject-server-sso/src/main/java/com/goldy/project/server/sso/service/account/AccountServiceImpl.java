/**
 * FileName : {@link AccountServiceImpl}.java
 * Created : 2019. 1. 27. 오후 10:20:20
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.account;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

import com.goldy.gtils.exception.LogicErrorType;
import com.goldy.gtils.exception.LogicException;
import com.goldy.gtils.utils.HttpServletGtils;
import com.goldy.gtils.utils.NullGtils;
import com.goldy.gtils.utils.RequestDetail;
import com.goldy.project.server.sso.model.entity.AccountAccessHistoryVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.service.jpa.dao.AccountAccessHistoryDao;
import com.goldy.project.server.sso.service.jpa.dao.AccountDao;
import com.goldy.project.server.sso.service.jpa.dao.AuthGeneralDao;
import com.goldy.project.server.sso.service.jpa.dao.PersonalInformationDao;
import com.google.common.annotations.VisibleForTesting;

public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountDao accountDao;

	@Autowired
	private AuthGeneralDao authGeneralDao;

	@Autowired
	private PersonalInformationDao personalInformationDao;

	@Autowired
	private AccountAccessHistoryDao accountAccessHistoryDao;

	/**
	 * {@inheritDoc}
	 * 이곳에서 {@link AccountAccessHistoryVo#isLoginSuccessFlag()} 는 false를 세팅하고 {@link #successLogin(long)}에서 true로 변경함
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public AccountAccessHistoryVo createAccessHistory(long accountId, HttpServletRequest servletRequest) {

		final RequestDetail extractRequest = HttpServletGtils.extractRequest(servletRequest);

		final AccountAccessHistoryVo vo = new AccountAccessHistoryVo();
		vo.setAccountId(NullGtils.throwIfNull(accountId, "계정 아이디가 null 일 수 없습니다."));
		vo.setAgent(NullGtils.throwIfNull(extractRequest.getUserAgent(), "agent가 null 일 수 없습니다."));
		vo.setIp(extractRequest.getLocalAddr());
		vo.setJsessionId(extractRequest.getJsessionId());
		vo.setName(extractRequest.getLocalName());
		vo.setSessionId(extractRequest.getSessionId());
		vo.setLoginSuccessFlag(false);
		return this.accountAccessHistoryDao.save(vo);
	}

	/**
	 * 로그인 성공 플래그 추가
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 */
	@VisibleForTesting
	void flaggedLoginSuccess(long accountId) {

		final AccountAccessHistoryVo accessHistory = this.accountAccessHistoryDao
			.findTop1ByAccountIdOrderByCreatedTimeDescIdDesc(accountId)
			.orElseThrow(LogicException::new);

		if (accessHistory.isLoginSuccessFlag() == true) {
			throw new LogicException(LogicErrorType.INVALID_DESIGN,
				"createAccessHistory에서 false를 set하고 이곳에서 true를 처리하는데 두번 true되면 로직 오류");
		}
		accessHistory.setLoginSuccessFlag(false);
		this.accountAccessHistoryDao.save(accessHistory);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Optional<AccountVo> get(Long accountId) {

		return this.accountDao.findById(accountId);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Optional<AuthGeneralVo> getAuthGeneral(long accountId) {

		return this.authGeneralDao.findById(accountId);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Optional<AccountVo> getByEmail(String email) {

		return this.accountDao.findByLoginEmail(email);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Optional<PersonalInformationVo> getPersonalInformation(long accountId) {

		return this.personalInformationDao.findById(accountId);
	}

	/**
	 * 로그인 횟수 증가
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 */
	@VisibleForTesting
	void incrementLoginCount(long accountId) {

		final AccountVo account = this.accountDao.findById(accountId)
			.orElseThrow(LogicException::new);

		account.setLatelyLogin(LocalDateTime.now());
		final long current = account.getLoginCount();
		final long nextCount = current + 1;
		account.setLoginCount(nextCount);
		this.accountDao.save(account);
	}

	/**
	 * 패스워드 틀린 횟수 초기화
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 */
	@VisibleForTesting
	void initializeWrongPasswordCount(long accountId) {

		final AuthGeneralVo authGeneral = this.authGeneralDao.findById(accountId)
			.orElseThrow(LogicException::new);

		if (authGeneral.getWrongPasswordCount() != 0) {
			authGeneral.setWrongPasswordCount(0);
			this.authGeneralDao.save(authGeneral);
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Transactional
	@Override
	public void successLogin(long accountId) {

		// 로그인 횟수 증가
		this.incrementLoginCount(accountId);

		// 패스워드 틀린 횟수 초기화
		this.initializeWrongPasswordCount(accountId);

		// 로그인 성공 플래그 추가
		this.flaggedLoginSuccess(accountId);

	}

}
