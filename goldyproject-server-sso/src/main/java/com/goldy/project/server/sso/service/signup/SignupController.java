/**
 * FileName : {@link SignupController}.java
 * Created : 2018. 9. 10. 오후 7:53:44
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.signup;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;

import com.goldy.gtils.does.SonarHelper;
import com.goldy.gtils.email.model.EmailModel;
import com.goldy.gtils.exception.LogicErrorType;
import com.goldy.gtils.exception.LogicException;
import com.goldy.gtils.exception.NoPrincipalException;
import com.goldy.gtils.inspect.ObjectInspection;
import com.goldy.gtils.type.Gender;
import com.goldy.gtils.user.service.signup.EmailAuthHelperService;
import com.goldy.gtils.user.service.signup.controller.SignupException;
import com.goldy.gtils.utils.HttpServletGtils;
import com.goldy.gtils.utils.NullGtils;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.form.SignupForm;
import com.goldy.project.server.sso.service.security.SecurityService;
import com.goldy.project.server.sso.utils.TempUtils;
import com.google.common.annotations.VisibleForTesting;

@Controller
public class SignupController {

	/**
	 * slf4j Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SignupController.class);

	private final SignupService signupService;

	private final SignupPageHandler signupPageHandler;

	private final EmailAuthHelperService emailAuthHelperService;

	private final SecurityService securityServiceImpl;

	/**
	 * {@link SignupController} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 1. 22. 오후 10:19:31 jeonghyun.kum
	 */
	@Autowired
	public SignupController(SecurityService securityServiceImpl,
		SignupService signupServiceImpl,
		SignupPageHandler signupHandler, EmailAuthHelperService emailAuthHelperService) {

		this.securityServiceImpl = securityServiceImpl;
		this.signupService = signupServiceImpl;
		this.signupPageHandler = signupHandler;
		this.emailAuthHelperService = emailAuthHelperService;
	}

	private String generateEmailAuthenticationUrl(HttpServletRequest servletRequest, AccountVo accountVo) {

		final String authCode = this.emailAuthHelperService.generateAuthCode(accountVo.getLoginEmail());
		final String baseUrl = HttpServletGtils.extractBaseUrl(servletRequest);

		return baseUrl + "/signup/auth/email?authcode=" + authCode;
	}

	/**
	 * 회원가입 페이지로 이동합니다.
	 *
	 * @author jeonghyun.kum
	 * @since 2016. 4. 25. 오전 1:14:59
	 * @param servletRequest
	 *            서블릿 요청 정보
	 * @param webRequest
	 *            웹 요청 정보
	 * @param model
	 *            페이지 모델
	 * @return registrationForm.jsp
	 */
	@GetMapping(value = "/signup")
	public String getSignupForm(HttpServletRequest servletRequest, WebRequest webRequest, Model model) {

		// TODO 아래 인자는 Social 쓸때 사용할 예정
		SonarHelper.soFar(servletRequest, webRequest);

		final SignupForm form = new SignupForm();
		form.setEmail("hokkk01@naver.com");
		form.setGender(Gender.FEMALE);
		form.setNickName("Goldy");
		form.setPhoneNumber("010-5055-6284");
		form.setUserName("금정현");

		model.addAttribute("signupForm", form);

		return this.signupPageHandler.getSignupPage();
	}

	/**
	 * 이메일 인증 페이지
	 *
	 * @author jeonghyun.kum
	 * @param authentication
	 *            로그인 인증 정보
	 * @param authcode
	 *            인증 코드
	 * @return 인증 결과에 따른 페이지
	 */
	@GetMapping(value = "/signup/auth/email")
	public String processEmailAuth(@AuthenticationPrincipal Authentication authentication,
		@RequestParam String authcode) {

		final String uniqueKey = authentication.getName();
		if (this.emailAuthHelperService.isValidAuthenticationCode(uniqueKey, authcode) == false) {
			return this.signupPageHandler.getFailEmailAuthPage();
		}

		long accountId;
		try {
			accountId = this.securityServiceImpl.getUserDetail(authentication).getAccount().getId();
		} catch (final NoPrincipalException ex) {
			throw new LogicException(LogicErrorType.INVALID_DESIGN, ex);
		}

		if (this.signupService.isAlreadyRegistredUser(accountId)) {
			return this.signupPageHandler.getAlreadyRegistredUserPage();
		}

		this.signupService.defineUserRole(accountId);

		return this.signupPageHandler.getFinishEmailAuthPage();
	}

	@VisibleForTesting
	void sendAuthEmail(HttpServletRequest servletRequest, AccountVo accountVo) {

		final String emailAuthUrl = this.generateEmailAuthenticationUrl(servletRequest, accountVo);

		final EmailModel emailModel = this.signupService.createEmailModel(accountVo.getLoginEmail(), emailAuthUrl);

		this.emailAuthHelperService.sendEmail(emailModel);
	}

	/**
	 * 회원가입 폼을 검사합니다.
	 *
	 * @author jeonghyun.kum
	 * @since 2016. 4. 25. 오전 1:14:59
	 * @param signupForm
	 *            회원가입 폼
	 * @param bindingResult
	 *            사용자 입력 검사 결과
	 * @param model
	 *            JSP Model
	 * @return 사용자 입력 오류를 판단후 다음과 같은 작업을 진행합니다.
	 *         <ul>
	 *         <li>성공적으로 수행하였을 경우 : redirect:/login/auth?entry=signup</li>
	 *         <li>폼검사에 실패하였을 경우 registrationForm.jsp</li>
	 *         </ul>
	 */
	@PostMapping(value = "/signup")
	@Transactional
	public String signup(@Valid @ModelAttribute SignupForm signupForm, BindingResult bindingResult, Model model,
		HttpServletRequest servletRequest) {

		model.addAttribute("signupForm", signupForm);

		if (bindingResult.hasErrors()) {
			return this.signupPageHandler.getSignupPage();
		}

		return this.signup(signupForm, servletRequest, bindingResult);
	}

	@VisibleForTesting
	String signup(SignupForm signupForm, HttpServletRequest servletRequest, BindingResult bindingResult) {

		ObjectInspection.checkNull(signupForm, servletRequest, bindingResult).inspect();

		AccountVo accountVo;
		try {
			final AccountVo tempAccount = this.signupService.signup(signupForm);
			accountVo = NullGtils.throwIfNull(tempAccount);
		} catch (final SignupException e) {
			if ("already regestred and authenticated email".equals(e.getMessage())) {
				TempUtils.addFieldError(bindingResult, "email", "이미 등록된 이메일 입니다.");
				LOGGER.trace("이미 등록된 이메일로 회원가입 실패", e);
			} else {
				LOGGER.error("알 수 없는 이유로 회원가입에 실패", e);
				throw new LogicException(LogicErrorType.INVALID_DESIGN, e);
			}
			return this.signupPageHandler.getSignupPage();
		}

		try {
			// 회원가입 후 해당 사용자를 로그인한다.
			this.securityServiceImpl.login(accountVo.getId());
		} catch (final NoPrincipalException e) {
			throw new LogicException(LogicErrorType.INVALID_DESIGN, "", e);
		}

		this.sendAuthEmail(servletRequest, accountVo);

		return this.signupPageHandler.getSendedEmailPage();
	}
}
