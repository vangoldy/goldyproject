/**
 * FileName : {@link PersonalInformationVo}.java
 * Created : 2018. 9. 10. 오후 8:25:47
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;
import com.goldy.gtils.type.Gender;

@Entity(name = SsoTable.PERSONAL_INFORMATION)
public class PersonalInformationVo implements Serializable {

	/** Serial Version UID */
	private static final long serialVersionUID = -8303091547574430393L;

	@Id
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId(SsoTable.ACCOUNT)
	private long accountId;

	private String userName;

	private String nickname;

	private String phoneNumber;

	@Enumerated(EnumType.STRING)
	private Gender gender;

	@Column(columnDefinition = "DATE")
	private LocalDate birthday;

	/**
	 * userId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return accountId
	 */
	public long getAccountId() {

		return this.accountId;
	}

	/**
	 * birthday를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return birthday
	 */
	public LocalDate getBirthday() {

		return this.birthday;
	}

	/**
	 * gender를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return gender
	 */
	public Gender getGender() {

		return this.gender;
	}

	/**
	 * nickName를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return nickName
	 */
	public String getNickname() {

		return this.nickname;
	}

	/**
	 * phoneNumber를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return phoneNumber
	 */
	public String getPhoneNumber() {

		return this.phoneNumber;
	}

	/**
	 * userName를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return userName
	 */
	public String getUserName() {

		return this.userName;
	}

	/**
	 * accountId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            초기화 값
	 */
	public void setAccountId(long accountId) {

		this.accountId = accountId;
	}

	/**
	 * birthday 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param birthday
	 *            초기화 값
	 */
	public void setBirthday(LocalDate birthday) {

		this.birthday = birthday;
	}

	/**
	 * gender 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param gender
	 *            초기화 값
	 */
	public void setGender(Gender gender) {

		this.gender = gender;
	}

	/**
	 * nickName 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param nickName
	 *            초기화 값
	 */
	public void setNickname(String nickName) {

		this.nickname = nickName;
	}

	/**
	 * phoneNumber 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param phoneNumber
	 *            초기화 값
	 */
	public void setPhoneNumber(String phoneNumber) {

		this.phoneNumber = phoneNumber;
	}

	/**
	 * userName 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param userName
	 *            초기화 값
	 */
	public void setUserName(String userName) {

		this.userName = userName;
	}

}
