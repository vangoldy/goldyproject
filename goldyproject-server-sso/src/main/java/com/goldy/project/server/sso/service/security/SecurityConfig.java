/**
 * FileName : {@link SecurityConfig}.java
 * Created : 2018. 8. 31. 오후 10:41:29
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Configuration
@Import({
		UserDetailsServiceImpl.class,
		AuthenticationSuccessHandlerImpl.class,
		AuthenticationFailureHandlerImpl.class,
		AuthenticationEventListener.class,
		SecurityServiceImpl.class,
})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private AuthenticationSuccessHandler successHandler;

	@Autowired
	private AuthenticationFailureHandler failureHandler;

	/**
	 * {@link SecurityConfig} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 9. 오전 11:25:44 jeonghyun.kum
	 */
	public SecurityConfig() {

		super();

	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(this.userDetailsService)
			.passwordEncoder(this.passwordEncoder);

		auth.inMemoryAuthentication()
			.withUser("john")
			.password(this.passwordEncoder.encode("123"))
			.roles("USER");
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests()
			.antMatchers(HttpMethod.GET, "/account/**").permitAll()
			.antMatchers(HttpMethod.GET, "/account2/**").permitAll()
			.antMatchers(HttpMethod.GET, "/account3/**").permitAll()
			.antMatchers(HttpMethod.GET, "/account4/**").permitAll()
			.antMatchers(HttpMethod.GET, "/resources/**").permitAll()
			.antMatchers(HttpMethod.GET, "/signup").permitAll()
			.antMatchers(HttpMethod.POST, "/signup").permitAll()
			.antMatchers(HttpMethod.GET, "/signup/auth/email").authenticated()
			.antMatchers(HttpMethod.GET, "/about").permitAll()
			.antMatchers(HttpMethod.GET, "/session/signin").permitAll()
			.antMatchers(HttpMethod.POST, "/session/signin/processing").permitAll()
			.antMatchers(HttpMethod.GET, "/session/signout").authenticated()
			.antMatchers(HttpMethod.GET, "/", "/actuator/health").permitAll()
			.anyRequest().authenticated()
			.and()
			.formLogin()
			.loginPage("/session/signin")
			.loginProcessingUrl("/session/signin/processing")
			.successHandler(this.successHandler)
			.failureHandler(this.failureHandler)
			.permitAll();
	}

}
