/**
 * FileName : {@link AccountService}.java
 * Created : 2019. 1. 27. 오후 10:25:14
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.account;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;

import org.springframework.validation.annotation.Validated;

import com.goldy.project.server.sso.model.entity.AccountAccessHistoryVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.service.DataService;

/**
 * 계정 관리 서비스
 */
@Validated
public interface AccountService extends DataService<AccountVo, Long> {

	/**
	 * {@link HttpServletRequest}로부터 사용자 요청 정보를 가져와 계정 접근 히스토리를 생성합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 * @param servletRequest
	 *            사용자 요청정보 {@link HttpServletRequest}
	 * @return 계정 접근 히스토리 VO
	 */
	AccountAccessHistoryVo createAccessHistory(@Min(1) long accountId, HttpServletRequest servletRequest);

	/**
	 * 계정 ID에 해당하는 {@link AuthGeneralVo} 값을 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 * @return 일반 회원 정보
	 */
	Optional<AuthGeneralVo> getAuthGeneral(@Min(1) long accountId);

	/**
	 * 이메일에 해당하는 {@link AccountVo} 값을 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param email
	 *            찾으려는 사용자 Email
	 * @return 해당하는 {@link AccountVo}
	 */
	Optional<AccountVo> getByEmail(@Email String email);

	/**
	 * 계정 ID에 해당하는 개인정보를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 * @return 해당하는 개인정보 {@link PersonalInformationVo}
	 */
	Optional<PersonalInformationVo> getPersonalInformation(@Min(1) long accountId);

	/**
	 * 로그인 완료 처리를 수행합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 */
	void successLogin(@Min(1) long accountId);

}
