/**
 * FileName : {@link SpringSecurityRoleCast}.java
 * Created : 2019. 2. 10. 오후 1:56:29
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security.role;

import java.util.Collection;
import java.util.Objects;
import java.util.stream.Collectors;

import com.goldy.gtils.utils.EnumGtils;

/**
 * Spring Security 및 SSO 서버에서 컨트롤이 가능한 ROLE을 추출하여 {@link SpringSecurityRole}에 대입하는 클래스
 * ROLE 문자열 중 "ROLE_"로 시작하는 문자열을 대상으로 하며 {@link SpringSecurityRole}에서 관리되는 문자열만 정상 캐스팅 됨
 */
public class SpringSecurityRoleCast implements RoleCast<SpringSecurityRole> {

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Collection<SpringSecurityRole> casting(Collection<String> roles) {

		return roles.stream()
			.filter(role -> role.startsWith("ROLE_"))
			.map(stringValue -> EnumGtils.getOfStringValueRole(SpringSecurityRole.class, stringValue))
			.filter(Objects::nonNull)
			.collect(Collectors.toList());
	}

}
