/**
 * FileName : package-info.java
 * Created : 2018. 9. 9. 오후 1:44:07
 * Author : jeonghyun.kum Jpa \@Entity 테이블 데이터 모델
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.entity;
