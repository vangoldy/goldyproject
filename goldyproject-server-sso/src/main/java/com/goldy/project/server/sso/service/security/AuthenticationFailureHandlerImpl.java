/**
 * FileName : {@link AuthenticationFailureHandlerImpl}.java
 * Created : 2019. 1. 29. 오후 10:44:04
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationFailureHandlerImpl extends ExceptionMappingAuthenticationFailureHandler {

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest servletRequest, HttpServletResponse servletResponse,
		AuthenticationException exception) throws IOException, ServletException {

		super.onAuthenticationFailure(servletRequest, servletResponse, exception);

	}

}
