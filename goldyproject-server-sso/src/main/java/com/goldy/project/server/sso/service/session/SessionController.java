/**
 * FileName : {@link SessionController}.java
 * Created : 2019. 1. 27. 오전 1:58:22
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.session;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.goldy.gtils.utils.HttpServletGtils;
import com.goldy.project.server.sso.model.form.LoginForm;
import com.goldy.project.server.sso.service.security.SecurityService;
import com.goldy.project.server.sso.service.security.SecurityUserDetail;

@Controller
public class SessionController {

	/**
	 * slf4j Logger
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SessionController.class);

	@Autowired
	private SecurityService securityServiceImpl;

	@GetMapping("/session/exit")
	public void exit(Authentication authentication, HttpServletRequest servletRequest,
		HttpServletResponse servletResponse) {

		if (authentication == null) {

			LOGGER.debug("세션이 없는 사용자가 로그아웃합니다: session id:{}", servletRequest.getSession().getId());

			final Cookie[] cookies = servletRequest.getCookies();
			for (final Cookie cookie : cookies) {
				if ("JSESSIONID".equals(cookie.getName())) {
					LOGGER.debug("JSESSIONID id {}", cookie.getValue());
				}
			}
		} else {
			LOGGER.debug("{}을 로그아웃합니다", authentication.getName());
		}
		this.securityServiceImpl.logout(authentication, servletRequest, servletResponse);
	}

	@GetMapping(value = "/session/signin")
	public String getMethodName(HttpServletRequest servletRequest, Model model) {

		model.addAttribute("LOGIN_PROCESS_URL", "/session/signin/processing");
		final LoginForm loginForm = new LoginForm();
		loginForm.setUsername("zzz");

		model.addAttribute("loginForm", loginForm);
		model.addAttribute("username", "sdf");

		final String referrer = HttpServletGtils.getReferer(servletRequest);
		if (referrer != null) {
			servletRequest.getSession().setAttribute("referer", referrer);
		}
		return "annoymous/Login/index";
	}

	@GetMapping("/session/api/user/me")
	public SecurityUserDetail user(@AuthenticationPrincipal SecurityUserDetail principal) {

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final Object principal2 = authentication.getPrincipal();
		System.out.println(principal2);
		System.out.println(principal);
		return principal;
	}

}
