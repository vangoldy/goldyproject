/**
 * FileName : {@link AccountVo}.java
 * Created : 2018. 9. 9. 오후 1:48:31
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;
import com.goldy.project.module.webcommon.db.entity.TimeEntity;

@Entity(name = SsoTable.ACCOUNT)
public class AccountVo extends TimeEntity implements Serializable {

	/** Serial Version UID */
	private static final long serialVersionUID = -161794024451807242L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId(SsoTable.ACCOUNT)
	private Long id;

	private String loginEmail;

	private boolean loginEnable;

	private boolean loginNonExpired;

	private boolean credentialsNonExpired;

	private boolean loginNonLocked;

	private LocalDateTime latelyLogin;

	private long loginCount;

	/**
	 * id를 반환합니다.
	 *
	 * @return id
	 * @author jeonghyun.kum
	 */
	public Long getId() {

		return this.id;
	}

	/**
	 * loginCount를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginCount
	 */
	public long getLoginCount() {

		return this.loginCount;
	}

	/**
	 * loginEmail를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginEmail
	 */
	public String getLoginEmail() {

		return this.loginEmail;
	}

	/**
	 * credentialsNoneExpired를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return credentialsNoneExpired
	 */
	public boolean isCredentialsNonExpired() {

		return this.credentialsNonExpired;
	}

	/**
	 * latelyLogin를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return latelyLogin
	 */
	public LocalDateTime isLatelyLogin() {

		return this.latelyLogin;
	}

	/**
	 * loginEnable를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginEnable
	 */
	public boolean isLoginEnable() {

		return this.loginEnable;
	}

	/**
	 * loginNonExpired를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginNonExpired
	 */
	public boolean isLoginNonExpired() {

		return this.loginNonExpired;
	}

	/**
	 * loginNonLocked를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginNonLocked
	 */
	public boolean isLoginNonLocked() {

		return this.loginNonLocked;
	}

	/**
	 * credentialsNoneExpired 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param credentialsNoneExpired
	 *            초기화 값
	 */
	public void setCredentialsNonExpired(boolean credentialsNoneExpired) {

		this.credentialsNonExpired = credentialsNoneExpired;
	}

	/**
	 * id 초기화 합니다.
	 *
	 * @param id
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * latelyLogin 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param latelyLogin
	 *            초기화 값
	 */
	public void setLatelyLogin(LocalDateTime latelyLogin) {

		this.latelyLogin = latelyLogin;
	}

	/**
	 * loginCount 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginCount
	 *            초기화 값
	 */
	public void setLoginCount(long loginCount) {

		this.loginCount = loginCount;
	}

	/**
	 * loginEmail 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginEmail
	 *            초기화 값
	 */
	public void setLoginEmail(String loginEmail) {

		this.loginEmail = loginEmail;
	}

	/**
	 * loginEnable 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginEnable
	 *            초기화 값
	 */
	public void setLoginEnable(boolean loginEnable) {

		this.loginEnable = loginEnable;
	}

	/**
	 * loginNonExpired 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginNonExpired
	 *            초기화 값
	 */
	public void setLoginNonExpired(boolean loginNonExpired) {

		this.loginNonExpired = loginNonExpired;
	}

	/**
	 * loginNonLocked 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginNonLocked
	 *            초기화 값
	 */
	public void setLoginNonLocked(boolean loginNonLocked) {

		this.loginNonLocked = loginNonLocked;
	}

}
