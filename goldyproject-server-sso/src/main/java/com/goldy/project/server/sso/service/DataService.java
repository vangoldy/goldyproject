/**
 * FileName : {@link DataService}.java
 * Created : 2019. 1. 27. 오후 10:25:19
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.goldy.gtils.exception.InspectException;
import com.goldy.gtils.inspection.StringInspection;
import com.goldy.gtils.utils.ReflectionGtils;
import com.goldy.gtils.utils.StringCollectionGtils;
import com.goldy.project.module.webcommon.db.entity.TimeEntity;

public interface DataService<T extends TimeEntity, ID> {

	default String findEntityName() {

		final List<Class<?>> generics = ReflectionGtils.getGenerics(getClass());

		Class<T> genericT;
		try {
			genericT = (Class<T>) generics.get(0);
		} catch (final ClassCastException ex) {
			return "";
		}

		final List<String> candidate = new ArrayList<>();

		if (genericT.isAnnotationPresent(Table.class)) {
			final Table table = genericT.getAnnotation(Table.class);
			candidate.add(table.name());
		}

		if (genericT.isAnnotationPresent(Entity.class)) {
			final Entity entity = genericT.getAnnotation(Entity.class);
			candidate.add(entity.name());
		}

		candidate.add(genericT.getSimpleName());

		return StringCollectionGtils.getNotEmpty(candidate);
	}

	default String generateExceptionMessage(ID id) {

		final String entityName = this.findEntityName();

		final StringBuilder builder = new StringBuilder();
		builder.append("Unknown ");
		if (StringInspection.checkBlank(entityName).raise() == false) {
			builder.append(entityName).append(' ');
		}
		builder.append("ID of [").append(id).append(']');
		return builder.toString();
	}

	Optional<T> get(ID id);

	default T getOrThrow(ID id) {

		return get(id)
			.orElseThrow(() -> {
				final String message = generateExceptionMessage(id);
				return new InspectException(message);
			});
	}

}
