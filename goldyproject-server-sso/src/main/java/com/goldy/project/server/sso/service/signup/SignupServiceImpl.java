/**
 * FileName : {@link SignupServiceImpl}.java
 * Created : 2018. 9. 10. 오후 7:24:12
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.signup;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import com.goldy.gtils.email.model.EmailModel;
import com.goldy.gtils.exception.ServerErrorException;
import com.goldy.gtils.user.service.signup.controller.SignupException;
import com.goldy.project.server.sso.model.entity.AccountRoleVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.model.form.SignupForm;
import com.goldy.project.server.sso.service.jpa.dao.AccountDao;
import com.goldy.project.server.sso.service.jpa.dao.AccountRoleDao;
import com.goldy.project.server.sso.service.jpa.dao.AuthGeneralDao;
import com.goldy.project.server.sso.service.jpa.dao.PersonalInformationDao;
import com.goldy.project.server.sso.service.security.role.SpringSecurityRole;

/**
 * 회원가입 서비스
 */
@Transactional
public class SignupServiceImpl implements SignupService {

	/** 패스워드 인코더 */
	@Autowired
	private PasswordEncoder passwordEncoder;

	/** 계정 DAO */
	@Autowired
	private AccountDao accountDao;

	/** 일반 회원 정보 DAO */
	@Autowired
	private AuthGeneralDao authGeneralDao;

	/** 개인 정보 DAO */
	@Autowired
	private PersonalInformationDao personalInformationDao;

	/** 계정 권한 DAO */
	@Autowired
	private AccountRoleDao accountRoleDao;

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public EmailModel createEmailModel(String toEmail, String emailAuthUrl) {

		try {
			final InternetAddress sender = new InternetAddress("jhkume90@gmail.com");
			final EmailModel emailModel = new EmailModel(sender, "이메일 인증");
			emailModel.setText(emailAuthUrl);
			emailModel.setTo(new InternetAddress(toEmail));
			return emailModel;
		} catch (final AddressException e) {
			throw new ServerErrorException(e, "이메일 전송 중 오류 발생");
		}
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void defineUserRole(long accountId) {

		final AccountRoleVo accountRole = new AccountRoleVo();
		accountRole.setAccountId(accountId);
		accountRole.setRole(SpringSecurityRole.USER.getValue());

		this.accountRoleDao.save(accountRole);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public boolean isAlreadyRegistredUser(long accountId) {

		return this.accountRoleDao.existsByAccountIdAndRole(accountId, SpringSecurityRole.USER.getValue());
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public boolean isDuplicateEmail(String email) {

		return this.accountDao.existsByLoginEmail(email);
	}

	/**
	 * 해당 이메일에 해당하는 계정을 저장합니다.
	 *
	 * @author jeonghyun.kum
	 * @param email
	 *            이메일
	 * @return 저장된 계정 VO
	 */
	private AccountVo saveAccount(String email) {

		final AccountVo account = new AccountVo();
		account.setLoginEmail(email);
		account.setLatelyLogin(LocalDateTime.now());
		account.setLoginEnable(true);
		account.setLoginNonExpired(true);
		account.setLoginNonLocked(true);
		account.setCredentialsNonExpired(true);
		return this.accountDao.save(account);
	}

	/**
	 * 일반 회원 정보를 저장합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            생성 된 계정 ID {@link #saveAccount(String)}를 통해 발급
	 * @param email
	 *            이메일
	 * @param password
	 *            패스워드
	 * @return 저장된 일반 회원 정보 VO
	 */
	private AuthGeneralVo saveAuthGeneral(long accountId, String email, String password) {

		final AuthGeneralVo authGeneralVo = new AuthGeneralVo();
		authGeneralVo.setAccountId(accountId);
		authGeneralVo.setLoginId(email);
		authGeneralVo.setLoginPassword(this.passwordEncoder.encode(password));
		authGeneralVo.setPasswordUpdateTime(LocalDateTime.now());
		return this.authGeneralDao.save(authGeneralVo);
	}

	/**
	 * 개인정보를 저장합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            생성 된 계정 ID {@link #saveAccount(String)}를 통해 발급
	 * @param signupForm
	 *            회원가입 폼
	 * @return 저장된 개인 정보 VO
	 */
	private PersonalInformationVo savePi(long accountId, SignupForm signupForm) {

		final PersonalInformationVo personalInformationVo = new PersonalInformationVo();
		personalInformationVo.setAccountId(accountId);
		personalInformationVo.setUserName(signupForm.getUserName());
		personalInformationVo.setNickname(signupForm.getNickName());
		personalInformationVo.setGender(signupForm.getGender());
		personalInformationVo.setPhoneNumber(signupForm.getPhoneNumber());
		personalInformationVo.setBirthday(signupForm.getBirthday());
		return this.personalInformationDao.save(personalInformationVo);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public AccountVo signup(SignupForm signupForm) throws SignupException {

		final String email = signupForm.getEmail();

		final Optional<AccountVo> optionalAccountVo = this.accountDao.findByLoginEmail(email);

		AccountVo account;
		if (optionalAccountVo.isPresent()) {
			account = optionalAccountVo.get();
			if (this.accountRoleDao.existsByAccountIdAndRole(account.getId(), SpringSecurityRole.USER.getValue())) {
				throw new SignupException("already regestred and authenticated email");
			}

		} else {
			account = this.saveAccount(email);
		}
		this.saveAuthGeneral(account.getId(), email, signupForm.getPassword());
		this.savePi(account.getId(), signupForm);

		return account;
	}

}
