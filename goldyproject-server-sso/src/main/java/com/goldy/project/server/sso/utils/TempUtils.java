/**
 * FileName : {@link TempUtils}.java
 * Created : 2019. 2. 2. 오후 10:53:59
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.utils;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

/**
 * 아직 어떤 Utils로 이동할지 몰라서 임시로 생성한 유틸
 */
public class TempUtils {

	/**
	 * {@link TempUtils} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 17. 오후 5:06:01 jeonghyun.kum
	 */
	private TempUtils() {

		// do nothing
	}

	public static void addFieldError(BindingResult bindingResult, String field, String message) {

		final FieldError error = new FieldError("object", field, message);
		bindingResult.addError(error);

	}
}
