/**
 * FileName : {@link AccountAccessHistoryVo}.java
 * Created : 2019. 1. 27. 오후 5:27:04
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;
import com.goldy.project.module.webcommon.db.entity.TimeEntity;

@Entity(name = SsoTable.ACCOUNT_ACCESS_HISTORY)
public class AccountAccessHistoryVo extends TimeEntity implements Serializable {

	/** Serial Version UID */
	private static final long serialVersionUID = -6984446687944159296L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId(SsoTable.ACCOUNT_ACCESS_HISTORY)
	private Long id;

	@Column(nullable = false)
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId(SsoTable.ACCOUNT)
	private long accountId;

	@Column(nullable = false)
	private String sessionId;

	@Column(nullable = false)
	private String agent;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String ip;

	@Column(nullable = true)
	private String jsessionId;

	@Column(nullable = true)
	private String authType;

	@Column(nullable = false)
	private boolean loginSuccessFlag;

	/**
	 * accountId를 반환합니다.
	 *
	 * @return accountId
	 * @author jeonghyun.kum
	 */
	public long getAccountId() {

		return this.accountId;
	}

	/**
	 * agent를 반환합니다.
	 *
	 * @return agent
	 * @author jeonghyun.kum
	 */
	public String getAgent() {

		return this.agent;
	}

	/**
	 * authType를 반환합니다.
	 *
	 * @return authType
	 * @author jeonghyun.kum
	 */
	public String getAuthType() {

		return this.authType;
	}

	/**
	 * id를 반환합니다.
	 *
	 * @return id
	 * @author jeonghyun.kum
	 */
	public Long getId() {

		return this.id;
	}

	/**
	 * ip를 반환합니다.
	 *
	 * @return ip
	 * @author jeonghyun.kum
	 */
	public String getIp() {

		return this.ip;
	}

	/**
	 * jsessionId를 반환합니다.
	 *
	 * @return jsessionId
	 * @author jeonghyun.kum
	 */
	public String getJsessionId() {

		return this.jsessionId;
	}

	/**
	 * name를 반환합니다.
	 *
	 * @return name
	 * @author jeonghyun.kum
	 */
	public String getName() {

		return this.name;
	}

	/**
	 * sessionId를 반환합니다.
	 *
	 * @return sessionId
	 * @author jeonghyun.kum
	 */
	public String getSessionId() {

		return this.sessionId;
	}

	/**
	 * loginSucceesed를 반환합니다.
	 *
	 * @return loginSuccessFlag
	 * @author jeonghyun.kum
	 */
	public boolean isLoginSuccessFlag() {

		return this.loginSuccessFlag;
	}

	/**
	 * accountId 초기화 합니다.
	 *
	 * @param accountId
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setAccountId(long accountId) {

		this.accountId = accountId;
	}

	/**
	 * agent 초기화 합니다.
	 *
	 * @param agent
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setAgent(String agent) {

		this.agent = agent;
	}

	/**
	 * authType 초기화 합니다.
	 *
	 * @param authType
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setAuthType(String authType) {

		this.authType = authType;
	}

	/**
	 * id 초기화 합니다.
	 *
	 * @param id
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * ip 초기화 합니다.
	 *
	 * @param ip
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setIp(String ip) {

		this.ip = ip;
	}

	/**
	 * jsessionId 초기화 합니다.
	 *
	 * @param jsessionId
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setJsessionId(String jsessionId) {

		this.jsessionId = jsessionId;
	}

	/**
	 * loginSuccessFlag 초기화 합니다.
	 *
	 * @param loginSuccessFlag
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setLoginSuccessFlag(boolean loginSucceesed) {

		this.loginSuccessFlag = loginSucceesed;
	}

	/**
	 * name 초기화 합니다.
	 *
	 * @param name
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setName(String name) {

		this.name = name;
	}

	/**
	 * sessionId 초기화 합니다.
	 *
	 * @param sessionId
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setSessionId(String sessionId) {

		this.sessionId = sessionId;
	}

}
