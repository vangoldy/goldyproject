/**
 * FileName : {@link AccountController}.java
 * Created : 2019. 2. 16. 오후 7:41:23
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.account;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.project.server.sso.model.entity.AccountVo;

@RestController
public class AccountController {

	@Autowired
	private AccountService accountService;

	@GetMapping(value = "/account/{accountId}")
	public Optional<AccountVo> getMethodName(@PathVariable long accountId) {

		return this.accountService.get(accountId);
	}

	@GetMapping(value = "/account3/{accountId}")
	public Optional<AccountVo> getMethodName3(@DecryptId("account") @PathVariable long accountId) {

		return this.accountService.get(accountId);
	}

	@GetMapping(value = "/account4")
	public Optional<AccountVo> getMethodName4(@DecryptId("account") @RequestParam long accountId) {

		return this.accountService.get(accountId);
	}

}
