/**
 * FileName : {@link DataSourceConfig}.java
 * Created : 2018. 9. 1. 오전 12:14:53
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.jpa;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@PropertySource("classpath:datasource.properties")
@EntityScan(
	value = "com.goldy.project.server.sso.model.entity",
	basePackageClasses = Jsr310JpaConverters.class)
@EnableJpaRepositories(basePackages = "com.goldy.project.server.sso.service.jpa.dao")
public class DataSourceConfig {

	@Bean
	@ConfigurationProperties("gp.datasource.sso")
	public DataSource dataSource() {

		return new DriverManagerDataSource();
	}

}
