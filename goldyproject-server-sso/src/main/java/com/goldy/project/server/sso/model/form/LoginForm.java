/**
 * FileName : {@link LoginForm}.java
 * Created : 2019. 2. 3. 오후 6:58:51
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.form;

public class LoginForm {

	private String username;

	private String password;

	/**
	 * password를 반환합니다.
	 *
	 * @return password
	 * @author jeonghyun.kum
	 */
	public String getPassword() {

		return this.password;
	}

	/**
	 * username를 반환합니다.
	 *
	 * @return username
	 * @author jeonghyun.kum
	 */
	public String getUsername() {

		return this.username;
	}

	/**
	 * password 초기화 합니다.
	 *
	 * @param password
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setPassword(String password) {

		this.password = password;
	}

	/**
	 * username 초기화 합니다.
	 *
	 * @param username
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setUsername(String username) {

		this.username = username;
	}
}
