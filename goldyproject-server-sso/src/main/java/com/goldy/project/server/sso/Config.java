/**
 * FileName : {@link Config}.java
 * Created : 2018. 8. 31. 오후 10:41:29
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.goldy.project.server.sso.service.ControllerConfig;
import com.goldy.project.server.sso.service.PropertyConfig;
import com.goldy.project.server.sso.service.ServerConfig;
import com.goldy.project.server.sso.service.ServiceConfig;

@Configuration
@EnableWebMvc
@Import({
		PropertyConfig.class,
		ServerConfig.class,
		ServiceConfig.class,
		ControllerConfig.class,
})

public class Config {
	// do nothing
}
