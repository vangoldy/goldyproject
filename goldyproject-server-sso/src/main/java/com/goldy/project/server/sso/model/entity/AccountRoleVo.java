/**
 * FileName : {@link AccountRoleVo}.java
 * Created : 2018. 9. 9. 오후 2:44:03
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;

@Entity(name = SsoTable.ACCOUNT_ROLE)
public class AccountRoleVo implements Serializable {

	/** Serial Version UID */
	private static final long serialVersionUID = 2838019734334647143L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId(SsoTable.ACCOUNT_ROLE)
	private Long id;

	@Column(nullable = false)
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId(SsoTable.ACCOUNT)
	private long accountId;

	@Column(nullable = false)
	private String role;

	/**
	 * userId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return accountId
	 */
	public long getAccountId() {

		return this.accountId;
	}

	/**
	 * id를 반환합니다.
	 *
	 * @return id
	 * @author jeonghyun.kum
	 */
	public Long getId() {

		return this.id;
	}

	/**
	 * role를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return role
	 */
	public String getRole() {

		return this.role;
	}

	/**
	 * accountId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            초기화 값
	 */
	public void setAccountId(long accountId) {

		this.accountId = accountId;
	}

	/**
	 * id 초기화 합니다.
	 *
	 * @param id
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * role 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param role
	 *            초기화 값
	 */
	public void setRole(String role) {

		this.role = role;
	}

}
