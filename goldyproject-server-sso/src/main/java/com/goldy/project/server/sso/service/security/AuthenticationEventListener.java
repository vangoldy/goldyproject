/**
 * FileName : {@link AuthenticationEventListener}.java
 * Created : 2019. 2. 3. 오후 5:52:23
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.service.jpa.dao.AuthGeneralDao;

@Component
public class AuthenticationEventListener {

	@Autowired
	private AuthGeneralDao authGeneralDao;

	@EventListener
	public void authenticationFailed(AuthenticationFailureBadCredentialsEvent event) {

		final AuthenticationException exception = event.getException();

		// 패스워드 틀릴때
		if (exception instanceof BadCredentialsException) {
			final Object principal = event.getAuthentication().getPrincipal();
			if (principal instanceof String) {
				final String loginEmail = (String) principal;

				this.authGeneralDao
					.findByLoginId(loginEmail)
					.ifPresent(this::incrementWrongPasswordCount);
			}
		}

	}

	private void incrementWrongPasswordCount(AuthGeneralVo authGeneral) {

		final int wrongPasswordCount = authGeneral.getWrongPasswordCount();
		final int nextCount = wrongPasswordCount + 1;

		authGeneral.setWrongPasswordCount(nextCount);
		this.authGeneralDao.save(authGeneral);
	}

}
