/**
 * FileName : {@link AccountAccessHistoryDao}.java
 * Created : 2019. 1. 27. 오후 5:37:54
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.jpa.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.goldy.project.server.sso.model.entity.AccountAccessHistoryVo;

/**
 * "account_access_history" 테이블 JPA Repository
 */
public interface AccountAccessHistoryDao extends JpaRepository<AccountAccessHistoryVo, Long> {

	/**
	 * "account_id" 필드에 해당하는 VO 페이지를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            "account_id" 필드 값
	 * @param pageable
	 *            페이지 옵션
	 * @return {@link AccountAccessHistoryVo} 페이지
	 */
	Page<AccountAccessHistoryVo> findByAccountId(long accountId, Pageable pageable);

	/**
	 * "account_id"에 해당하는 VO 중 마지막 데이터를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            "account_id" 필드 값
	 * @return 마지막 데이터
	 */
	Optional<AccountAccessHistoryVo> findTop1ByAccountIdOrderByCreatedTimeDescIdDesc(long accountId);

}
