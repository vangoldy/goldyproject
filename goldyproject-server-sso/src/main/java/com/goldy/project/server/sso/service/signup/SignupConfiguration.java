/**
 * FileName : {@link SignupConfiguration}.java
 * Created : 2019. 1. 31. 오후 10:46:34
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.signup;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.mail.javamail.JavaMailSender;

import com.goldy.gtils.email.Email;
import com.goldy.gtils.email.sender.GmailSender;
import com.goldy.gtils.encryption.Encryption;
import com.goldy.gtils.encryption.exception.EncryptionException;
import com.goldy.gtils.user.service.signup.EmailAuthHelperServiceImpl;

@Configuration
@Import({
		SignupPageHandler.class,
		SignupController.class,
		Email.class,
		SignupPageHandler.class,
		EmailAuthHelperServiceImpl.class,
})
public class SignupConfiguration {

	@Bean
	public JavaMailSender javaMailSender() throws EncryptionException {

		return new GmailSender("jhkume90@gmail.com", new Encryption().decrypt("p/0I21uv7pwVHi2QDln+iw=="));
	}
}
