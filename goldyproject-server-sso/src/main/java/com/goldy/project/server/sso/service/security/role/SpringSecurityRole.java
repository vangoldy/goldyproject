/**
 * FileName : {@link SpringSecurityRole}.java
 * Created : 2019. 2. 10. 오후 1:57:02
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security.role;

import com.goldy.gtils.type.EnumStringValue;

public enum SpringSecurityRole implements EnumStringValue {

	/** 사용자 권한 */
	USER("ROLE_USER"),

	/** 관리자 권한 */
	ADMIN("ROLE_ADMIN");

	/** 실제 이름 */
	private final String value;

	/**
	 * {@link SpringSecurityRole} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 10. 오후 1:58:58 jeonghyun.kum
	 */
	SpringSecurityRole(String value) {

		this.value = value;
	}

	/**
	 * fullName를 반환합니다.
	 *
	 * @return value
	 * @author jeonghyun.kum
	 */
	@Override
	public String getValue() {

		return this.value;
	}

}
