/**
 * FileName : {@link SsoTable}.java
 * Created : 2019. 2. 16. 오후 10:20:18
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.entity;

public class SsoTable {

	public static final String ACCOUNT = "account";

	public static final String ACCOUNT_ACCESS_HISTORY = "account_access_history";

	public static final String ACCOUNT_ROLE = "account_role";

	public static final String AUTH_GENERAL = "auth_general";

	public static final String PERSONAL_INFORMATION = "personal_information";
}
