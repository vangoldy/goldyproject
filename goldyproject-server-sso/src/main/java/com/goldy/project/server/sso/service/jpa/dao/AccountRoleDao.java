/**
 * FileName : {@link AccountRoleDao}.java
 * Created : 2018. 9. 9. 오후 2:45:08
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.jpa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.goldy.project.server.sso.model.entity.AccountRoleVo;

/**
 * "account_role" 테이블 JPA Repository
 */
public interface AccountRoleDao extends JpaRepository<AccountRoleVo, Long> {

	/**
	 * "account_id" 및 "role"이 일치한 데이터가 있는지 여부를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            "account_id" 필드 값
	 * @param role
	 *            "role" 필드 값
	 * @return 존재 여부
	 */
	boolean existsByAccountIdAndRole(Long accountId, String role);

	/**
	 * "account_id"를 포함한 VO 목록을 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            "account_id" 필드 값
	 * @param pageable
	 *            페이지 옵션
	 * @return {@link AccountRoleVo} 페이지
	 */
	Page<AccountRoleVo> findByAccountId(long accountId, Pageable pageable);
}
