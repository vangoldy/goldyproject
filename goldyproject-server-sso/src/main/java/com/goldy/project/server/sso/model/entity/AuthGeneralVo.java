/**
 * FileName : {@link AuthGeneralVo}.java
 * Created : 2018. 9. 9. 오후 1:54:13
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.model.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;
import com.goldy.project.module.webcommon.db.entity.TimeEntity;

@Entity(name = SsoTable.AUTH_GENERAL)
public class AuthGeneralVo extends TimeEntity implements Serializable, LoginProviderUserAuth {

	/** Serial Version UID */
	private static final long serialVersionUID = -1758233959446238937L;

	@Id
	@Column(nullable = false, columnDefinition = "BIGINT UNSIGNED")
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId(SsoTable.AUTH_GENERAL)
	private long accountId;

	private String loginId;

	private String loginPassword;

	private LocalDateTime passwordUpdateTime;

	private int wrongPasswordCount;

	/**
	 * userId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return accountId
	 */
	public long getAccountId() {

		return this.accountId;
	}

	/**
	 * loginId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginId
	 */
	public String getLoginId() {

		return this.loginId;
	}

	/**
	 * loginPassword를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return loginPassword
	 */
	public String getLoginPassword() {

		return this.loginPassword;
	}

	/**
	 * passwordUpdateTime를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return passwordUpdateTime
	 */
	public LocalDateTime getPasswordUpdateTime() {

		return this.passwordUpdateTime;
	}

	/**
	 * wrongPasswordCount를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return wrongPasswordCount
	 */
	public int getWrongPasswordCount() {

		return this.wrongPasswordCount;
	}

	/**
	 * accountId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            초기화 값
	 */
	public void setAccountId(long userId) {

		this.accountId = userId;
	}

	/**
	 * loginId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginId
	 *            초기화 값
	 */
	public void setLoginId(String loginId) {

		this.loginId = loginId;
	}

	/**
	 * loginPassword 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param loginPassword
	 *            초기화 값
	 */
	public void setLoginPassword(String loginPassword) {

		this.loginPassword = loginPassword;
	}

	/**
	 * passwordUpdateTime 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param passwordUpdateTime
	 *            초기화 값
	 */
	public void setPasswordUpdateTime(LocalDateTime passwordUpdateTime) {

		this.passwordUpdateTime = passwordUpdateTime;
	}

	/**
	 * wrongPasswordCount 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param wrongPasswordCount
	 *            초기화 값
	 */
	public void setWrongPasswordCount(int wrongPasswordCount) {

		this.wrongPasswordCount = wrongPasswordCount;
	}
}
