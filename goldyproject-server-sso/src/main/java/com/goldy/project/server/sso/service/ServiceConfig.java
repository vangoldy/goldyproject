/**
 * FileName : {@link ServiceConfig}.java
 * Created : 2019. 1. 29. 오후 11:34:02
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.goldy.project.server.sso.service.account.AccountServiceImpl;
import com.goldy.project.server.sso.service.signup.SignupConfiguration;
import com.goldy.project.server.sso.service.signup.SignupServiceImpl;

@Configuration
@Import({
		SignupServiceImpl.class,
		AccountServiceImpl.class,
		SignupConfiguration.class,
})
public class ServiceConfig {
	// do nothing
}
