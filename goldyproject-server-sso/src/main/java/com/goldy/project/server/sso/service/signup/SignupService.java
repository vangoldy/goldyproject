/**
 * FileName : {@link SignupService}.java
 * Created : 2019. 2. 10. 오전 1:12:28
 * Author : jeonghyun.kum
 * Copyright (C) 2019 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.signup;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;

import org.springframework.validation.annotation.Validated;

import com.goldy.gtils.email.model.EmailModel;
import com.goldy.gtils.user.service.signup.controller.SignupException;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.form.SignupForm;

@Validated
public interface SignupService {

	/**
	 * 이메일 인증에 사용되는 이메일 모델
	 *
	 * @author jeonghyun.kum
	 * @param toEmail
	 *            전송할 이메일
	 * @param emailAuthUrl
	 *            인증 코드
	 * @return 이메일 모델
	 */
	EmailModel createEmailModel(@Email String toEmail, String emailAuthUrl);

	void defineUserRole(@Min(1) long accountId);

	/**
	 * 해당 유저가 이미 등록된 유저인지 확인
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            Account ID
	 * @return true인경우 이미 등록된 유저
	 */
	boolean isAlreadyRegistredUser(@Min(1) long accountId);

	/**
	 * 이미 중복된 이메일 검사
	 *
	 * @author jeonghyun.kum
	 * @param email
	 *            검사 대상 이메일 문자열
	 * @return true인경우 이미 등록된 이메일
	 */
	boolean isDuplicateEmail(@Email String email);

	/**
	 * 회원가입 폼을 입력받아 사용자 계정을 생성한다.
	 *
	 * @author jeonghyun.kum
	 * @param signupForm
	 *            회원가입 폼
	 * @return 생성된 계정 ID (account 테이블 ID)
	 * @throws SignupException
	 *             이미 등록된 이메일이거나, 문제가 있는 경우 발생
	 */
	AccountVo signup(SignupForm signupForm) throws SignupException;

}
