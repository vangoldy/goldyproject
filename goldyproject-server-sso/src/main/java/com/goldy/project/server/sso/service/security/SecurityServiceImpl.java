/**
 * FileName : {@link SecurityServiceImpl}.java
 * Created : 2017. 4. 10.
 * Author : jeong
 * Copyright (C) 2017 Goldy Project. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 Goldy Project에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.sso.service.security;

import java.io.IOException;
import java.util.Collection;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import com.goldy.gtils.exception.LogicErrorType;
import com.goldy.gtils.exception.LogicException;
import com.goldy.gtils.exception.NoPrincipalException;
import com.goldy.project.server.sso.model.entity.AccountRoleVo;
import com.goldy.project.server.sso.model.entity.AccountVo;
import com.goldy.project.server.sso.model.entity.AuthGeneralVo;
import com.goldy.project.server.sso.model.entity.PersonalInformationVo;
import com.goldy.project.server.sso.model.type.LoginProvider;
import com.goldy.project.server.sso.service.account.AccountService;
import com.goldy.project.server.sso.service.jpa.dao.AccountRoleDao;
import com.goldy.project.server.sso.service.security.role.SpringSecurityRole;
import com.goldy.project.server.sso.service.security.role.SpringSecurityRoleCast;
import com.google.common.annotations.VisibleForTesting;

/**
 * Security 우회 코드입니다.
 *
 * @author jeonghyun.kum
 * @since 2016. 4. 23. 오후 10:22:52
 */
public class SecurityServiceImpl implements SecurityService {

	/**
	 * slf4j Logger
	 *
	 * @author jeong
	 * @since 2017. 5. 22. 오후 9:20:02
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityServiceImpl.class);

	/** 계정 관리 서비스 */
	@Autowired
	private AccountService accountService;

	/** "account_role" 테이블 JPA Repository */
	@Autowired
	private AccountRoleDao accountRoleDao;

	/**
	 * {@link SecurityServiceImpl} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author jeong
	 * @since 2017. 4. 10. 오후 9:36:36
	 */
	public SecurityServiceImpl() {

		super();
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Collection<String> getAllRoles(long accountId) {

		return this.accountRoleDao.findByAccountId(accountId, Pageable.unpaged())
			.stream()
			.map(AccountRoleVo::getRole)
			.collect(Collectors.toList());
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public Collection<SpringSecurityRole> getSpringSecurityRoles(long accountId) {

		final Collection<String> allRoles = this.getAllRoles(accountId);

		return new SpringSecurityRoleCast().casting(allRoles);
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */

	@Override
	public SecurityUserDetail getUserDetail() throws NoPrincipalException {

		return this.getUserDetail(SecurityContextHolder
			.getContext()
			.getAuthentication());
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public SecurityUserDetail getUserDetail(Authentication authentication) throws NoPrincipalException {

		final Object auth = authentication.getPrincipal();

		if (auth instanceof SecurityUserDetail) {
			return (SecurityUserDetail) auth;
		}
		throw new NoPrincipalException();

	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void login(long accountId) throws NoPrincipalException {

		final SecurityUserDetail securityUserDetail = this.toSecurityUserDetail(accountId);

		final Authentication authentication = new UsernamePasswordAuthenticationToken(securityUserDetail, null,
			securityUserDetail.getAuthorities());
		SecurityContextHolder.getContext().setAuthentication(authentication);
		LOGGER.error("사용자 최근로그인 업데이트에 실패하였습니다."); //$NON-NLS-1$
	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void logout(Authentication authentication, HttpServletRequest servletRequest,
		HttpServletResponse servletResponse) {

		// token can be revoked here if needed
		new SecurityContextLogoutHandler().logout(servletRequest, servletResponse, authentication);

		// sending back to client app
		try {
			SessionRedirectSupporter.autoRedirect(servletRequest, servletResponse);
		} catch (final IOException ex) {
			LOGGER.error("로그아웃 후 redirect 중 오류 발생", ex);
		}
	}

	/**
	 * 계정 ID에 대한 {@link SecurityUserDetail}를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @param accountId
	 *            계정 ID
	 * @return {@link SecurityUserDetail}
	 * @throws NoPrincipalException
	 *             인증되지 않은 사용자
	 */
	@VisibleForTesting
	SecurityUserDetail toSecurityUserDetail(long accountId) throws NoPrincipalException {

		final AccountVo account = this.accountService.get(accountId)
			.orElseThrow(NoPrincipalException::new);

		final AuthGeneralVo authGeneral = this.accountService.getAuthGeneral(accountId)
			.orElseThrow(() -> new LogicException(LogicErrorType.INVALID_PARAMETER,
				"현재는 소셜 로그인을 지원하지 않기 때문에 AuthGeneralVO는 반드시 있어야함"));

		final Collection<String> roles = this.getAllRoles(accountId);
		final PersonalInformationVo pi = this.accountService.getPersonalInformation(accountId)
			.orElseThrow(() -> new LogicException(LogicErrorType.INVALID_PARAMETER, "어째 PI가 없는 사람을?"));

		return SecurityUserDetail.getBuilder()
			.username(authGeneral.getLoginId())
			.password(authGeneral.getLoginPassword())
			.enable(account.isLoginEnable())
			.accountNonExpired(true)
			.accountNonLocked(true)
			.credentialsNonExpired(true)
			.authorities(roles
				.stream()
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList()))
			.user(account)
			.personalInformationVo(pi)
			.authGeneralVo(authGeneral)
			.loginProvider(LoginProvider.GENERAL)
			.build();
	}

}
