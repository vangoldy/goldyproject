CREATE TABLE `account` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '테이블 ID',
  `login_email` varchar(127) COLLATE utf8mb4_unicode_ci NOT NULL,
  `login_enable` tinyint(1) NOT NULL DEFAULT '1' COMMENT '로그인 허용 여부',
  `login_non_expired` tinyint(1) NOT NULL DEFAULT '1' COMMENT '로그인 만료 여부',
  `credentials_non_expired` tinyint(1) NOT NULL DEFAULT '1',
  `login_non_locked` tinyint(1) NOT NULL DEFAULT '1' COMMENT '계정 잠김 여부',
  `lately_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '마지막 로그인 시간',
  `login_count` bigint(20) NOT NULL DEFAULT '0' COMMENT '로그인 횟수',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '필드 생성 시간',
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_key_UNIQUE` (`id`),
  UNIQUE KEY `loginEmail_UNIQUE` (`login_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `account_role` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '테이블 ID',
  `account_id` bigint(20) unsigned NOT NULL COMMENT 'user 테이블 ID',
  `role` varchar(31) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '권한명',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '필드 생성 시간',
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`,`role`),
  UNIQUE KEY `user_role_key_UNIQUE` (`id`),
  CONSTRAINT `account_role account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `auth_facebook` (
  `account_id` bigint(20) unsigned NOT NULL COMMENT 'user 테이블 ID',
  `uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '페이스북 사용자 UID',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '필드 생성 시간',
  `user_auth_facebookcol` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `login_id_UNIQUE` (`uid`),
  CONSTRAINT `auth_facebook account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `auth_general_password_change_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `auth_general_id` bigint(20) unsigned NOT NULL,
  `old_password` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_password_history user_auth_general_id_idx` (`auth_general_id`),
  CONSTRAINT `auth_general_password_change_history auth_general_id` FOREIGN KEY (`auth_general_id`) REFERENCES `auth_general` (`account_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `auth_general` (
  `account_id` bigint(20) unsigned NOT NULL COMMENT 'user 테이블 ID',
  `login_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '로그인 아이디',
  `login_password` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '로그인 비밀번호',
  `password_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '마지막 비밀번호 변경 날짜',
  `wrong_password_count` int(20) unsigned NOT NULL COMMENT '패스워드 틀린 횟수',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '필드 생성 시간',
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `login_id_UNIQUE` (`login_id`),
  CONSTRAINT `auth_general account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `auth_google` (
  `account_id` bigint(20) unsigned NOT NULL COMMENT 'user 테이블 ID',
  `uid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '구글 사용자 UID',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '필드 생성 시간',
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `login_id_UNIQUE` (`uid`),
  CONSTRAINT `auth_google account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `oauth_access_token` (
  `token_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `token` mediumblob,
  `authentication_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `client_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authentication` mediumblob,
  `refresh_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `oauth_client_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_secret` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resource_ids` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scope` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `authorized_grant_types` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `web_server_redirect_uri` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `authorities` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_validity` int(11) NOT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `autoapprove` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `client_secret_UNIQUE` (`client_secret`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `personal_information` (
  `account_id` bigint(20) unsigned NOT NULL COMMENT 'user 테이블 ID',
  `user_name` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `nickname` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` timestamp NULL DEFAULT NULL,
  KEY `user_detail id_idx` (`account_id`),
  CONSTRAINT `personal_information account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
