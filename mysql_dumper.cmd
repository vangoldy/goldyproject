SET PATH=%PATH%;C:\Program Files\MySQL\MySQL Server 8.0\bin
SET HOST=localhost
SET USER=root
SET PASS=root
SET FILE_NAME=structure.sql
SET SED=sed "s/ AUTO_INCREMENT=[0-9]*\b//"

mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_admin | %SED% > goldyproject-server-admin\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_eval | %SED% > goldyproject-server-eval\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_filesystem | %SED% > goldyproject-server-filesystem\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_home | %SED% > goldyproject-server-home\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_konzession | %SED% > goldyproject-server-konzession\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_me | %SED% > goldyproject-server-me\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_shorturl | %SED% > goldyproject-server-shorturl\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_sso | %SED% > goldyproject-server-sso\%FILE_NAME%
mysqldump -d -h %HOST% -u %USER% -p%PASS% goldyproject_version | %SED% > goldyproject-server-version\%FILE_NAME%