/**
 * FileName : {@link SecurityConfig}.java
 * Created : 2018. 8. 31. 오후 10:40:21
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.admin.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Value("${gp.host.sso}")
	private String sso;

	/**
	 * {@link SecurityConfig} 클래스의 새 인스턴스를 초기화 합니다.
	 *
	 * @author 2019. 2. 9. 오전 11:25:44 jeonghyun.kum
	 */
	public SecurityConfig() {

		super();

	}

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void configure(HttpSecurity http) throws Exception {

		http
			.antMatcher("/**")
			.authorizeRequests()
			.antMatchers(
				"/resources/**",
				"/actuator/health")
			.permitAll()
			.anyRequest().authenticated();
		http
			.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
			.logoutSuccessUrl(this.sso + "/exit");
	}
}
