/**
 * FileName : {@link TestController}.java
 * Created : 2018. 8. 25. 오후 12:04:59
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.filesystem.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.goldy.gtils.response.Response;

@RestController
public class TestController {

	@GetMapping(value = "/upload")
	public ResponseEntity<String> getMethodName() throws InterruptedException {

		System.out.println(Thread.currentThread().getName());
		Thread.sleep(10000);

		return Response.ok();
	}

}
