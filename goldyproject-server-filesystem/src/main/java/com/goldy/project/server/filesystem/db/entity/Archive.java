/**
 * FileName : {@link Archive}.java
 * Created : 2018. 8. 25. 오후 12:36:44
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.filesystem.db.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Archive {

	@Id
	private long id;

	private String api;

	private String option;

	private String fileName;

	private int userId;

	private ArchiveState state;

	private long downloadCount;

	private LocalDateTime createdTime;

	/**
	 * api를 반환합니다.
	 *
	 * @return api
	 * @author jeonghyun.kum
	 */
	public String getApi() {

		return this.api;
	}

	/**
	 * createdTime를 반환합니다.
	 *
	 * @return createdTime
	 * @author jeonghyun.kum
	 */
	public LocalDateTime getCreatedTime() {

		return this.createdTime;
	}

	/**
	 * downloadCount를 반환합니다.
	 *
	 * @return downloadCount
	 * @author jeonghyun.kum
	 */
	public long getDownloadCount() {

		return this.downloadCount;
	}

	/**
	 * fileName를 반환합니다.
	 *
	 * @return fileName
	 * @author jeonghyun.kum
	 */
	public String getFileName() {

		return this.fileName;
	}

	/**
	 * id를 반환합니다.
	 *
	 * @return id
	 * @author jeonghyun.kum
	 */
	public long getId() {

		return this.id;
	}

	/**
	 * option를 반환합니다.
	 *
	 * @return option
	 * @author jeonghyun.kum
	 */
	public String getOption() {

		return this.option;
	}

	/**
	 * state를 반환합니다.
	 *
	 * @return state
	 * @author jeonghyun.kum
	 */
	public ArchiveState getState() {

		return this.state;
	}

	/**
	 * userId를 반환합니다.
	 *
	 * @return userId
	 * @author jeonghyun.kum
	 */
	public int getUserId() {

		return this.userId;
	}

	/**
	 * api 초기화 합니다.
	 *
	 * @param api
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setApi(String api) {

		this.api = api;
	}

	/**
	 * createdTime 초기화 합니다.
	 *
	 * @param createdTime
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setCreatedTime(LocalDateTime createdTime) {

		this.createdTime = createdTime;
	}

	/**
	 * downloadCount 초기화 합니다.
	 *
	 * @param downloadCount
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setDownloadCount(long downloadCount) {

		this.downloadCount = downloadCount;
	}

	/**
	 * fileName 초기화 합니다.
	 *
	 * @param fileName
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setFileName(String fileName) {

		this.fileName = fileName;
	}

	/**
	 * id 초기화 합니다.
	 *
	 * @param id
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setId(long id) {

		this.id = id;
	}

	/**
	 * option 초기화 합니다.
	 *
	 * @param option
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setOption(String option) {

		this.option = option;
	}

	/**
	 * state 초기화 합니다.
	 *
	 * @param state
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setState(ArchiveState state) {

		this.state = state;
	}

	/**
	 * userId 초기화 합니다.
	 *
	 * @param userId
	 *            초기화 값
	 * @author jeonghyun.kum
	 */

	public void setUserId(int userId) {

		this.userId = userId;
	}

}
