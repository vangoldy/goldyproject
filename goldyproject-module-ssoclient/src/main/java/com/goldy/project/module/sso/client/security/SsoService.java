/**
 * FileName : {@link SsoService}.java
 * Created : 2018. 9. 14. 오후 8:42:57
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.module.sso.client.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class SsoService {

	private Authentication getAuthentication() {

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			throw new SsoException("is annoymous user");
		}
		return authentication;
	}

	public SsoPrincipal getPrincipal() {

		final Authentication authentication = this.getAuthentication();

		final Object details = authentication.getDetails();
		System.out.println(details);

		final Object principal = authentication.getPrincipal();
		System.out.println(principal);

		return new SsoPrincipal();
	}

}
