-- MySQL dump 10.13  Distrib 8.0.11, for Win64 (x86_64)
--
-- Host: localhost    Database: goldyproject_home
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`id`, `name`, `description`, `created_time`, `updated_time`) VALUES (4,'Admin','관리자','2018-09-01 12:16:50','2018-09-08 04:59:22'),(10,'Eval','심사','2018-09-08 04:54:48','2018-09-08 04:59:22'),(2,'File System','Web FileSystem','2018-09-01 12:16:50','2018-09-08 04:59:22'),(3,'Home','지금 이 페이지','2018-09-01 12:16:50','2018-09-08 04:59:22'),(9,'Konzession','특허','2018-09-08 04:52:50','2018-09-08 04:59:22'),(1,'Me','of the me, by the me, for the me','2018-09-01 12:16:50','2018-09-08 04:59:22'),(11,'Push','푸시 서버','2018-09-13 11:04:54',NULL),(5,'ShortURL','일반 URL, 암호화 URL, 기간제 URL','2018-09-01 12:16:50','2018-09-08 04:59:22'),(6,'SSO','인증','2018-09-01 12:16:50','2018-09-08 04:59:22'),(7,'Version','프로그램 버전 관리','2018-09-01 12:16:50','2018-09-08 04:59:22'),(8,'Webtoon Review','웹툰 리뷰','2018-09-01 12:16:50','2018-09-08 04:59:22');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `project_card_drawing`
--

LOCK TABLES `project_card_drawing` WRITE;
/*!40000 ALTER TABLE `project_card_drawing` DISABLE KEYS */;
INSERT INTO `project_card_drawing` (`project_id`, `icon_url`, `click_url`, `metadata`, `facebook_url`, `twitter_url`, `skype_url`, `google_url`, `detail_description`) VALUES (1,'gZu','http://localhost:20000/me',NULL,NULL,'test',NULL,'me','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(2,'bp9','http://localhost:10010/filesystem',NULL,'test',NULL,NULL,'File System','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(3,'3ZM','http://localhost',NULL,NULL,NULL,NULL,'Home','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(4,'z6H','http://localhost:10020/admin',NULL,NULL,'test',NULL,'Admin','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(5,'yl0','http://localhost:10030/shorturl',NULL,NULL,NULL,NULL,'ShortURL','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(6,'Wki','http://localhost:10000/sso',NULL,'test','test','test','SSO','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(7,'Yhy','http://localhost:20010/version',NULL,NULL,NULL,NULL,'Version MGT','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(8,'Krz','http://localhost:20020/webtoonreview',NULL,NULL,NULL,NULL,'Webtoon Review','This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(9,'w8M','http://localhost:20030/konzession',NULL,NULL,NULL,NULL,NULL,'This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(10,'S0i','http://localhost:20040/eval',NULL,NULL,NULL,NULL,NULL,'This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.'),(11,'UbZ','http://localhost:20050/push',NULL,NULL,NULL,NULL,NULL,'This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.This is basic card with image on top, title, description and button.');
/*!40000 ALTER TABLE `project_card_drawing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_name`, `age`, `memo`, `token_id`) VALUES ('jkkang',27,'jkkang in goldy.home',NULL),('jmpark',48,'jmpark in goldy.home',NULL),('tsong',27,'tsong in goldy.home',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-15 21:34:23
