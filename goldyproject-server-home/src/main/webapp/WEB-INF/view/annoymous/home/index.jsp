<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<title>GOLDY PROJECT</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
	integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous"
>
<link href="<c:url value="/resources/custom/view/home.css"/>" rel="stylesheet" type="text/css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="<c:url value="/resources/custom/view/home.js"/>"></script>
<script type="text/javascript">
	var shorturlPrepare = '${env["gp.host.shorturl.redirect.prefix"]}';
	var $projects = eval('(' + '${projects}' + ')');
</script>
</head>
<body type="menu-page">
	<center>
		<div class="log-outer">
			<img src='${env["gp.host.shorturl.redirect.prefix"]}J61' />
		</div>
	</center>
	<center>
		<div class="sso-login-outer">
			<!-- <div class="container"> -->
			<sec:authorize access="isAuthenticated()">
				<div class="btn btn-primary sso-login" onClick="javascript:location.href='logout'">logout</div>
			</sec:authorize>
			<sec:authorize access="isAnonymous()">
				<div class="btn btn-primary sso-login" onClick="javascript:location.href='securedpage'">Login</div>
			</sec:authorize>
			<!-- 	</div> -->
		</div>
	</center>
	<section id="team" class="pb-5">
		<div class="container">
			<h5 class="section-title h1">PROJECT</h5>
			<div class="row">
				<!-- Team member -->
				<div class="col-xs-12 col-sm-6 col-md-4 d-none">
					<div class="image-flip" ontouchstart="this.classList.toggle('hover');">
						<div class="mainflip">
							<div class="frontside">
								<div class="card">
									<div class="card-body text-center">
										<p>
											<img class=" img-fluid"
												src="https://cdn2.iconfinder.com/data/icons/social-media-network-fill-flat-icon/512/Spring.me-512.png"
												alt="card image"
											>
										</p>
										<h4 class="card-title">Me</h4>
										<p class="card-text">This is basic card with image on top, title, description and button.</p>
										<a href="#" class="btn btn-primary btn-sm">
											<i class="fa fa-plus"></i>
										</a>
									</div>
								</div>
							</div>
							<div class="backside">
								<div class="card">
									<div class="card-body text-center mt-4">
										<h4 class="card-title">Me</h4>
										<p class="card-text">This is basic card with image on top, title, description and button.This is basic
											card with image on top, title, description and button.This is basic card with image on top, title,
											description and button.</p>
										<ul class="list-inline">
											<li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#">
													<i class="fa fa-facebook"></i>
												</a></li>
											<li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#">
													<i class="fa fa-twitter"></i>
												</a></li>
											<li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#">
													<i class="fa fa-skype"></i>
												</a></li>
											<li class="list-inline-item"><a class="social-icon text-xs-center" target="_blank" href="#">
													<i class="fa fa-google"></i>
												</a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- ./Team member -->
			</div>
		</div>
	</section>
</body>
</html>