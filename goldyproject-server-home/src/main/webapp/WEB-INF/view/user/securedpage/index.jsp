<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<sec:authentication var="user" property="principal" />
</head>
<body type="menu-page">
	<h1>Secured Page</h1>
	Welcome,
	<span>${user}Name</span>
	<sec:authorize access="hasRole('ROLE_USER') and isAuthenticated()">
</sec:authorize>
</body>
</html>