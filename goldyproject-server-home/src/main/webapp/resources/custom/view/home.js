$(function() {
	drawProjectCards();
})

function drawProjectCards() {

	let $cardBox = $('section#team > .container > .row');
	var i = 0;

	function timeLoop() {
		setTimeout(function() {
			let project = $projects[i];

			$.ajax({
				url : '/api/project/' + project.id + '/carddrawing',
				async : false,
				success : function(cardDrawing) {
					if (!cardDrawing || cardDrawing === null
							|| cardDrawing === 'null') {
						return;
					}
					var $projectCard = createProjectCard(project, cardDrawing);
					$cardBox.append($projectCard);
				}
			})
			i++;
			if (i < $projects.length) {
				timeLoop();
			}
		}, Math.floor((Math.random() * 100)))
	}
	timeLoop();
}

function createProjectCard(project, cardDrawing) {

	let $colDiv = $('<div class="col-xs-12 col-sm-6 col-md-4"><div class="image-flip" ontouchstart="this.classList.toggle(\'hover\');"> <div class="mainflip">');
	let $mainFlip = $colDiv.find('.mainflip');
	let $frontSideDiv = createFrontSideCard(project, cardDrawing);
	let $backSideDiv = createBackSideCard(project, cardDrawing);

	$mainFlip.append($frontSideDiv);
	$mainFlip.append($backSideDiv);

	if (cardDrawing.clickUrl) {
		$.ajax({
			url : cardDrawing.clickUrl + '/actuator/health',
			success : function(result) {
				if (result.status || result.status === 'up') {
					$colDiv.addClass('linkable');
					$colDiv.css({
						'cursor' : 'pointer'
					});
					$colDiv.click(function() {
						location.href = cardDrawing.clickUrl;
					})
					
					var a = $mainFlip.find('.ready-server-enable');
					a.removeClass('d-none');
				}
			},
			error : function(xhr) {
				console.info(xhr);
			}
		})
	}

	return $colDiv;
}

function createFrontSideCard(project, cardDrawing) {
	let $frontSideDiv = $('<div class="frontside"><div class="card"><div class="card-body text-center">');
	let $cardBodyDiv = $frontSideDiv.find('.card-body');

	let $pImg = $('<p class="temp"><img class="img-fluid" alt="card image">');
	$cardBodyDiv.append($pImg);
	$pImg.find('img').attr('src', shorturlPrepare + cardDrawing.iconUrl);

	let $nameDiv = $('<h4 class="card-title">');
	$nameDiv.text(project.name);
	$cardBodyDiv.append($nameDiv);

	let $descriptionP = $('<p class="card-text">');
	$descriptionP.text(project.description);
	$cardBodyDiv.append($descriptionP);

	let $plusA = $('<a href="#" class="btn btn-primary btn-sm d-none ready-server-enable"><i class="fas fa-heartbeat">');
	$cardBodyDiv.append($plusA);

	return $frontSideDiv;
}
function createBackSideCard(project, cardDrawing, serverEnabled) {

	let $backSideDiv = $('<div class="backside"><div class="card"><div class="card-body text-center mt-4">');
	let $cardBodyDiv = $backSideDiv.find('.card-body');

	let $nameDiv = $('<h4 class="card-title">');
	$nameDiv.text(project.name);
	$cardBodyDiv.append($nameDiv);

	let $descriptionP = $('<p class="card-text">');
	$descriptionP.text(cardDrawing.detailDescription);
	$cardBodyDiv.append($descriptionP);

	let $socialList = $('<ul class="list-inline">');
	$cardBodyDiv.append($socialList);
	if (cardDrawing.facebookUrl) {
		let $item = createSocialItem('fa-facebook', cardDrawing.facebookUrl);
		$socialList.append($item);
	}
	if (cardDrawing.twitterUrl) {
		let $item = createSocialItem('fa-twitter', cardDrawing.twitterUrl);
		$socialList.append($item);
	}
	if (cardDrawing.skypeUrl) {
		let $item = createSocialItem('fa-skype', cardDrawing.skypeUrl);
		$socialList.append($item);
	}
	if (cardDrawing.googleUrl) {
		let $item = createSocialItem('fa-google', cardDrawing.googleUrl);
		$socialList.append($item);
	}

	let $button = createLinkButton(cardDrawing.clickUrl, 'GO TO '
			+ project.name);
	$cardBodyDiv.append($button);

	return $backSideDiv;
}

function createSocialItem(fontAwesome, url) {
	let $li = $('<li class="list-inline-item">');
	let $a = $('<a class="social-icon text-xs-center" target="_blank">');
	$a.attr('href', url);
	$li.append($a);

	let $i = $('<i class="fab">');
	$i.addClass(fontAwesome);
	$a.append($i);
	return $li;
}

function createLinkButton(url, text) {
	let $buttonDiv = $('<div class="btn btn-primary d-none ready-server-enable">');

	$buttonDiv.append(text);
	$buttonDiv.click(function() {
		location.href = url;
	})
	return $buttonDiv;
}