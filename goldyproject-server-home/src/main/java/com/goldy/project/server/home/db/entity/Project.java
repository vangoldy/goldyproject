/**
 * FileName : {@link Project}.java
 * Created : 2018. 9. 1. 오후 9:23:44
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.db.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.goldy.gtils.encryption.annotation.DecryptId;
import com.goldy.gtils.encryption.annotation.LongCryptoSerializer;

@Entity
public class Project extends TimeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false, columnDefinition = "BIGINT UNSIGNED")
	@JsonSerialize(using = LongCryptoSerializer.class)
	@DecryptId("account")
	private Long id;

	private String name;

	private String description;

	/**
	 * description를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return description
	 */
	public String getDescription() {

		return this.description;
	}

	/**
	 * id를 반환합니다.
	 *
	 * @return id
	 * @author jeonghyun.kum
	 */
	public Long getId() {

		return this.id;
	}

	/**
	 * name를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return name
	 */
	public String getName() {

		return this.name;
	}

	/**
	 * description 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param description
	 *            초기화 값
	 */
	public void setDescription(String description) {

		this.description = description;
	}

	/**
	 * id 초기화 합니다.
	 *
	 * @param id
	 *            초기화 값
	 * @author jeonghyun.kum
	 */
	public void setId(Long id) {

		this.id = id;
	}

	/**
	 * name 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param name
	 *            초기화 값
	 */
	public void setName(String name) {

		this.name = name;
	}

}
