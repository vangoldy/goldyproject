/**
 * FileName : {@link LoginSuccessHandler}.java
 * Created : 2018. 9. 11. 오후 8:04:57
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.config.service;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

@Service
public class LoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	/**
	 * {@inheritDoc}
	 *
	 * @author jeonghyun.kum
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest servletRequest, HttpServletResponse servletResponse,
		Authentication authentication) throws ServletException, IOException {

		super.onAuthenticationSuccess(servletRequest, servletResponse, authentication);
	}

}
