/**
 * FileName : {@link TimeEntity}.java
 * Created : 2018. 9. 9. 오후 2:00:36
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.db.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
public class TimeEntity {

	@CreationTimestamp
	@Column(nullable = false, updatable = false)
	private LocalDateTime createdTime;

	@UpdateTimestamp
	@Column(nullable = true, updatable = true)
	private LocalDateTime updatedTime;

	/**
	 * createdTime를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return createdTime
	 */
	public LocalDateTime getCreatedTime() {

		return this.createdTime;
	}

	/**
	 * updatedTime를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return updatedTime
	 */
	public LocalDateTime getUpdatedTime() {

		return this.updatedTime;
	}

	/**
	 * createdTime 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param createdTime
	 *            초기화 값
	 */
	public void setCreatedTime(LocalDateTime createdTime) {

		this.createdTime = createdTime;
	}

	/**
	 * updatedTime 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param updatedTime
	 *            초기화 값
	 */
	public void setUpdatedTime(LocalDateTime updatedTime) {

		this.updatedTime = updatedTime;
	}
}
