/**
 * FileName : {@link ProjectDao}.java
 * Created : 2018. 9. 1. 오후 9:35:50
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.db.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.goldy.project.server.home.db.entity.Project;

public interface ProjectDao extends JpaRepository<Project, Long> {
	// do nothing
}
