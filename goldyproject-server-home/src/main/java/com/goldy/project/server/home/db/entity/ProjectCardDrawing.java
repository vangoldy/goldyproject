/**
 * FileName : {@link ProjectCardDrawing}.java
 * Created : 2018. 9. 1. 오후 10:27:14
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.db.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ProjectCardDrawing {

	@Id
	private Long projectId;

	private String iconUrl;

	private String clickUrl;

	private String facebookUrl;

	private String twitterUrl;

	private String skypeUrl;

	private String googleUrl;

	private String detailDescription;

	/**
	 * clickUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return clickUrl
	 */
	public String getClickUrl() {

		return this.clickUrl;
	}

	/**
	 * detailDescription를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return detailDescription
	 */
	public String getDetailDescription() {

		return this.detailDescription;
	}

	/**
	 * facebookUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return facebookUrl
	 */
	public String getFacebookUrl() {

		return this.facebookUrl;
	}

	/**
	 * googleUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return googleUrl
	 */
	public String getGoogleUrl() {

		return this.googleUrl;
	}

	/**
	 * iconUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return iconUrl
	 */
	public String getIconUrl() {

		return this.iconUrl;
	}

	/**
	 * projectId를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return projectId
	 */
	public Long getProjectId() {

		return this.projectId;
	}

	/**
	 * skypeUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return skypeUrl
	 */
	public String getSkypeUrl() {

		return this.skypeUrl;
	}

	/**
	 * twitterUrl를 반환합니다.
	 *
	 * @author jeonghyun.kum
	 * @return twitterUrl
	 */
	public String getTwitterUrl() {

		return this.twitterUrl;
	}

	/**
	 * clickUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param clickUrl
	 *            초기화 값
	 */
	public void setClickUrl(String clickUrl) {

		this.clickUrl = clickUrl;
	}

	/**
	 * detailDescription 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param detailDescription
	 *            초기화 값
	 */
	public void setDetailDescription(String detailDescription) {

		this.detailDescription = detailDescription;
	}

	/**
	 * facebookUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param facebookUrl
	 *            초기화 값
	 */
	public void setFacebookUrl(String facebookUrl) {

		this.facebookUrl = facebookUrl;
	}

	/**
	 * googleUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param googleUrl
	 *            초기화 값
	 */
	public void setGoogleUrl(String googleUrl) {

		this.googleUrl = googleUrl;
	}

	/**
	 * iconUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param iconUrl
	 *            초기화 값
	 */
	public void setIconUrl(String iconUrl) {

		this.iconUrl = iconUrl;
	}

	/**
	 * projectId 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param projectId
	 *            초기화 값
	 */
	public void setProjectId(Long projectId) {

		this.projectId = projectId;
	}

	/**
	 * skypeUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param skypeUrl
	 *            초기화 값
	 */
	public void setSkypeUrl(String skypeUrl) {

		this.skypeUrl = skypeUrl;
	}

	/**
	 * twitterUrl 초기화 합니다.
	 *
	 * @author jeonghyun.kum
	 * @param twitterUrl
	 *            초기화 값
	 */
	public void setTwitterUrl(String twitterUrl) {

		this.twitterUrl = twitterUrl;
	}

}
