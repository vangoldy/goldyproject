/**
 * FileName : {@link DataSourceConfig}.java
 * Created : 2018. 8. 24. 오후 11:57:45
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@PropertySource("classpath:datasource.properties")
@Configuration
public class DataSourceConfig {

	@Bean
	@ConfigurationProperties("gp.datasource.home")
	public DataSource dataSource() {

		return new DriverManagerDataSource();
	}

}
