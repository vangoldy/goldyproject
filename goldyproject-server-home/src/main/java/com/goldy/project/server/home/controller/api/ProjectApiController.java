/**
 * FileName : {@link ProjectApiController}.java
 * Created : 2018. 9. 1. 오후 10:37:37
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.controller.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.goldy.project.server.home.db.entity.ProjectCardDrawing;
import com.goldy.project.server.home.db.repository.ProjectCardDrawingDao;

@RestController
public class ProjectApiController {

	@Autowired
	private ProjectCardDrawingDao projectCardDrawingDao;

	@Autowired
	private Environment env;

	@GetMapping(value = "/api/project/{projectId}/carddrawing")
	public Optional<ProjectCardDrawing> getMethodName(@PathVariable long projectId) {

		final Optional<ProjectCardDrawing> cardDrawing = this.projectCardDrawingDao.findById(projectId);

		if (cardDrawing.isPresent() == false) {
			return cardDrawing;
		}

		final ProjectCardDrawing projectCardDrawing = cardDrawing.get();
		projectCardDrawing
			.setClickUrl(this.env.getProperty(projectCardDrawing.getClickUrl(), projectCardDrawing.getClickUrl()));

		return cardDrawing;
	}

}
