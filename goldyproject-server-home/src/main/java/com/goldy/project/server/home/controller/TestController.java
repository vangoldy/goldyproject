/**
 * FileName : {@link TestController}.java
 * Created : 2018. 9. 1. 오후 1:45:43
 * Author : jeonghyun.kum
 * Copyright (C) 2019 GoldyProject. All rights reserved.
 * 이 문서의 모든 저작권 및 지적 재산권은 (주)GoldyProject에게 있습니다.
 * 이 문서의 어떠한 부분도 허가 없이 복제 또는 수정 하거나, 전송할 수 없습니다.
 */
package com.goldy.project.server.home.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.goldy.gtils.utils.JsonGtils;
import com.goldy.project.server.home.db.entity.Project;
import com.goldy.project.server.home.db.repository.ProjectDao;

@Controller
public class TestController {

	@Autowired
	private ProjectDao projectDao;

	@GetMapping(value = { "/", "/home" })
	public String getMethodName(ModelMap modelMap) {

		final List<Project> findAll = this.projectDao.findAll();
		findAll.sort((o1, o2) -> Long.compare(o1.getId(), o2.getId()));

		modelMap.addAttribute("projects", JsonGtils.toJackson(findAll));

		return "annoymous/home/index";
	}

	@GetMapping("/api/user/me")
	public Object user() {

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		final Object principal2 = authentication.getPrincipal();
		System.out.println(principal2);
		return principal2;
	}
}
